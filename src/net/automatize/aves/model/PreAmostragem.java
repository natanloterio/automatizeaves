package net.automatize.aves.model;

import java.util.Date;

public class PreAmostragem
{
	public static final String CODIGO = "codigo";
	public static final String NRLOTE = "nrlote";
	public static final String SEXO = "sexo";
	public static final String TIPO = "tipo";
	public static final String NRAVES = "nraves";
	public static final String NRCORTES = "nrcortes";
	public static final String DATA = "data";
	public static final String[] COLUNAS = { CODIGO, NRLOTE, SEXO, TIPO,
			NRAVES, NRCORTES, DATA };

	private short codigo;
	private String numeroLote;
	private String sexo;
	private String tipo;
	private int numeroAves;
	private short numeroCortes;
	private Date data;

	public short getCodigo()
	{
		return codigo;
	}

	public void setCodigo(short codigo)
	{
		this.codigo = codigo;
	}

	public String getNumeroLote()
	{
		return numeroLote;
	}

	public void setNumeroLote(String numeroLote)
	{
		this.numeroLote = numeroLote;
	}

	public String getSexo()
	{
		return sexo;
	}

	public void setSexo(String sexo)
	{
		this.sexo = sexo;
	}

	public String getTipo()
	{
		return tipo;
	}

	public void setTipo(String tipo)
	{
		this.tipo = tipo;
	}

	public int getNumeroAves()
	{
		return numeroAves;
	}

	public void setNumeroAves(int numeroAves)
	{
		this.numeroAves = numeroAves;
	}

	public short getNumeroCortes()
	{
		return numeroCortes;
	}

	public void setNumeroCortes(short numeroCortes)
	{
		this.numeroCortes = numeroCortes;
	}

	public Date getData()
	{
		return data;
	}

	public void setData(Date data)
	{
		this.data = data;
	}

	@Override
	public String toString()
	{
		return "PreAmostragem [codigo=" + codigo + ", numeroLote=" + numeroLote
				+ ", sexo=" + sexo + ", tipo=" + tipo + ", numeroAves="
				+ numeroAves + ", numeroCortes=" + numeroCortes + ", data="
				+ data + "]";
	}
}
