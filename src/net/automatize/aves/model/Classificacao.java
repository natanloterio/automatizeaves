package net.automatize.aves.model;

public class Classificacao
{
	public static final String CODIGO = "codigo";
	public static final String SIGLA = "sigla";
	public static final String[] COLUNAS = { CODIGO, SIGLA };

	private short codigo;
	private String sigla;

	public short getCodigo()
	{
		return codigo;
	}

	public void setCodigo(short codigo)
	{
		this.codigo = codigo;
	}

	public String getSigla()
	{
		return sigla;
	}

	public void setSigla(String sigla)
	{
		this.sigla = sigla;
	}

	public Classificacao(short codigo, String sigla)
	{
		super();
		this.codigo = codigo;
		this.sigla = sigla;
	}

	@Override
	public String toString()
	{
		return codigo + "\t" + sigla;
	}

}
