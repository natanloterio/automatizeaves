package net.automatize.aves.model;

public class Parametros
{
	public static final String CODIGO = "codigo";
	public static final String NUMEROLOTE = "numerolote";
	public static final String TAMANHONRLOTE = "tamanhonrlote";
	public static final String BALANCACOD = "balancacod";
	public static final String[] COLUNAS = { CODIGO, NUMEROLOTE, TAMANHONRLOTE,
			BALANCACOD };

	private int codigo;
	private short numeroLote;
	private short tamanhoNumeroLote;
	private short codigoBalanca;

	public Parametros(int codigo, short numeroLote, short tamanhoNumeroLote,
			short codigoBalanca)
	{
		super();
		this.codigo = codigo;
		this.numeroLote = numeroLote;
		this.tamanhoNumeroLote = tamanhoNumeroLote;
		this.codigoBalanca = codigoBalanca;
	}

	public int getCodigo()
	{
		return codigo;
	}

	public void setCodigo(int codigo)
	{
		this.codigo = codigo;
	}

	public short getNumeroLote()
	{
		return numeroLote;
	}

	public void setNumeroLote(short numeroLote)
	{
		this.numeroLote = numeroLote;
	}

	public short getTamanhoNumeroLote()
	{
		return tamanhoNumeroLote;
	}

	public void setTamanhoNumeroLote(short tamanhoNumeroLote)
	{
		this.tamanhoNumeroLote = tamanhoNumeroLote;
	}

	public short getCodigoBalanca()
	{
		return codigoBalanca;
	}

	public void setCodigoBalanca(short codigoBalanca)
	{
		this.codigoBalanca = codigoBalanca;
	}
}
