package net.automatize.aves.model;

public class TipoPesagem
{
	public static final String CODIGO = "codigo";
	public static final String TIPOPESAGEM = "tipopesagem";
	public static final String PESOMINIMO = "pesominimo";
	public static final String MACADDRESS = "macaddress";
	public static final String[] COLUNAS = { CODIGO, TIPOPESAGEM, PESOMINIMO,
			MACADDRESS };

	private short codigo;
	private short tipoPesagem;
	private int pesoMinimo;
	private String macAddress;

	public short getCodigo()
	{
		if (codigo == 0)
			return 1;
		return codigo;
	}

	public void setCodigo(short codigo)
	{
		this.codigo = codigo;
	}

	public short getTipoPesagem()
	{
		return tipoPesagem;
	}

	public void setTipoPesagem(short tipoPesagem)
	{
		this.tipoPesagem = tipoPesagem;
	}

	public int getPesoMinimo()
	{
		return pesoMinimo;
	}

	public void setPesoMinimo(int pesoMinimo)
	{
		this.pesoMinimo = pesoMinimo;
	}

	public String getMacAddress()
	{
		return macAddress;
	}

	public void setMacAddress(String macAddress)
	{
		this.macAddress = macAddress;
	}

	public TipoPesagem(short codigo, short tipoPesagem, int pesoMinimo,
			String macAddress)
	{
		super();
		this.codigo = codigo;
		this.tipoPesagem = tipoPesagem;
		this.pesoMinimo = pesoMinimo;
		this.macAddress = macAddress;
	}

	public TipoPesagem()
	{
	};
}
