package net.automatize.aves.model;

public class PreAmostragemResultado
{
	private String classificacao_sigla;
	private int faixa_inicial;
	private int faixa_final;
	private int qtd;
	private int acumulado;
	private int media;

	public String getClassificacao_sigla()
	{
		return classificacao_sigla;
	}

	public void setClassificacao_sigla(String classificacao_sigla)
	{
		this.classificacao_sigla = classificacao_sigla;
	}

	public int getFaixa_inicial()
	{
		return faixa_inicial;
	}

	public void setFaixa_inicial(int faixa_inicial)
	{
		this.faixa_inicial = faixa_inicial;
	}

	public int getFaixa_final()
	{
		return faixa_final;
	}

	public void setFaixa_final(int faixa_final)
	{
		this.faixa_final = faixa_final;
	}

	public int getQtd()
	{
		return qtd;
	}

	public void setQtd(int qtd)
	{
		this.qtd = qtd;
	}

	public int getMedia()
	{
		return media;
	}

	public void setMedia(int media)
	{
		this.media = media;
	}

	public int getAcumulado()
	{
		return acumulado;
	}

	public void setAcumulado(int acumulado)
	{
		this.acumulado = acumulado;
	}

}
