package net.automatize.aves.model;

public class Selecao100Peso
{
	public static final String CODIGO = "codigo";
	public static final String PESO = "peso";
	public static final String CLASSIFICACAO_CODIGO = "classificacao_codigo";
	public static final String[] COLUNAS = { CODIGO, PESO, CLASSIFICACAO_CODIGO };

	private int codigo;
	private int peso;
	private short classificacao_codigo;
	private String classificacao_sigla;

	public int getCodigo()
	{
		return codigo;
	}

	public void setCodigo(int codigo)
	{
		this.codigo = codigo;
	}

	public int getPeso()
	{
		return peso;
	}

	public void setPeso(int peso)
	{
		this.peso = peso;
	}

	public short getClassificacao_codigo()
	{
		return classificacao_codigo;
	}

	public void setClassificacao_codigo(short classificacao_codigo)
	{
		this.classificacao_codigo = classificacao_codigo;
	}

	public String getClassificacao_sigla()
	{
		return classificacao_sigla;
	}

	public void setClassificacao_sigla(String classificacao_sigla)
	{
		this.classificacao_sigla = classificacao_sigla;
	}
}
