package net.automatize.aves.model;

public class Selecao100ClassificacaoItem
{
	private short classificacao_codigo;
	private String classificacao_sigla;
	private boolean selecionado;
	private int faixainicial;
	private int faixafinal;

	public short getClassificacao_codigo()
	{
		return classificacao_codigo;
	}

	public void setClassificacao_codigo(short classificacao_codigo)
	{
		this.classificacao_codigo = classificacao_codigo;
	}

	public String getClassificacao_sigla()
	{
		return classificacao_sigla;
	}

	public void setClassificacao_sigla(String classificacao_sigla)
	{
		this.classificacao_sigla = classificacao_sigla;
	}

	public boolean getSelecionado()
	{
		return selecionado;
	}

	public void setSelecionado(boolean selecionado)
	{
		this.selecionado = selecionado;
	}

	public int getFaixainicial()
	{
		return faixainicial;
	}

	public void setFaixainicial(int faixainicial)
	{
		this.faixainicial = faixainicial;
	}

	public int getFaixafinal()
	{
		return faixafinal;
	}

	public void setFaixafinal(int faixafinal)
	{
		this.faixafinal = faixafinal;
	}

}
