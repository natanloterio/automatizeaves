package net.automatize.aves.model;

public class AmostragemTransferenciaPeso
{
	private String nr_lote;
	private String sexo;
	private short nr_box;
	private String classificacao_codigo;
	private double peso;

	public String getNr_lote()
	{
		return nr_lote;
	}

	public void setNr_lote(String nr_lote)
	{
		this.nr_lote = nr_lote;
	}

	public String getSexo()
	{
		return sexo;
	}

	public void setSexo(String sexo)
	{
		this.sexo = sexo;
	}

	public short getNr_box()
	{
		return nr_box;
	}

	public void setNr_box(short nr_box)
	{
		this.nr_box = nr_box;
	}

	public String getClassificacao_codigo()
	{
		return classificacao_codigo;
	}

	public void setClassificacao_codigo(String classificacao_codigo)
	{
		this.classificacao_codigo = classificacao_codigo;
	}

	public double getPeso()
	{
		return peso;
	}

	public void setPeso(double peso)
	{
		this.peso = peso;
	}
}