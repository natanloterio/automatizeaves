package net.automatize.aves.model;

public class AmostragemPeso
{
	public static final String AMOSTRAGEMLOTE_CODIGO = "amostragemlote_codigo"; // AmostragemBox.AMOSTRAGEM_CODIGO
	public static final String AMOSTRAGEMBOX_CODIGO = "amostragembox_codigo"; // AmostragemBox.CODIGO
	public static final String CODIGO = "codigo";
	public static final String PESO = "peso";
	public static final String[] COLUNAS = { CODIGO, AMOSTRAGEMLOTE_CODIGO,
			AMOSTRAGEMBOX_CODIGO, PESO };

	private int amostragemlote_codigo;
	private int amostragembox_codigo;
	private int codigo;
	private int peso;
	
	public AmostragemPeso()
	{
		super();
	}

	public AmostragemPeso(int codigo, int peso)
	{
		super();
		this.codigo = codigo;
		this.peso = peso;
	}

	public AmostragemPeso(int amostragemlote_codigo, int amostragembox_codigo,
			int codigo, int peso)
	{
		super();
		this.amostragemlote_codigo = amostragemlote_codigo;
		this.amostragembox_codigo = amostragembox_codigo;
		this.codigo = codigo;
		this.peso = peso;
	}

	public int getAmostragemlote_codigo()
	{
		return amostragemlote_codigo;
	}

	public void setAmostragemlote_codigo(int amostragemlote_codigo)
	{
		this.amostragemlote_codigo = amostragemlote_codigo;
	}

	public int getAmostragembox_codigo()
	{
		return amostragembox_codigo;
	}

	public void setAmostragembox_codigo(int amostragembox_codigo)
	{
		this.amostragembox_codigo = amostragembox_codigo;
	}

	public int getCodigo()
	{
		return codigo;
	}

	public void setCodigo(int codigo)
	{
		this.codigo = codigo;
	}

	public int getPeso()
	{
		return peso;
	}

	public void setPeso(int peso)
	{
		this.peso = peso;
	}
}
