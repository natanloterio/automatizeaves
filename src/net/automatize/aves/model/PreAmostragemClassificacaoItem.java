package net.automatize.aves.model;

public class PreAmostragemClassificacaoItem
{
	private int classificacao_codigo;
	private String classificacao_sigla;
	private boolean selecionado;
	private int percentual;

	public int getClassificacao_codigo()
	{
		return classificacao_codigo;
	}

	public void setClassificacao_codigo(int classificacao_codigo)
	{
		this.classificacao_codigo = classificacao_codigo;
	}

	public String getClassificacao_sigla()
	{
		return classificacao_sigla;
	}

	public void setClassificacao_sigla(String classificacao_sigla)
	{
		this.classificacao_sigla = classificacao_sigla;
	}

	public boolean getSelecionado()
	{
		return selecionado;
	}

	public void setSelecionado(boolean selecionado)
	{
		this.selecionado = selecionado;
	}

	public int getPercentual()
	{
		return percentual;
	}

	public void setPercentual(int percentual)
	{
		this.percentual = percentual;
	}
}
