package net.automatize.aves.model;

public class PesoEstatistica
{
	private int qtd;
	private int sum;
	private int avg;
	private int min;
	private int max;
	
	public int getQtd()
	{
		return qtd;
	}

	public void setQtd(int qtd)
	{
		this.qtd = qtd;
	}

	public int getSum()
	{
		return sum;
	}

	public void setSum(int sum)
	{
		this.sum = sum;
	}

	public int getAvg()
	{
		return avg;
	}

	public void setAvg(int avg)
	{
		this.avg = avg;
	}

	public int getMin()
	{
		return min;
	}

	public void setMin(int min)
	{
		this.min = min;
	}

	public int getMax()
	{
		return max;
	}

	public void setMax(int max)
	{
		this.max = max;
	}
}
