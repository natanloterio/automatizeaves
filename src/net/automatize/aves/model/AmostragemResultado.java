package net.automatize.aves.model;

public class AmostragemResultado
{
	private AmostragemBox amostragemBox;
	private int qtdPesos;
	private double pesoTotal;
	private double pesoMedio;
	private double menorPeso;
	private double maiorPeso;

	public AmostragemBox getAmostragemBox()
	{
		return amostragemBox;
	}

	public void setAmostragemBox(AmostragemBox amostragemBox)
	{
		this.amostragemBox = amostragemBox;
	}

	public int getQtdPesos()
	{
		return qtdPesos;
	}

	public void setQtdPesos(int qtdPesos)
	{
		this.qtdPesos = qtdPesos;
	}

	public double getPesoTotal()
	{
		return pesoTotal;
	}

	public void setPesoTotal(double pesoTotal)
	{
		this.pesoTotal = pesoTotal;
	}

	public double getPesoMedio()
	{
		return pesoMedio;
	}

	public void setPesoMedio(double pesoMedio)
	{
		this.pesoMedio = pesoMedio;
	}

	public double getMenorPeso()
	{
		return menorPeso;
	}

	public void setMenorPeso(double menorPeso)
	{
		this.menorPeso = menorPeso;
	}

	public double getMaiorPeso()
	{
		return maiorPeso;
	}

	public void setMaiorPeso(double maiorPeso)
	{
		this.maiorPeso = maiorPeso;
	}
}
