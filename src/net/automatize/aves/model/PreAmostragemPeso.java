package net.automatize.aves.model;

public class PreAmostragemPeso
{
	public static final String CODIGO = "codigo";
	public static final String PESO = "peso";
	public static final String[] COLUNAS = { CODIGO, PESO };

	private int codigo;
	private int peso;

	public int getCodigo()
	{
		return codigo;
	}

	public void setCodigo(int codigo)
	{
		this.codigo = codigo;
	}

	public int getPeso()
	{
		return peso;
	}

	public void setPeso(int peso)
	{
		this.peso = peso;
	}
}
