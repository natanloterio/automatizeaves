package net.automatize.aves.model;

import java.sql.Date;

public class AmostragemData
{
	public static final String CODIGO = "codigo";
	public static final String DATAAMOSTRAGEM = "data_amostragem";
	public static final String[] COLUNAS = { CODIGO, DATAAMOSTRAGEM };

	private short codigo;
	private Date data;

	public short getCodigo()
	{
		return codigo;
	}

	public void setCodigo(short codigo)
	{
		this.codigo = codigo;
	}

	public Date getData()
	{
		return data;
	}

	public void setData(Date data)
	{
		this.data = data;
	}
}
