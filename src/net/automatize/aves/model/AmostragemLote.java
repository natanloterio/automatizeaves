package net.automatize.aves.model;

public class AmostragemLote
{
	public static final String CODIGO = "codigo";
	public static final String NR_LOTE = "nrlote";
	public static final String SEXO = "sexo";
	public static final String QTD_BOXES = "qtdboxes";
	public static final String QTD_AVES_POR_BOX = "qtdavesporbox";
	public static final String[] COLUNAS = { CODIGO, NR_LOTE, SEXO, QTD_BOXES,
			QTD_AVES_POR_BOX };

	private int codigo;
	private String numeroLote;
	private String sexo;
	private short qtdBoxes;
	private int qtdAvesPorBox;

	public AmostragemLote()
	{
		super();
	}

	public AmostragemLote(int codigo, String numeroLote, String sexo,
			short qtdBoxes, int qtdAvesPorBox)
	{
		super();
		this.codigo = codigo;
		this.numeroLote = numeroLote;
		this.sexo = sexo;
		this.qtdBoxes = qtdBoxes;
		this.qtdAvesPorBox = qtdAvesPorBox;
	}

	public int getCodigo()
	{
		return codigo;
	}

	public void setCodigo(int codigo)
	{
		this.codigo = codigo;
	}

	public String getNumeroLote()
	{
		return numeroLote;
	}

	public void setNumeroLote(String numeroLote)
	{
		this.numeroLote = numeroLote;
	}

	public String getSexo()
	{
		return sexo;
	}

	public void setSexo(String sexo)
	{
		this.sexo = sexo;
	}

	public short getQtdBoxes()
	{
		return qtdBoxes;
	}

	public void setQtdBoxes(short qtdBoxes)
	{
		this.qtdBoxes = qtdBoxes;
	}

	public int getQtdAvesPorBox()
	{
		return qtdAvesPorBox;
	}

	public void setQtdAvesPorBox(int qtdAvesPorBox)
	{
		this.qtdAvesPorBox = qtdAvesPorBox;
	}

}
