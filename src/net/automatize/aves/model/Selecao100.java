package net.automatize.aves.model;

import java.util.Date;

public class Selecao100
{
	public static final String CODIGO = "codigo";
	public static final String NRLOTE = "nrlote";
	public static final String SEXO = "sexo";
	public static final String DATA = "data";
	// public static final String[] COLUNAS = { CODIGO, NRLOTE, SEXO, DATA };

	private short codigo;
	private String nrlote;
	private String sexo;
	private Date data;

	public short getCodigo()
	{
		return codigo;
	}

	public void setCodigo(short codigo)
	{
		this.codigo = codigo;
	}

	public String getNrlote()
	{
		return nrlote;
	}

	public void setNrlote(String nrlote)
	{
		this.nrlote = nrlote;
	}

	public String getSexo()
	{
		return sexo;
	}

	public void setSexo(String sexo)
	{
		this.sexo = sexo;
	}

	public Date getData()
	{
		return data;
	}

	public void setData(Date data)
	{
		this.data = data;
	}

}
