package net.automatize.aves.model;

public class PreAmostragemClassificacao
{
	public static final String CLASSIFICACAO_CODIGO = "classificacao_codigo";
	public static final String PERCENTUAL = "percentual";
	public static final String QTDPESOS = "qtdpesos";
	public static final String FAIXAINICIAL = "faixainicial";
	public static final String FAIXAFINAL = "faixafinal";
	public static final String[] COLUNAS = { CLASSIFICACAO_CODIGO, PERCENTUAL,
			QTDPESOS, FAIXAINICIAL, FAIXAFINAL };

	private int classificacaoCodigo;
	private int percentual;
	private int qtdPesos;
	private int faixaInicial;
	private int faixaFinal;

	public int getClassificacaoCodigo()
	{
		return classificacaoCodigo;
	}

	public void setClassificacaoCodigo(int classificacaoCodigo)
	{
		this.classificacaoCodigo = classificacaoCodigo;
	}

	public int getPercentual()
	{
		return percentual;
	}

	public void setPercentual(int percentual)
	{
		this.percentual = percentual;
	}

	public int getQtdPesos()
	{
		return qtdPesos;
	}

	public void setQtdPesos(int qtdPesos)
	{
		this.qtdPesos = qtdPesos;
	}

	public int getFaixaInicial()
	{
		return faixaInicial;
	}

	public void setFaixaInicial(int faixaInicial)
	{
		this.faixaInicial = faixaInicial;
	}

	public int getFaixaFinal()
	{
		return faixaFinal;
	}

	public void setFaixaFinal(int faixaFinal)
	{
		this.faixaFinal = faixaFinal;
	}
}
