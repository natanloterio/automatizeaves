package net.automatize.aves.model;

public class AmostragemBox
{
	public static final String CODIGO = "codigo";
	public static final String NRBOX = "nrbox";
	public static final String AMOSTRAGEMLOTE_CODIGO = "amostragemlote_codigo";
	public static final String QTDAVESPESAR = "qtdavespesar";
	public static final String CLASSIFICACAO_CODIGO = "classificacao_codigo";
	public static final String[] COLUNAS = { CODIGO, NRBOX,
			AMOSTRAGEMLOTE_CODIGO, QTDAVESPESAR, CLASSIFICACAO_CODIGO };

	public static final String CLASSIFICACAO_DESCRICAO = "classificacao_descricao";

	private int codigo;
	private short nrbox;
	private int amostragemlote_codigo;
	private int qtdAvesPesar;
	private short classificacao_codigo;
	private String classificacao_descricao;
	private int qtdPesadas;

	public AmostragemBox()
	{
		super();
		// TODO Auto-generated constructor stub
	}

	public AmostragemBox(int codigo, short numero, int amostragemCodigo,
			int qtdAvesPesar, short classificacaoCodigo)
	{
		super();
		this.codigo = codigo;
		this.nrbox = numero;
		this.amostragemlote_codigo = amostragemCodigo;
		this.qtdAvesPesar = qtdAvesPesar;
		this.classificacao_codigo = classificacaoCodigo;
	}

	public AmostragemBox(int codigo, short numero, int amostragemCodigo,
			int qtdAvesPesar, short classificacaoCodigo,
			String classificacaoDescricao)
	{
		super();
		this.codigo = codigo;
		this.nrbox = numero;
		this.amostragemlote_codigo = amostragemCodigo;
		this.qtdAvesPesar = qtdAvesPesar;
		this.classificacao_codigo = classificacaoCodigo;
		this.classificacao_descricao = classificacaoDescricao;
	}

	public int getCodigo()
	{
		return codigo;
	}

	public void setCodigo(int codigo)
	{
		this.codigo = codigo;
	}

	public short getNrbox()
	{
		return nrbox;
	}

	public void setNrbox(short nrbox)
	{
		this.nrbox = nrbox;
	}

	public int getAmostragemlote_codigo()
	{
		return amostragemlote_codigo;
	}

	public void setAmostragemlote_codigo(int amostragemlote_codigo)
	{
		this.amostragemlote_codigo = amostragemlote_codigo;
	}

	public int getQtdAvesPesar()
	{
		return qtdAvesPesar;
	}

	public void setQtdAvesPesar(int qtdAvesPesar)
	{
		this.qtdAvesPesar = qtdAvesPesar;
	}

	public short getClassificacao_codigo()
	{
		return classificacao_codigo;
	}

	public void setClassificacao_codigo(short classificacao_codigo)
	{
		this.classificacao_codigo = classificacao_codigo;
	}

	public String getClassificacao_descricao()
	{
		return classificacao_descricao;
	}

	public void setClassificacao_descricao(String classificacao_descricao)
	{
		this.classificacao_descricao = classificacao_descricao;
	}

	public int getQtdPesadas()
	{
		return qtdPesadas;
	}

	public void setQtdPesadas(int qtdPesadas)
	{
		this.qtdPesadas = qtdPesadas;
	}
}
