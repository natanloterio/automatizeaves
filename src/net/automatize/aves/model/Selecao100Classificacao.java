package net.automatize.aves.model;

public class Selecao100Classificacao
{
	public static final String CLASSIFICACAO_CODIGO = "classificacao_codigo";
	public static final String FAIXAINICIAL = "faixainicial";
	public static final String FAIXAFINAL = "faixafinal";
	public static final String[] COLUNAS = { CLASSIFICACAO_CODIGO,
			FAIXAINICIAL, FAIXAFINAL };

	private short classificacaoCodigo;
	private int faixaInicial;
	private int faixaFinal;

	public short getClassificacaoCodigo()
	{
		return classificacaoCodigo;
	}

	public void setClassificacaoCodigo(short classificacaoCodigo)
	{
		this.classificacaoCodigo = classificacaoCodigo;
	}

	public int getFaixaInicial()
	{
		return faixaInicial;
	}

	public void setFaixaInicial(int faixaInicial)
	{
		this.faixaInicial = faixaInicial;
	}

	public int getFaixaFinal()
	{
		return faixaFinal;
	}

	public void setFaixaFinal(int faixaFinal)
	{
		this.faixaFinal = faixaFinal;
	}

}
