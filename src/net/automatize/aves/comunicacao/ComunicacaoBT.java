package net.automatize.aves.comunicacao;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import net.automatize.aves.util.Constantes;
import net.automatize.aves.util.FuncoesCompartilhadas;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.os.Message;

public class ComunicacaoBT
{
	private BluetoothDevice device = null;
	private ConnectThread connectThread = null;
	private ConnectedThread connectedThread = null;

	private StringBuilder _buffer = null;
	private double peso = 0;
	private String speso = "";
	private boolean bTerminou = false;

	public boolean bConectado = false;

	Handler mHandler = new Handler()
	{
		@Override
		public void handleMessage(Message msg)
		{
			super.handleMessage(msg);
			switch (msg.what) {
			case Constantes.SUCESSO_CONEXAO:
				//FuncoesCompartilhadas.log("CONECTADO");
				bConectado = true;
				connectedThread = new ConnectedThread((BluetoothSocket) msg.obj);
				connectedThread.start();

				break;
			case Constantes.MENSAGEM_LIDA:
				//FuncoesCompartilhadas.log("MENSAGEM_LIDA");
				byte[] data = (byte[]) msg.obj;
				pesoLido(data);

				break;
			case Constantes.DESCONECTADO:
				FuncoesCompartilhadas.log("DESCONECTADO");
				bConectado = false;
				break;
			}
		}
	};

	public double getPeso()
	{
		return peso;
	}

	public String getPesoStr()
	{
		return speso;
	}

	public String getPesoFormatado()
	{
		return getPesoFormatado(Constantes.ASYNCTASK_MSG_PESO);
	}

	public String getPesoFormatado(String label)
	{
		if (peso > 0 && speso != null
				&& speso.contains(Constantes.ASYNCTASK_MSG_PESO))
		{
			return label + " \u0009\u0009"
					+ FuncoesCompartilhadas.formataPeso(peso);
		} else
			return FuncoesCompartilhadas.formataPeso(peso);
	}

	private void pesoLido(byte[] data)
	{
		for (int i = 0; i < data.length; i++)
		{
			if (data[i] == 1)
			{
				_buffer = new StringBuilder();
				bTerminou = false;
			}

			if (data[i] == 2)
			{
				bTerminou = true;
				break;
			}
		}

		_buffer.append(new String(data));
		
		//FuncoesCompartilhadas.log(new String(data));

		if (bTerminou)
		{
			char[] dataFinal = _buffer.toString().toCharArray();
			speso = "";

			for (int i = 0; i < dataFinal.length; i++)
			{
				if (Character.isDigit(dataFinal[i])
						|| dataFinal[i] == Constantes.ASCII_PONTO
						|| dataFinal[i] == Constantes.ASCII_LETRA_P)
					speso += dataFinal[i];
			}

			peso = 0;
			if (FuncoesCompartilhadas.validaPeso(speso))
			{
				peso = Double.parseDouble(speso.replace(
						Constantes.ASYNCTASK_MSG_PESO, ""));
			}
		}
	}

	public boolean isConnected()
	{
		if (connectedThread == null)
			bConectado = false;

		return bConectado;
	}

	public void peso5()
	{
		//FuncoesCompartilhadas.log("Constantes.PESO5");
		if (connectedThread != null)
			connectedThread.EnviaComando(Constantes.PESO5);
	}

	public void peso6()
	{
		//FuncoesCompartilhadas.log("Constantes.PESO6");
		if (connectedThread != null)
			connectedThread.EnviaComando(Constantes.PESO6);
	}

	public void tarar()
	{
		if (connectedThread != null)
			connectedThread.EnviaComandoTarar();
	}

	public void fechar()
	{
		if (connectedThread != null && bConectado)
			connectedThread.resetConnection();

		bConectado = false;
	}

	public void enviaComando(String comando)
	{
		if (connectedThread != null)
			connectedThread.EnviaComandoCalibracao(comando, 0);
	}

	public void enviaComando(int comando)
	{
		if (connectedThread != null)
			connectedThread.EnviaComando(comando);
	}

	public void inicializaComunicacaoBalanca(String smacaddress)
	{
		device = Constantes.btAdapter.getRemoteDevice(smacaddress);
		if (device != null)
		{
			connectThread = new ConnectThread(device);
			connectThread.start();
		}
	}

	private class ConnectThread extends Thread
	{
		private final BluetoothSocket mmSocket;
		private final BluetoothDevice mmDevice;

		public ConnectThread(BluetoothDevice device)
		{
			// Use a temporary object that is later assigned to mmSocket,
			// because mmSocket is final
			BluetoothSocket tmp = null;
			mmDevice = device;
			FuncoesCompartilhadas.log("construct");
			// Get a BluetoothSocket to connect with the given BluetoothDevice
			try
			{
				// MY_UUID is the app's UUID string, also used by the server
				// code
				tmp = device
						.createRfcommSocketToServiceRecord(Constantes.MY_UUID);
			} catch (IOException e)
			{
				FuncoesCompartilhadas.log("get socket failed");
			}
			mmSocket = tmp;
		}

		@Override
		public void run()
		{
			// Cancel discovery because it will slow down the connection
			Constantes.btAdapter.cancelDiscovery();
			// Log.i(TAG, "connect - run");
			try
			{
				// Connect the device through the socket. This will block
				// until it succeeds or throws an exception
				mmSocket.connect();
				FuncoesCompartilhadas.log("connect - succeeded");

			} catch (IOException connectException)
			{
				FuncoesCompartilhadas.log("connect failed - "
						+ connectException.getMessage());
				// Unable to connect; close the socket and get out
				try
				{
					mmSocket.close();
				} catch (IOException closeException)
				{
				}
				return;
			}

			// Do work to manage the connection (in a separate thread)
			mHandler.obtainMessage(Constantes.SUCESSO_CONEXAO, mmSocket)
					.sendToTarget();
		}

		/** Will cancel an in-progress connection, and close the socket */
		public void cancel()
		{
			try
			{
				mmSocket.close();
				// Do work to manage the connection (in a separate thread)
				mHandler.obtainMessage(Constantes.DESCONECTADO, mmSocket)
						.sendToTarget();

			} catch (IOException e)
			{
			}
		}
	}

	private class ConnectedThread extends Thread
	{
		private final BluetoothSocket mmSocket;
		private final InputStream mmInStream;
		private final OutputStream mmOutStream;

		public ConnectedThread(BluetoothSocket socket)
		{
			mmSocket = socket;
			InputStream tmpIn = null;
			OutputStream tmpOut = null;

			// Get the input and output streams, using temp objects because
			// member streams are final
			try
			{
				tmpIn = socket.getInputStream();
				tmpOut = socket.getOutputStream();
			} catch (IOException e)
			{
			}

			mmInStream = tmpIn;
			mmOutStream = tmpOut;
		}

		@Override
		public void run()
		{
			byte[] buffer; // buffer store for the stream
			int bytes; // bytes returned from read()

			// Keep listening to the InputStream until an exception occurs
			while (true)
			{
				try
				{
					// Read from the InputStream
					buffer = new byte[128];
					bytes = mmInStream.read(buffer);

					// Send the obtained bytes to the UI activity
					mHandler.obtainMessage(Constantes.MENSAGEM_LIDA, bytes, -1,
							buffer).sendToTarget();

				} catch (IOException e)
				{
					break;
				}
			}
		}

		private void EnviaComando(int c)
		{
			try
			{
				try
				{
					mmOutStream.write(c);
					Thread.sleep(100);

				} catch (InterruptedException e)
				{
					Thread.currentThread().interrupt();
					FuncoesCompartilhadas.log(e.getMessage());
				}
			} catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
				/* em teste */
				resetConnection();
			}
		}

		private void EnviaComandoTarar()
		{
			try
			{
				try
				{
					char c = Constantes.TARAR.toCharArray()[0];
					mmOutStream.write(c);
					Thread.sleep(250);

				} catch (InterruptedException e)
				{
					Thread.currentThread().interrupt();
					FuncoesCompartilhadas.log(e.getMessage());
				}
			} catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
				/* em teste */
				resetConnection();
			}
		}

		private void EnviaComandoCalibracao(final String comando, final int _c)
		{
			try
			{
				try
				{
					Thread.sleep(Constantes.CALIBRACAO_TEMPO_ENVIO_ANTES);
					if (_c > 0)
					{
						mmOutStream.write(_c);
						Thread.sleep(Constantes.CALIBRACAO_TEMPO_ENVIO_DEPOIS);
					} else
					{
						for (char c : comando.toCharArray())
						{
							mmOutStream.write(c);
							Thread.sleep(Constantes.CALIBRACAO_TEMPO_ENVIO_DEPOIS);
						}
					}
				} catch (IOException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
					/* em teste */
					resetConnection();
				}

			} catch (InterruptedException e)
			{
				// TODO Auto-generated catch block
				Thread.currentThread().interrupt();
				FuncoesCompartilhadas.log(e.getMessage());
			}
		}

		/* Call this from the main activity to shutdown the connection */

		public void resetConnection()
		{
			if (mmInStream != null)
			{
				try
				{
					mmInStream.close();
				} catch (Exception e)
				{
				}
			}

			if (mmOutStream != null)
			{
				try
				{
					mmOutStream.close();
				} catch (Exception e)
				{
				}
			}

			if (mmSocket != null)
			{
				try
				{
					mmSocket.close();
				} catch (Exception e)
				{
				}
			}

			connectThread.cancel();
			bConectado = false;
		}
	}
}