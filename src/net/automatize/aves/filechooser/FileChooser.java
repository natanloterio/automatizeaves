package net.automatize.aves.filechooser;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import net.automatize.aves.R;
import net.automatize.aves.util.Constantes;
import net.automatize.aves.util.FuncoesCompartilhadas;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

public class FileChooser extends ListActivity
{
	public static final String FILECHOOSER_KEY = "path";
	private final String FOLDER = "Pasta";
	private final String PARENT_DIRECTORY = "Diretório superior";
	private File currentDir;
	private FileArrayAdapter adapter;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		currentDir = new File(Environment.getExternalStorageDirectory()
				.getPath());
		fill(currentDir);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.view_chooser, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Handle presses on the action bar items
		switch (item.getItemId()) {
		case R.id.chooser_ok:

			Intent i = getIntent();
			i.putExtra(FILECHOOSER_KEY, currentDir.getPath());
			setResult(RESULT_OK, i);
			finish();

			return true;
		case R.id.choooser_cancel:
			setResult(RESULT_CANCELED);
			finish();

			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id)
	{
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		Option o = adapter.getItem(position);
		if (o.getData().equalsIgnoreCase(FOLDER)
				|| o.getData().equalsIgnoreCase(PARENT_DIRECTORY))
		{
			currentDir = new File(o.getPath());
			fill(currentDir);
		}
	}

	private void fill(File f)
	{
		if (f.canWrite() == false)
		{
			FuncoesCompartilhadas.exibirToastMsgLong(this,
					Constantes.MSG_ERRO_PERMISSAO_PASTA);
			return;
		}

		File[] dirs = f.listFiles();
		this.setTitle("Diretório atual: " + f.getName());
		List<Option> dir = new ArrayList<Option>();

		try
		{
			for (File ff : dirs)
			{
				if (ff.isDirectory())
					dir.add(new Option(ff.getName(), FOLDER, ff
							.getAbsolutePath()));
			}
		} catch (Exception e)
		{

		}
		Collections.sort(dir);

		if (!f.getName().equalsIgnoreCase("sdcard"))
			dir.add(0, new Option("..", PARENT_DIRECTORY, f.getParent()));
		adapter = new FileArrayAdapter(FileChooser.this,
				R.layout.chooser_file_view, dir);
		this.setListAdapter(adapter);
	}
}