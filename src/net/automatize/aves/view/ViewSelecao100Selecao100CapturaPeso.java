package net.automatize.aves.view;

import java.util.ArrayList;

import net.automatize.aves.R;
import net.automatize.aves.comunicacao.ComunicacaoBT;
import net.automatize.aves.dao.DAOSelecao100Classificacao;
import net.automatize.aves.dao.DAOSelecao100Pesos;
import net.automatize.aves.dao.DAOTipoPesagem;
import net.automatize.aves.model.Selecao100Peso;
import net.automatize.aves.model.TipoPesagem;
import net.automatize.aves.util.Constantes;
import net.automatize.aves.util.FuncoesCompartilhadas;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

public class ViewSelecao100Selecao100CapturaPeso extends Activity
{
	final Context context = this;

	/* Objetos globais */
	private DAOSelecao100Pesos dao = new DAOSelecao100Pesos(context);
	private DAOSelecao100Classificacao daoClass = new DAOSelecao100Classificacao(
			context);

	/* Widget */
	private TextView tvStatus;
	private TextView tvStatusPesosBalanca;
	private TextView tvTipoPesagem;
	private Button btnCapturar;
	private Button btnTarar;
	private Button btnEliminar;
	private Button btnProxBox;
	private Button btnSair;
	private EditText edtAvesPesadas;
	private EditText edtPesoTotal;
	private EditText edtPesoMedio;
	private EditText edtMenorPeso;
	private EditText edtMaiorPeso;

	/* Lista de pesos */
	private Selecao100PesoAdapter adapterPesos = null;
	private ArrayList<Selecao100Peso> listaPesos = new ArrayList<Selecao100Peso>();

	/* Parametros e valores da pesagem */
	private int itotalAvesPesadas;
	private double dpesoTotalAvesPesadas;
	private double dpesoMedioAvesPesadas;
	private double dmenorPesoAvesPesadas;
	private double dmaiorPesoAvesPesadas;

	/* Bluetooth e comunica��o com balan�a */
	private TipoPesagem _tipoPesagem;
	private CapturaPesoAsyncTask mCapturaPesoAsyncTask = null;
	private ComunicacaoBT comunicacao = new ComunicacaoBT();
	private String macAddress = null;

	/* variaveis de controle */
	private boolean bPesagemAutomatica = false;
	private boolean bPararAssincrono = false;
	private boolean bEncontrouPesoCapturado = false;
	private boolean bJaGravoPesoCapturado = false;
	private boolean bCapturarPeso = false;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_amostragem_semanal_captura_peso);

		_tipoPesagem = FuncoesCompartilhadas
				.parametrosPesagemConfigurados(context);

		if (_tipoPesagem == null)
		{
			finish();
			return;
		}

		tvTipoPesagem = (TextView) findViewById(R.id.amostragemsemanal_capturapeso_tvTipoPesagem);
		tvStatusPesosBalanca = (TextView) findViewById(R.id.amostragemsemanal_capturapeso_tvStatusPesosBalanca);

		switch (_tipoPesagem.getTipoPesagem()) {
		case Constantes.TIPO_PESAGEM_AUTOMATICO:
			tvTipoPesagem.setText("Tipo de Pesagem "
					+ Constantes.TIPO_PESAGEM_AUTOMATICO_DESC);

			bPesagemAutomatica = true;
			break;

		default:
			tvTipoPesagem.setText("Tipo de Pesagem "
					+ Constantes.TIPO_PESAGEM_MANUAL_DESC);
			break;
		}

		inicializaComunicacaoBalanca();

		/* Lista de pesos */
		final ListView lvPeso = (ListView) findViewById(R.id.amostragemsemanal_capturapeso_lvPesos);

		listarPesos();

		adapterPesos = new Selecao100PesoAdapter(context,
				R.layout.activity_view_selecao100_pesos_grid_item, listaPesos);

		lvPeso.setAdapter(adapterPesos);

		/* Bot�es */

		/* Capturar */
		btnCapturar = (Button) findViewById(R.id.amostragemsemanal_capturapeso_btnCapturar);

		if (_tipoPesagem.getTipoPesagem() == Constantes.TIPO_PESAGEM_AUTOMATICO)
		{
			btnCapturar.setText("Iniciar");
		}

		btnCapturar.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{
				if (_tipoPesagem.getTipoPesagem() == Constantes.TIPO_PESAGEM_AUTOMATICO)
				{
					btnCapturar.setEnabled(false);
				}

				if (comunicacao.isConnected())
					solicitaPeso();
			}
		});

		/* Tarar */
		btnTarar = (Button) findViewById(R.id.amostragemsemanal_capturapeso_btnTarar);
		btnTarar.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{
				tarar();
			}
		});

		/* Eliminar */
		btnEliminar = (Button) findViewById(R.id.amostragemsemanal_capturapeso_btnLimpar);
		btnEliminar.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				limparUltimoPeso();
			}
		});

		/* Pr�ximo box */
		btnProxBox = (Button) findViewById(R.id.amostragemsemanal_capturapeso_btnProxBox);
		btnProxBox.setEnabled(false);
		// btnProxBox.setOnClickListener(new View.OnClickListener()
		// {
		//
		// @Override
		// public void onClick(View arg0)
		// {
		// selecionarProximoBox();
		// }
		// });

		/* Reiniciar */
		// btnReiniciar = (Button)
		// findViewById(R.id.amostragemsemanal_capturapeso_btnReiniciar);
		// btnReiniciar.setEnabled(false);
		// btnReiniciar.setOnClickListener(new View.OnClickListener()
		// {
		//
		// @Override
		// public void onClick(View arg0)
		// {
		// reiniciar();
		// }
		// });

		/* Sair */
		btnSair = (Button) findViewById(R.id.amostragemsemanal_capturapeso_btnSair);
		btnSair.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				if (mCapturaPesoAsyncTask != null)
					mCapturaPesoAsyncTask.cancel(true);
				finish();
			}
		});
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		comunicacao.fechar();
	}

	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		comunicacao.fechar();
	}

	private void atualizarTvStatusPesagem()
	{
		tvStatus = (TextView) findViewById(R.id.amostragemsemanal_capturapeso_tvStatus);
		tvStatus.setText(Constantes.MSG_SELECAO_CAPTURAPESO_TITLE
				+ listaPesos.size());
	}

	private void atualizaPesagemTotais()
	{
		itotalAvesPesadas = 0;
		reiniciarEstatisticasBox();

		for (int i = 0; i < listaPesos.size(); i++)
		{
			/* total de aves */
			itotalAvesPesadas++;

			/* somatorio de todos os pesos */
			dpesoTotalAvesPesadas = dpesoTotalAvesPesadas
					+ listaPesos.get(i).getPeso();
			/* m�dia de peso */
			dpesoMedioAvesPesadas = (dpesoTotalAvesPesadas / itotalAvesPesadas);

			/* maior peso */
			if (dmaiorPesoAvesPesadas == 0
					|| listaPesos.get(i).getPeso() > dmaiorPesoAvesPesadas)
				dmaiorPesoAvesPesadas = listaPesos.get(i).getPeso();
			/* menor peso */
			if (dmenorPesoAvesPesadas == 0
					|| listaPesos.get(i).getPeso() < dmenorPesoAvesPesadas)
				dmenorPesoAvesPesadas = listaPesos.get(i).getPeso();
		}

		edtAvesPesadas = (EditText) findViewById(R.id.amostragemsemanal_capturapeso_edtAvesPesadas);
		edtAvesPesadas.setText(String.valueOf(itotalAvesPesadas));

		edtPesoTotal = (EditText) findViewById(R.id.amostragemsemanal_capturapeso_edtPesoTotal);
		edtPesoTotal.setText(FuncoesCompartilhadas
				.formataPeso(dpesoTotalAvesPesadas));

		edtPesoMedio = (EditText) findViewById(R.id.amostragemsemanal_capturapeso_edtPesoMedio);
		edtPesoMedio.setText(FuncoesCompartilhadas
				.formataPeso(dpesoMedioAvesPesadas));

		edtMenorPeso = (EditText) findViewById(R.id.amostragemsemanal_capturapeso_edtMenorPeso);
		edtMenorPeso.setText(FuncoesCompartilhadas
				.formataPeso(dmenorPesoAvesPesadas));

		edtMaiorPeso = (EditText) findViewById(R.id.amostragemsemanal_capturapeso_edtMaiorPeso);
		edtMaiorPeso.setText(FuncoesCompartilhadas
				.formataPeso(dmaiorPesoAvesPesadas));
	}

	private void inicializaComunicacaoBalanca()
	{
		if (_tipoPesagem != null)
		{
			if (_tipoPesagem.getMacAddress() == null)
			{
				finish();
			} else
			{
				if (macAddress == null)
					macAddress = new DAOTipoPesagem(context).getMacAddress();

				if (macAddress == null)
				{
					FuncoesCompartilhadas.exibirToastMsgLong(context,
							Constantes.MSG_BALANCA_NAO_CONFIGURADA);
				} else
				{
					mCapturaPesoAsyncTask = new CapturaPesoAsyncTask();
					mCapturaPesoAsyncTask.execute(macAddress);
				}
			}
		}
	}

	private void solicitaPeso()
	{
		posSolicitaPeso();
	}

	private void posSolicitaPeso()
	{
		bCapturarPeso = true;

		if (comunicacao.isConnected())
			comunicacao.peso5();
	}

	private void tarar()
	{
		comunicacao.tarar();
	}

	private void limparUltimoPeso()
	{
		if (listaPesos.isEmpty())
		{
			FuncoesCompartilhadas.exibirToastMsgLong(context,
					"N�o h� pesos para serem eliminados!");
			return;
		}

		// AlertDialog.Builder dialog = new AlertDialog.Builder(context);
		// dialog.setTitle(Constantes.MSG_CONFIRMACAO_EXCLUSAO_TITLE);
		// dialog.setMessage("Confirma a exclus�o do �ltimo peso?");

		final AlertDialog.Builder dialog = FuncoesCompartilhadas
				.criaAlertDialogCustom(context,
						Constantes.MSG_CONFIRMACAO_EXCLUSAO_MSG,
						Constantes.MSG_CONFIRMACAO_LIMPEZA_ULTIMO_PESO);

		dialog.setIcon(android.R.drawable.ic_delete);
		dialog.setPositiveButton("Sim", new DialogInterface.OnClickListener()
		{

			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				apagarUltimoPeso();
			}
		});

		dialog.setNegativeButton("N�o", null);
		dialog.create().show();
	}

	private void listarPesos()
	{
		listaPesos.clear();
		listaPesos.addAll(dao.getSelecao100Pesos());

		if (adapterPesos != null)
			adapterPesos.notifyDataSetChanged();

		atualizarTvStatusPesagem();
		atualizaPesagemTotais();
	}

	private void apagarUltimoPeso()
	{
		if (listaPesos.isEmpty() == false)
		{
			// int selecao100_codigo = listaPesos.get(listaPesos.size() - 1)
			// .getCodigo();

			int selecao100_codigo = listaPesos.get(0).getCodigo();

			if (dao.excluir(selecao100_codigo) == false)
			{
				FuncoesCompartilhadas.exibirToastMsgLong(context,
						Constantes.MSG_ERRO_GRAVACAO);
			} else
			{
				listarPesos();
			}
		}
	}

	private void reiniciarEstatisticasBox()
	{
		itotalAvesPesadas = 0;
		dpesoTotalAvesPesadas = 0;
		dpesoMedioAvesPesadas = 0;
		dmenorPesoAvesPesadas = 0;
		dmaiorPesoAvesPesadas = 0;
	}

	private void validaPeso()
	{
		boolean bOk = true;
		String speso = comunicacao.getPesoStr();

		if (FuncoesCompartilhadas.validaPeso(speso))
		{
			if (speso.contains(Constantes.ASYNCTASK_MSG_PESO))
			{
				double peso = Double.parseDouble(speso.replace(
						Constantes.ASYNCTASK_MSG_PESO, ""));
				Log.i("PESO", String.valueOf(peso));

				if (peso > 0)
				{
					// if (_tipoPesagem.getTipoPesagem() ==
					// Constantes.TIPO_PESAGEM_AUTOMATICO)
					// {
					// if (peso < FuncoesCompartilhadas.grToKg(_tipoPesagem
					// .getPesoMinimo()))
					// {
					// bOk = false;
					// }
					// }

					if (isPesoMinimo(peso))
					{
						bOk = false;
					}

					if (bOk)
					{
						String retorno = dao.lancaPeso(FuncoesCompartilhadas
								.kgToGr(peso));
						if (retorno != null)
						{
							FuncoesCompartilhadas.log(retorno);
							FuncoesCompartilhadas.exibirToastMsgLong(context,
									retorno);
						}

						listarPesos();
						bJaGravoPesoCapturado = true;
					}
				}
			}
		}

		bCapturarPeso = false;
		if (bPesagemAutomatica)
		{
			bCapturarPeso = true;
			solicitaPeso();
		}
	}

	private boolean isPesoMinimo(double peso)
	{
		if (peso > 0)
		{
			if (_tipoPesagem.getTipoPesagem() == Constantes.TIPO_PESAGEM_AUTOMATICO)
			{
				if (peso < FuncoesCompartilhadas.grToKg(_tipoPesagem
						.getPesoMinimo()))
				{
					return true;
				}
			}
		}

		return false;
	}

	private class CapturaPesoAsyncTask extends AsyncTask<String, String, Void>
	{

		/* antes */
		@Override
		protected void onPreExecute()
		{
			if (comunicacao.isConnected() == false)
			{
				publishProgress("Conectando...");
			}
		}

		@Override
		protected Void doInBackground(String... params)
		{
			do
			{
				if (comunicacao.isConnected() == false)
				{
					publishProgress("Conectando...");
					comunicacao.inicializaComunicacaoBalanca(params[0]);

					int count = 0;
					while (comunicacao.bConectado == false
							&& count < Constantes.TEMPO_TENTATIVA_CONEXAO)
					{
						try
						{
							Thread.sleep(100);
						} catch (InterruptedException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						count += 100;
					}

					if (comunicacao.bConectado == false)
					{
						FuncoesCompartilhadas.log("Conexao falhou");
						publishProgress("Conex�o falhou!",
								Constantes.ASYNCTASK_MSG_FALHA);
						try
						{
							Thread.sleep(1000);
						} catch (InterruptedException e)
						{
							e.printStackTrace();
						}

						cancel(true);

					} else
					{
						FuncoesCompartilhadas.log("Conex�o OK!");
					}
				}

				if (isCancelled())
					break;

				comunicacao.peso6();
				publishProgress(comunicacao.getPesoFormatado(daoClass
						.reconheceClassificacao(comunicacao.getPeso())));

				if (bCapturarPeso
						&& comunicacao.getPesoStr().contains(
								Constantes.ASYNCTASK_MSG_PESO) == false)
				{
					bEncontrouPesoCapturado = false;
					bJaGravoPesoCapturado = false;

				} else if (comunicacao.getPeso() > 0
						&& comunicacao.getPesoStr().contains(
								Constantes.ASYNCTASK_MSG_PESO) && bCapturarPeso
						&& bEncontrouPesoCapturado == false
						&& bJaGravoPesoCapturado == false)
				{
					bEncontrouPesoCapturado = true;

					if (isPesoMinimo(comunicacao.getPeso()))
					{
						publishProgress(Constantes.MSG_PESO_MENOR_MINIMO);
						try
						{
							Thread.sleep(2000);
						} catch (InterruptedException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						validaPeso();

					} else
					{
						publishProgress(
								FuncoesCompartilhadas.formataPeso(comunicacao
										.getPeso()),
								Constantes.ASYNCTASK_MSG_PESO);
					}
				}
			} while (bPararAssincrono == false);

			return null;
		}

		/* atualizar a interface */
		@Override
		protected void onProgressUpdate(String... values)
		{
			super.onProgressUpdate(values);
			if (values.length > 1) // tem o 'P' como segundo argumento
			{
				if (values[1].contains(Constantes.ASYNCTASK_MSG_PESO))
					validaPeso();
			}
			tvStatusPesosBalanca.setText(values[0]);
		}
	}

	private class Selecao100PesoAdapter extends ArrayAdapter<Selecao100Peso>
	{
		private ArrayList<Selecao100Peso> entries;
		private Context context;

		public Selecao100PesoAdapter(Context context, int textViewResourceId,
				ArrayList<Selecao100Peso> entries)
		{
			super(context, textViewResourceId, entries);
			this.entries = entries;
			this.context = context;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent)
		{
			View v = convertView;
			final ViewHolder holder;
			if (v == null)
			{
				LayoutInflater vi = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

				v = vi.inflate(
						R.layout.activity_view_selecao100_pesos_grid_item, null);

				holder = new ViewHolder();
				holder.item1 = (TextView) v
						.findViewById(R.id.selecao100_peso_peso);

				holder.item2 = (TextView) v
						.findViewById(R.id.selecao100_peso_sigla);

				v.setTag(holder);
			} else
				holder = (ViewHolder) v.getTag();

			final Selecao100Peso custom = entries.get(position);
			if (custom != null)
			{
				holder.item1.setText(FuncoesCompartilhadas
						.grToKgFormatado(custom.getPeso()));
				holder.item2.setText(custom.getClassificacao_sigla());
			}
			return v;
		}

		public class ViewHolder
		{
			public TextView item1;
			public TextView item2;
		}
	}
}