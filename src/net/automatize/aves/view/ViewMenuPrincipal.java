package net.automatize.aves.view;

import net.automatize.aves.R;
import net.automatize.aves.dao.DAOClassificacao;
import net.automatize.aves.model.Classificacao;
import net.automatize.aves.util.Constantes;
import net.automatize.aves.util.FuncoesCompartilhadas;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ViewMenuPrincipal extends Activity
{
	final Context context = this;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_principal);

		setTitle(Constantes.TITULO_APLICACAO);
		
		final Button btnAmostragem = (Button) findViewById(R.id.btnLimparMemoriaPesos);
		final Button btnPreAmostragem = (Button) findViewById(R.id.btnClassificacoes);
		final Button btnSelecao100 = (Button) findViewById(R.id.btnParametros);
		final Button btnUtilitarios = (Button) findViewById(R.id.btnUtilitarios);
		final Button btnFinalizar = (Button) findViewById(R.id.btnFinalizar);

		btnAmostragem.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				Intent intent = new Intent(ViewMenuPrincipal.this,
						ViewAmostragem.class);
				startActivity(intent);
			}
		});

		btnPreAmostragem.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				Intent intent = new Intent(ViewMenuPrincipal.this,
						ViewPreAmostragem.class);
				startActivity(intent);
			}
		});

		btnSelecao100.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				Intent intent = new Intent(ViewMenuPrincipal.this,
						ViewSelecao100.class);
				startActivity(intent);
			}
		});

		btnUtilitarios.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				Intent intent = new Intent(ViewMenuPrincipal.this,
						ViewUtilitarios.class);
				startActivity(intent);
			}
		});

		btnFinalizar.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				finish();
			}
		});

		if (FuncoesCompartilhadas.bluetoothIsSupported(context) == false)
		{
			finish();
		} else
		{
			FuncoesCompartilhadas.inicializaDiretorios();
			inicializaDados();
		}
	}

	/* PROC_INICIALIZA_DADOS */
	private void inicializaDados()
	{
		DAOClassificacao dao = new DAOClassificacao(context);

		Classificacao classificacaoFB = dao.getClassificacao((short) -1);
		if (classificacaoFB == null)
		{
			dao.inserir(new Classificacao((short) -1, "FB"));
		} else if (classificacaoFB.getSigla().equals("FB") == false)
		{
			dao.alterar(new Classificacao((short) -1, "FB"));
		}

		Classificacao classificacaoFA = dao.getClassificacao((short) 0);
		if (classificacaoFA == null)
		{
			dao.inserir(new Classificacao((short) 0, "FA"));
		} else if (classificacaoFA.getSigla().equals("FA") == false)
		{
			dao.alterar(new Classificacao((short) 0, "FA"));
		}
	}
}
