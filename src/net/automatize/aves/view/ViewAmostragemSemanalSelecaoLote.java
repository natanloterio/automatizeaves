package net.automatize.aves.view;

import java.util.ArrayList;

import net.automatize.aves.R;
import net.automatize.aves.adapter.AmostragemLotesAdapter;
import net.automatize.aves.dao.ConexaoManager;
import net.automatize.aves.dao.DAOAmostragemBox;
import net.automatize.aves.dao.DAOAmostragemLotes;
import net.automatize.aves.dao.DAOUtilitarios;
import net.automatize.aves.model.AmostragemLote;
import net.automatize.aves.util.Constantes;
import net.automatize.aves.util.FuncoesCompartilhadas;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

public class ViewAmostragemSemanalSelecaoLote extends Activity
{
	private Context context = this;

	private ArrayAdapter<CharSequence> adapterSexo = null;

	private ArrayList<AmostragemLote> _dataList = null;
	private AmostragemLote _amostragemLoteselecionada = null;
	private AmostragemLotesAdapter adapter = null;
	private DAOAmostragemLotes dao = new DAOAmostragemLotes(
			ViewAmostragemSemanalSelecaoLote.this);

	/* Campos */
	private EditText edtLoteLote;
	private Spinner spLoteSexo;
	private EditText edtLoteQtdBoxes;
	private EditText edtLoteQtdAvesBox;

	/* Indices para o context menu */
	private final int LOTE_CONTEXT_MENU_ALTERAR = 0;
	private final int LOTE_CONTEXT_MENU_EXCLUIR = 1;
	private final int LOTE_CONTEXT_MENU_VISUALIZAR_RESULTADO = 2;
	private final int LOTE_CONTEXT_MENU_LIMPAR_PESOS_LOTE = 3;
	private final int LOTE_CONTEXT_MENU_CANCELAR = 4;

	private final String CADASTRO_LOTE_TITLE = "Cadastro de Lote";

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_amostragem_semanal_selecaolote);

		adapterSexo = ArrayAdapter.createFromResource(context,
				R.array.sexo_array, R.layout.spinner_item);

		_dataList = new ArrayList<AmostragemLote>();
		final ListView lv = (ListView) findViewById(R.id.lvAmostragemSemanalLotes);

		adapter = new AmostragemLotesAdapter(context,
				R.id.lvAmostragemSemanalLotes, _dataList);

		carregaDados();

		lv.setAdapter(adapter);

		registerForContextMenu(lv);

		lv.setOnItemClickListener(new OnItemClickListener()
		{

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long id)
			{
				_amostragemLoteselecionada = _dataList.get(position);

				if (_amostragemLoteselecionada != null)
				{
					if (temDadosAmostragem() == false)
					{
						Intent i = new Intent(
								context,
								ViewAmostragemSemanalConfiguracaoBoxesLote.class);
						i.putExtra(AmostragemLote.CODIGO,
								_amostragemLoteselecionada.getCodigo());
						startActivity(i);
					}
				}
			}
		});

		/* Menu de contexto */

		lv.setOnItemLongClickListener(new OnItemLongClickListener()
		{

			@Override
			public boolean onItemLongClick(AdapterView<?> adapter, View view,
					int position, long id)
			{
				_amostragemLoteselecionada = _dataList.get(position);
				return false;
			}
		});

		final Button btnNovoLote = (Button) findViewById(R.id.btnAmostragemSemanalNovoLote);
		btnNovoLote.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{
				_amostragemLoteselecionada = null;

				final Dialog dialogLoteCadastro = new Dialog(context);
				dialogLoteCadastro.setTitle(CADASTRO_LOTE_TITLE);
				dialogLoteCadastro.setCanceledOnTouchOutside(false);
				dialogLoteCadastro
						.setContentView(R.layout.activity_view_lote_cadastro);

				edtLoteLote = (EditText) dialogLoteCadastro
						.findViewById(R.id.lote_cadastro_edtLote);

				spLoteSexo = (Spinner) dialogLoteCadastro
						.findViewById(R.id.lote_cadastro_spSexo);

				spLoteSexo.setAdapter(adapterSexo);

				edtLoteQtdBoxes = (EditText) dialogLoteCadastro
						.findViewById(R.id.lote_cadastro_edtQtde_boxes);

				edtLoteQtdAvesBox = (EditText) dialogLoteCadastro
						.findViewById(R.id.lote_cadastro_edtQtde_aves_box);

				final Button btnGravar = (Button) dialogLoteCadastro
						.findViewById(R.id.lote_cadastro_btnSalvar);
				btnGravar.setOnClickListener(new View.OnClickListener()
				{

					@Override
					public void onClick(View v)
					{
						if (gravar())
						{
							dialogLoteCadastro.dismiss();
							carregaDados();
						}
					}
				});

				final Button btnCancelar = (Button) dialogLoteCadastro
						.findViewById(R.id.lote_cadastro_btnCancelar);
				btnCancelar.setOnClickListener(new View.OnClickListener()
				{

					@Override
					public void onClick(View v)
					{
						dialogLoteCadastro.dismiss();
					}
				});

				dialogLoteCadastro.show();
			}
		});

		final Button btnRetornar = (Button) findViewById(R.id.btnAmostragemSemanalVoltar);
		btnRetornar.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				finish();
			}
		});

	}

	// @Override
	// public boolean onCreateOptionsMenu(Menu menu)
	// {
	// // Inflate the menu; this adds items to the action bar if it is present.
	// getMenuInflater().inflate(R.menu.view_amostragem_semanal, menu);
	// return true;
	// }

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo)
	{
		super.onCreateContextMenu(menu, v, menuInfo);

		if (temDadosAmostragem() == false)
		{
			menu.setHeaderTitle(Constantes.MSG_TITLE_MENU_CONTEXT);
			menu.add(0, v.getId(), LOTE_CONTEXT_MENU_ALTERAR, "Alterar");
			menu.add(0, v.getId(), LOTE_CONTEXT_MENU_EXCLUIR, "Excluir");
			menu.add(0, v.getId(), LOTE_CONTEXT_MENU_VISUALIZAR_RESULTADO,
					"Visualizar Resultado");
			menu.add(0, v.getId(), LOTE_CONTEXT_MENU_LIMPAR_PESOS_LOTE,
					"Limpar Pesos do Lote");
			menu.add(0, v.getId(), LOTE_CONTEXT_MENU_CANCELAR, "Cancelar");
		}
	}

	@Override
	public boolean onContextItemSelected(MenuItem item)
	{
		switch (item.getOrder()) {
		case LOTE_CONTEXT_MENU_ALTERAR:
			if (_amostragemLoteselecionada != null)
				carregaDadosLote();
			break;

		case LOTE_CONTEXT_MENU_EXCLUIR:
			if (_amostragemLoteselecionada != null)
			{
				final AlertDialog.Builder dialog = FuncoesCompartilhadas
						.criaAlertDialogCustom(context,
								Constantes.MSG_CONFIRMACAO_EXCLUSAO_MSG,
								Constantes.MSG_CONFIRMACAO_EXCLUSAO_TITLE);

				dialog.setIcon(android.R.drawable.ic_delete);
				dialog.setPositiveButton("Sim",
						new DialogInterface.OnClickListener()
						{

							@Override
							public void onClick(DialogInterface arg0, int arg1)
							{
								if (excluir() == false)
								{
									FuncoesCompartilhadas.exibirToastMsgLong(
											context,
											Constantes.MSG_ERRO_GRAVACAO);
								}
								{
									FuncoesCompartilhadas
											.exibirToastMsgLong(
													context,
													Constantes.MSG_REGISTRO_EXCLUIDO_SUCESSO);
									carregaDados();
								}
							}
						});

				dialog.setNegativeButton("N�o", null);

				dialog.create().show();

			}

			break;

		case LOTE_CONTEXT_MENU_VISUALIZAR_RESULTADO:
			if (_amostragemLoteselecionada != null)
			{
				Intent intent = new Intent(context,
						ViewAmostragemSemanalResultado.class);
				intent.putExtra(AmostragemLote.CODIGO,
						_amostragemLoteselecionada.getCodigo());
				intent.putExtra(AmostragemLote.NR_LOTE,
						_amostragemLoteselecionada.getNumeroLote());

				startActivity(intent);
			}

			break;
		case LOTE_CONTEXT_MENU_LIMPAR_PESOS_LOTE:
			if (_amostragemLoteselecionada != null)
			{
				final AlertDialog.Builder dialog = FuncoesCompartilhadas
						.criaAlertDialogCustom(context,
								Constantes.MSG_CONFIRMACAO_LIMPEZA_LOTE,
								Constantes.MSG_CONFIRMACAO_EXCLUSAO_TITLE);

				dialog.setIcon(android.R.drawable.ic_delete);
				dialog.setPositiveButton("Sim",
						new DialogInterface.OnClickListener()
						{

							@Override
							public void onClick(DialogInterface arg0, int arg1)
							{
								if (dao.limparPesosLote(_amostragemLoteselecionada) == false)
								{
									FuncoesCompartilhadas.exibirToastMsgLong(
											context,
											Constantes.MSG_ERRO_GRAVACAO);
								} else
								{
									FuncoesCompartilhadas
											.exibirToastMsgLong(
													context,
													Constantes.MSG_REGISTRO_EXCLUIDO_SUCESSO);
								}
							}
						});

				dialog.setNegativeButton("N�o", null);
				dialog.create().show();
			}

			break;
		}

		return false;
	}

	private boolean gravar()
	{
		/* codigo */
		int icodigo = 0;
		if (_amostragemLoteselecionada != null)
		{
			icodigo = _amostragemLoteselecionada.getCodigo();
		}

		/* lote */
		String lote = edtLoteLote.getText().toString().trim();
		if (lote == null || lote.equals(""))
		{
			FuncoesCompartilhadas.exibirToastMsgLong(
					ViewAmostragemSemanalSelecaoLote.this,
					String.format(Constantes.MSG_CAMPO_NAO_INFORMADO, "Lote"));

			edtLoteLote.requestFocus();

			return false;
		}

		/* Verifica se o lote j� encontra-se cadastrado */

		if (dao.loteJaCadastrado(lote, icodigo))
		{
			FuncoesCompartilhadas
					.exibirToastMsgLong(ViewAmostragemSemanalSelecaoLote.this,
							"Lote j� cadastrado");
			return false;
		}

		/* sexo */
		int sexo = spLoteSexo.getSelectedItemPosition();
		if (sexo < 0)
		{
			FuncoesCompartilhadas.exibirToastMsgLong(
					ViewAmostragemSemanalSelecaoLote.this,
					String.format(Constantes.MSG_CAMPO_NAO_INFORMADO, "Sexo"));

			spLoteSexo.requestFocus();
			return false;
		}

		/* qtd boxes */

		String qtdBox = edtLoteQtdBoxes.getText().toString();
		if (FuncoesCompartilhadas.EditTextIsEmpty(edtLoteQtdBoxes))
		{

			FuncoesCompartilhadas.exibirToastMsgLong(
					ViewAmostragemSemanalSelecaoLote.this, String.format(
							Constantes.MSG_CAMPO_NAO_INFORMADO,
							"Qtde. de Boxes"));

			edtLoteQtdBoxes.requestFocus();
			return false;

		} else
		{
			if (Integer.parseInt(qtdBox) == 0)
			{

				FuncoesCompartilhadas.exibirToastMsgLong(
						ViewAmostragemSemanalSelecaoLote.this,
						"O n�mero de boxes deve ser maior que 0!");

				edtLoteQtdBoxes.requestFocus();
				return false;
			}
		}

		// ver se mudou a qtd de boxes mudou para menos que o registrado
		if (_amostragemLoteselecionada != null
				&& _amostragemLoteselecionada.getCodigo() > 0)
		{
			int qtdBoxesPorLote = new DAOAmostragemBox(context)
					.amostragemBoxPorLoteCount(_amostragemLoteselecionada
							.getCodigo());
			if (qtdBoxesPorLote > Integer.parseInt(qtdBox))
			{
				FuncoesCompartilhadas
						.exibirToastMsgLong(context,
								"N�mero de boxes configurados para este lote � maior que o informado");
				return false;
			}
		}

		/* qtd aves box */

		String qtdAvesBox = edtLoteQtdAvesBox.getText().toString();
		if (FuncoesCompartilhadas.EditTextIsEmpty(edtLoteQtdAvesBox))
		{
			FuncoesCompartilhadas.exibirToastMsgLong(
					ViewAmostragemSemanalSelecaoLote.this, String.format(
							Constantes.MSG_CAMPO_NAO_INFORMADO,
							"Qtde. Aves por Box"));

			edtLoteQtdAvesBox.requestFocus();
			return false;

		} else
		{
			if (Integer.parseInt(qtdAvesBox) == 0)
			{
				FuncoesCompartilhadas.exibirToastMsgLong(
						ViewAmostragemSemanalSelecaoLote.this,
						"O n�mero de aves por box deve ser maior que 0!");

				edtLoteQtdAvesBox.requestFocus();
				return false;
			}
		}

		if (_amostragemLoteselecionada != null
				&& _amostragemLoteselecionada.getCodigo() > 0)
		{
			int qtdMaxAvesNoLote = new DAOAmostragemBox(context)
					.amostragemBoxPorLoteMaxAves(_amostragemLoteselecionada
							.getCodigo());

			if (Integer.parseInt(qtdAvesBox) < qtdMaxAvesNoLote)
			{
				FuncoesCompartilhadas.MensagemAlerta(context,
						"N�mero de aves por box configurado n�o pode ser menor que o maior "
								+ "valor configurado para os boxes deste lote."
								+ Constantes.TXT_NOVA_LINHA
								+ Constantes.TXT_NOVA_LINHA + "Maior valor: "
								+ qtdMaxAvesNoLote);

				edtLoteQtdAvesBox.requestFocus();
				return false;
			}
		}

		AmostragemLote al = new AmostragemLote();
		al.setCodigo(icodigo);
		al.setNumeroLote(lote);

		switch (sexo) {
		case Constantes.SEXO_INDEX_FEMEA:
			al.setSexo(Constantes.SEXO_VALOR_FEMEA);
			break;

		case Constantes.SEXO_INDEX_MACHO:
			al.setSexo(Constantes.SEXO_VALOR_MACHO);
			break;

		case Constantes.SEXO_INDEX_AMBOS:
			al.setSexo(Constantes.SEXO_VALOR_AMBOS);
			break;
		}

		al.setQtdBoxes(Short.parseShort(qtdBox));
		al.setQtdAvesPorBox(Integer.parseInt(qtdAvesBox));

		if (al.getCodigo() > 0)
		{
			if (dao.alterar(al) == 0)
			{
				FuncoesCompartilhadas.exibirToastMsgLong(
						ViewAmostragemSemanalSelecaoLote.this,
						Constantes.MSG_ERRO_GRAVACAO);
				return false;
			} else
			{
				FuncoesCompartilhadas.exibirToastMsgLong(context,
						Constantes.MSG_REGISTRO_ALTERADO_SUCESSO);
			}
		} else
		{
			if (dao.inserir(al) == 0)
			{
				FuncoesCompartilhadas.exibirToastMsgLong(
						ViewAmostragemSemanalSelecaoLote.this,
						Constantes.MSG_ERRO_GRAVACAO);
				return false;
			} else
			{
				FuncoesCompartilhadas.exibirToastMsgLong(context,
						Constantes.MSG_REGISTRO_INCLUIDO_SUCESSO);
			}
		}

		return true;
	}

	private void carregaDadosLote()
	{
		FuncoesCompartilhadas.log("CodigoLote: "
				+ _amostragemLoteselecionada.getCodigo());

		final Dialog dialogLoteCadastro = new Dialog(
				ViewAmostragemSemanalSelecaoLote.this);
		dialogLoteCadastro.setTitle(CADASTRO_LOTE_TITLE);
		dialogLoteCadastro.setContentView(R.layout.activity_view_lote_cadastro);
		dialogLoteCadastro.setCanceledOnTouchOutside(false);

		edtLoteLote = (EditText) dialogLoteCadastro
				.findViewById(R.id.lote_cadastro_edtLote);

		spLoteSexo = (Spinner) dialogLoteCadastro
				.findViewById(R.id.lote_cadastro_spSexo);

		edtLoteQtdBoxes = (EditText) dialogLoteCadastro
				.findViewById(R.id.lote_cadastro_edtQtde_boxes);

		edtLoteQtdAvesBox = (EditText) dialogLoteCadastro
				.findViewById(R.id.lote_cadastro_edtQtde_aves_box);

		edtLoteLote.setText(_amostragemLoteselecionada.getNumeroLote());
		edtLoteQtdBoxes.setText(String.valueOf(_amostragemLoteselecionada
				.getQtdBoxes()));
		edtLoteQtdAvesBox.setText(String.valueOf(_amostragemLoteselecionada
				.getQtdAvesPorBox()));

		spLoteSexo.setAdapter(adapterSexo);

		if (_amostragemLoteselecionada.getSexo().equals(
				Constantes.SEXO_VALOR_FEMEA))
		{
			spLoteSexo.setSelection(Constantes.SEXO_INDEX_FEMEA);
		} else if (_amostragemLoteselecionada.getSexo().equals(
				Constantes.SEXO_VALOR_MACHO))
		{
			spLoteSexo.setSelection(Constantes.SEXO_INDEX_MACHO);
		} else if (_amostragemLoteselecionada.getSexo().equals(
				Constantes.SEXO_VALOR_AMBOS))
		{
			spLoteSexo.setSelection(Constantes.SEXO_INDEX_AMBOS);
		}

		final Button btnGravar = (Button) dialogLoteCadastro
				.findViewById(R.id.lote_cadastro_btnSalvar);
		btnGravar.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				if (gravar())
				{
					dialogLoteCadastro.dismiss();
					carregaDados();
				}
			}
		});

		final Button btnCancelar = (Button) dialogLoteCadastro
				.findViewById(R.id.lote_cadastro_btnCancelar);
		btnCancelar.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				dialogLoteCadastro.dismiss();
			}
		});

		dialogLoteCadastro.show();
	}

	private void carregaDados()
	{
		if (adapter != null)
		{
			adapter.clear();
			adapter.addAll(dao.getAmostragemLotes());
			adapter.notifyDataSetChanged();
		}
	}

	private boolean temDadosAmostragem()
	{
		// valida��o pela data de coleta (pesagem)
		DAOUtilitarios daoUtil = new DAOUtilitarios(context);

		if (daoUtil.inicializarAmostragemSemanal())
		{
			FuncoesCompartilhadas.exibirToastMsgLong(context,
					Constantes.MSG_AMOSTRAGEM_PESOS_OUTRA_DATA);
			return true;
		}

		return false;
	}

	private boolean excluir()
	{
		SQLiteDatabase conexao = ConexaoManager.getInstance(context);
		conexao.beginTransaction(); // CUIDADO COM A TRANSA��O
		try
		{
			if (dao.limparPesosLote(_amostragemLoteselecionada, conexao, true) == false)
			{
				return false;
			} else
			{
				if (dao.excluir(_amostragemLoteselecionada.getCodigo(), conexao) < 0)
				{
					return false;
				}
			}

			FuncoesCompartilhadas.exibirToastMsgLong(context,
					Constantes.MSG_REGISTRO_EXCLUIDO_SUCESSO);

			conexao.setTransactionSuccessful();

		} catch (SQLiteException ex)
		{
			FuncoesCompartilhadas.log(ex.getMessage());
			return false;

		} finally
		{
			conexao.endTransaction();
		}
		return true;
	}
}
