package net.automatize.aves.view;

import net.automatize.aves.R;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ViewPreAmostragem extends Activity
{
	final Context context = this;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_pre_amostragem);

		final Button btnPreAmostragem = (Button) findViewById(R.id.preamostragem1_btnpreamostragem);
		btnPreAmostragem.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				Intent i = new Intent(context,
						ViewPreAmostragemSelecao100.class);
				startActivity(i);
			}
		});

		final Button btnTransferenciaPeso = (Button) findViewById(R.id.preamostragem1_btntransferenciapeso);
		btnTransferenciaPeso.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				Intent intent = new Intent(context,
						ViewPreAmostragemTransferenciaPeso.class);
				startActivity(intent);
			}
		});

		final Button btnRetornar = (Button) findViewById(R.id.preamostragem1_btnretornar);
		btnRetornar.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				finish();
			}
		});
	}
}
