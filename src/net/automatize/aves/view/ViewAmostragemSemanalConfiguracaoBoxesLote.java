package net.automatize.aves.view;

import java.util.ArrayList;
import java.util.List;

import net.automatize.aves.R;
import net.automatize.aves.adapter.AmostragemBoxAdapter;
import net.automatize.aves.dao.DAOAmostragemBox;
import net.automatize.aves.dao.DAOAmostragemLotes;
import net.automatize.aves.dao.DAOAmostragemPesos;
import net.automatize.aves.dao.DAOClassificacao;
import net.automatize.aves.model.AmostragemBox;
import net.automatize.aves.model.AmostragemLote;
import net.automatize.aves.model.Classificacao;
import net.automatize.aves.util.Constantes;
import net.automatize.aves.util.FuncoesCompartilhadas;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

public class ViewAmostragemSemanalConfiguracaoBoxesLote extends Activity
{
	final Context context = this;

	private DAOAmostragemBox dao = new DAOAmostragemBox(
			ViewAmostragemSemanalConfiguracaoBoxesLote.this);
	private static AmostragemLote _amostragemlote = null;
	private static AmostragemBox _amostragembox = null;
	private ArrayList<AmostragemBox> _dataList = null;
	private AmostragemBoxAdapter adapter = null;

	/* Indices para o context menu */
	private final int BOX_CONTEXT_MENU_ALTERAR = 0;
	private final int BOX_CONTEXT_MENU_EXCLUIR = 1;
	private final int BOX_CONTEXT_MENU_LIMPAR_BOX = 2;
	private final int BOX_CONTEXT_MENU_CANCELAR = 3;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_amostragem_semanal_configuracao_box);

		Bundle b = getIntent().getExtras();
		if (b != null)
		{
			if (b.containsKey(AmostragemLote.CODIGO))
			{
				DAOAmostragemLotes daoAmostragemLote = new DAOAmostragemLotes(
						ViewAmostragemSemanalConfiguracaoBoxesLote.this);
				_amostragemlote = daoAmostragemLote.getAmostragemLote(b
						.getInt(AmostragemLote.CODIGO));

				getActionBar()
						.setTitle(
								String.format(
										getString(R.string.title_activity_view_amostragem_semanal_configuracao_boxes),
										_amostragemlote.getNumeroLote()));
			}
		}

		_dataList = new ArrayList<AmostragemBox>();
		final ListView lv = (ListView) findViewById(R.id.amostragembox_configuracaoLote_lv);

		adapter = new AmostragemBoxAdapter(
				ViewAmostragemSemanalConfiguracaoBoxesLote.this,
				R.id.amostragembox_configuracaoLote_lv, _dataList);

		carregaDados();

		lv.setAdapter(adapter);

		registerForContextMenu(lv);

		lv.setOnItemClickListener(new AdapterView.OnItemClickListener()
		{

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long id)
			{
				_amostragembox = _dataList.get(position);

				if (_amostragembox != null)
				{
					if (FuncoesCompartilhadas
							.parametrosPesagemConfigurados(context) == null)
						return;

					Intent intent = new Intent(
							ViewAmostragemSemanalConfiguracaoBoxesLote.this,
							ViewAmostragemSemanalCapturaPeso.class);
					intent.putExtra(AmostragemBox.CODIGO,
							_amostragembox.getCodigo());
					startActivity(intent);
				}
			}
		});

		lv.setOnItemLongClickListener(new OnItemLongClickListener()
		{

			@Override
			public boolean onItemLongClick(AdapterView<?> adapter, View view,
					int position, long id)
			{
				_amostragembox = _dataList.get(position);
				return false;
			}
		});

		final Button btnNovoBox = (Button) findViewById(R.id.amostragembox_configuracaoLote_btnNovoBox);
		btnNovoBox.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{
				/*
				 * Valida��o para numero de box cadastrados e configura��o do
				 * lote
				 */

				_amostragembox = null;

				if (_dataList.size() == _amostragemlote.getQtdBoxes())
				{
					FuncoesCompartilhadas.exibirToastMsgLong(context, String
							.format(Constantes.MSG_TOTAL_BOXES_EXCEDIDO,
									_amostragemlote.getQtdBoxes()));
					return;
				}

				final Dialog dialog = new Dialog(
						ViewAmostragemSemanalConfiguracaoBoxesLote.this);
				dialog.setTitle("Cadastro de Box");
				dialog.setContentView(R.layout.activity_view_box_cadastro);
				dialog.setCanceledOnTouchOutside(false);

				/* Setando numero de aves por box configurado no lote */
				final EditText edtQtdeAvesPesar = (EditText) dialog
						.findViewById(R.id.box_cadastro_edt_qtde_aves_pesar);
				edtQtdeAvesPesar.setText(String.valueOf(_amostragemlote
						.getQtdAvesPorBox()));

				/* classificacoes - montando o combo */

				final Spinner spinner = (Spinner) dialog
						.findViewById(R.id.box_cadastro_spClassificacao);

				DAOClassificacao daoClassificacoes = new DAOClassificacao(
						ViewAmostragemSemanalConfiguracaoBoxesLote.this);

				final List<Classificacao> _dataListClassificacoes = daoClassificacoes
						.getClassificacoes(true);

				String[] spinnerArray;

				if (_dataListClassificacoes.isEmpty())
				{
					spinner.setEnabled(false);
					spinnerArray = new String[1];
					spinnerArray[0] = Constantes.MSG_NAO_HA_CLASSIFICACOES;
				} else
				{
					spinnerArray = montarSpinnerClassificacoes(_dataListClassificacoes);
				}

				ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
						ViewAmostragemSemanalConfiguracaoBoxesLote.this,
						// android.R.layout.simple_spinner_dropdown_item,
						R.layout.spinner_item, spinnerArray);

				spinner.setAdapter(spinnerArrayAdapter);

				/* Cancelar */
				Button btnCancelar = (Button) dialog
						.findViewById(R.id.box_cadastro_btnCancelar);
				btnCancelar.setOnClickListener(new View.OnClickListener()
				{

					@Override
					public void onClick(View arg0)
					{
						dialog.dismiss();
					}
				});

				/* Gravar */

				Button btnGravar = (Button) dialog
						.findViewById(R.id.box_cadastro_btnGravar);
				btnGravar.setOnClickListener(new View.OnClickListener()
				{

					@Override
					public void onClick(View v)
					{
						/* Codigo */

						// final EditText edtCodigo = (EditText) dialog
						// .findViewById(R.id.box_cadastro_edtCodigo);
						// String scodigo = edtCodigo.getText().toString();

						/* Nro box */

						final EditText edtNroBox = (EditText) dialog
								.findViewById(R.id.box_cadastro_edtNro_box);

						String snro_box = edtNroBox.getText().toString();
						if (snro_box == null || snro_box.equals(""))
						{
							FuncoesCompartilhadas
									.exibirToastMsgLong(
											ViewAmostragemSemanalConfiguracaoBoxesLote.this,
											String.format(
													Constantes.MSG_CAMPO_NAO_INFORMADO,
													"N� do box"));
							edtNroBox.requestFocus();
							return;
						}

						/* Qtde aves por box */

						final EditText edtQtdeAvesPesar = (EditText) dialog
								.findViewById(R.id.box_cadastro_edt_qtde_aves_pesar);

						String sqtde_aves_pesar = edtQtdeAvesPesar.getText()
								.toString();
						if (sqtde_aves_pesar == null
								|| sqtde_aves_pesar.equals(""))
						{
							FuncoesCompartilhadas
									.exibirToastMsgLong(
											ViewAmostragemSemanalConfiguracaoBoxesLote.this,
											String.format(
													Constantes.MSG_CAMPO_NAO_INFORMADO,
													"Aves � pesar"));
							edtQtdeAvesPesar.requestFocus();
							return;
						} else
						{
							if (Integer.parseInt(sqtde_aves_pesar) == 0)
							{
								FuncoesCompartilhadas
										.exibirToastMsgLong(
												ViewAmostragemSemanalConfiguracaoBoxesLote.this,
												"O n�mero de Aves � Pesar deve ser maior que 0");
								edtQtdeAvesPesar.requestFocus();
								return;
							}
						}

						if (Integer.parseInt(sqtde_aves_pesar) > _amostragemlote
								.getQtdAvesPorBox())
						{
							FuncoesCompartilhadas
									.exibirToastMsgLong(context,
											"O n�mero de aves a pesar n�o pode ser maior que o valor configurado por lote.");
							return;
						}

						/* Classifica��o */

						final Spinner spinner = (Spinner) dialog
								.findViewById(R.id.box_cadastro_spClassificacao);

						if (_dataListClassificacoes.isEmpty()
								|| spinner.getSelectedItemPosition() <= 0)
						{
							FuncoesCompartilhadas
									.exibirToastMsgLong(
											ViewAmostragemSemanalConfiguracaoBoxesLote.this,
											String.format(
													Constantes.MSG_CAMPO_NAO_INFORMADO,
													"Classifica��o"));
							spinner.requestFocus();
							return;
						}

						Classificacao c = _dataListClassificacoes.get(spinner
								.getSelectedItemPosition());

						AmostragemBox amostragemBox = new AmostragemBox();
						if (_amostragembox != null
								&& _amostragembox.getCodigo() > 0)
						{
							amostragemBox.setCodigo(_amostragembox.getCodigo());
						}
						amostragemBox.setAmostragemlote_codigo(_amostragemlote
								.getCodigo());
						amostragemBox.setNrbox(Short.parseShort(snro_box));
						amostragemBox.setQtdAvesPesar(Integer
								.parseInt(sqtde_aves_pesar));
						amostragemBox.setClassificacao_codigo(c.getCodigo());

						if (dao.gravar(amostragemBox) == 0)
						{
							FuncoesCompartilhadas
									.exibirToastMsgLong(
											ViewAmostragemSemanalConfiguracaoBoxesLote.this,
											Constantes.MSG_ERRO_GRAVACAO);
							return;
						}

						dialog.dismiss();
						carregaDados();
					}
				});

				dialog.show();
			}
		});

		final Button btnRetornar = (Button) findViewById(R.id.amostragembox_configuracaoLote_btnRetornar);
		btnRetornar.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				finish();
			}
		});

	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo)
	{
		super.onCreateContextMenu(menu, v, menuInfo);
		menu.setHeaderTitle(Constantes.MSG_TITLE_MENU_CONTEXT);
		menu.add(0, v.getId(), BOX_CONTEXT_MENU_ALTERAR, "Alterar");
		menu.add(0, v.getId(), BOX_CONTEXT_MENU_EXCLUIR, "Excluir");
		menu.add(0, v.getId(), BOX_CONTEXT_MENU_LIMPAR_BOX,
				"Limpeza de Pesos do Box");
		menu.add(0, v.getId(), BOX_CONTEXT_MENU_CANCELAR, "Cancelar");
	}

	@Override
	public boolean onContextItemSelected(MenuItem item)
	{
		switch (item.getOrder()) {
		case BOX_CONTEXT_MENU_ALTERAR:
			if (_amostragembox != null)
				carregaDadosBox();

			break;
		case BOX_CONTEXT_MENU_EXCLUIR:

			if (_amostragembox != null)
			{
				/* ver se tem box com peso coletados */
				int numeroPesosColetasPorBox = new DAOAmostragemPesos(context)
						.boxTemPesos(_amostragembox.getCodigo());

				if (numeroPesosColetasPorBox > 0)
				{
					FuncoesCompartilhadas.exibirToastMsgLong(context,
							Constantes.MSG_IMPOSSIVEL_EXCLUIR);
					FuncoesCompartilhadas.exibirToastMsgLong(context, String
							.format("Este box possu� %d peso(s) coletado(s).",
									numeroPesosColetasPorBox));
					return true;
				}

				// final AlertDialog.Builder dialog = new AlertDialog.Builder(
				// ViewAmostragemSemanalConfiguracaoBoxesLote.this);
				// dialog.setTitle(Constantes.MSG_CONFIRMACAO_EXCLUSAO_TITLE);
				// dialog.setMessage(Constantes.MSG_CONFIRMACAO_EXCLUSAO_MSG);

				final AlertDialog.Builder dialog = FuncoesCompartilhadas
						.criaAlertDialogCustom(context,
								Constantes.MSG_CONFIRMACAO_EXCLUSAO_MSG,
								Constantes.MSG_CONFIRMACAO_EXCLUSAO_TITLE);

				dialog.setIcon(android.R.drawable.ic_delete);
				dialog.setPositiveButton("Sim",
						new DialogInterface.OnClickListener()
						{

							@Override
							public void onClick(DialogInterface arg0, int arg1)
							{
								if (dao.excluir(_amostragembox.getCodigo()) == 0)
								{
									FuncoesCompartilhadas.exibirToastMsgLong(
											context,
											Constantes.MSG_ERRO_GRAVACAO);
								} else
								{
									FuncoesCompartilhadas.exibirToastMsgLong(context,
											Constantes.MSG_REGISTRO_EXCLUIDO_SUCESSO);
									carregaDados();
								}
							}
						});

				dialog.setNegativeButton("N�o", null);

				dialog.create().show();
			}

			break;
		case BOX_CONTEXT_MENU_LIMPAR_BOX:

			// final AlertDialog.Builder dialog2 = new AlertDialog.Builder(
			// ViewAmostragemSemanalConfiguracaoBoxesLote.this);
			// dialog2.setTitle(Constantes.MSG_CONFIRMACAO_EXCLUSAO_TITLE);
			//
			// dialog2.setMessage(Constantes.MSG_CONFIRMACAO_LIMPEZA_BOX);

			final AlertDialog.Builder dialog2 = FuncoesCompartilhadas
					.criaAlertDialogCustom(context,
							Constantes.MSG_CONFIRMACAO_LIMPEZA_BOX,
							Constantes.MSG_CONFIRMACAO_EXCLUSAO_TITLE);

			dialog2.setIcon(android.R.drawable.ic_delete);
			dialog2.setPositiveButton("Sim",
					new DialogInterface.OnClickListener()
					{

						@Override
						public void onClick(DialogInterface arg0, int arg1)
						{
							limparPesosBox();
							carregaDados();
						}
					});

			dialog2.setNegativeButton("N�o", null);
			dialog2.create().show();

			break;
		default:
			break;
		}
		return false;
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		carregaDados();
	}

	// @Override
	// public boolean onCreateOptionsMenu(Menu menu)
	// {
	// // Inflate the menu; this adds items to the action bar if it is present.
	// getMenuInflater().inflate(
	// R.menu.view_amostragem_semanal_configuracao_lote, menu);
	// return true;
	// }

	private void limparPesosBox()
	{
		if (_amostragembox != null)
		{
			DAOAmostragemPesos daoAmostragemPesos = new DAOAmostragemPesos(
					context);
			if (daoAmostragemPesos.limparPesosBox(_amostragembox.getCodigo()) == false)
			{
				FuncoesCompartilhadas.exibirToastMsgLong(context,
						Constantes.MSG_ERRO_GRAVACAO);
			} else
			{
				FuncoesCompartilhadas.exibirToastMsgLong(context,
						Constantes.MSG_REGISTRO_EXCLUIDO_SUCESSO);
			}
		}
	}

	private void carregaDados()
	{
		if (adapter != null)
		{
			adapter.clear();
			adapter.addAll(dao.amostragemBoxPorLote(_amostragemlote.getCodigo()));
			adapter.notifyDataSetChanged();
		}
	}

	private String[] montarSpinnerClassificacoes(
			List<Classificacao> _dataListClassificacoes)
	{
		String[] result = new String[_dataListClassificacoes.size()];
		for (int i = 0; i < result.length; i++)
		{
			result[i] = _dataListClassificacoes.get(i).getSigla();
		}
		return result;
	}

	private void carregaDadosBox()
	{
		final Dialog dialog = new Dialog(
				ViewAmostragemSemanalConfiguracaoBoxesLote.this);
		dialog.setTitle("Cadastro de Box");
		dialog.setContentView(R.layout.activity_view_box_cadastro);
		dialog.setCanceledOnTouchOutside(false);

		/* Codigo */
		// final EditText edtCodigo = (EditText) dialog
		// .findViewById(R.id.box_cadastro_edtCodigo);

		/* Nro box */
		final EditText edtNroBox = (EditText) dialog
				.findViewById(R.id.box_cadastro_edtNro_box);

		/* Qtde aves por box */
		final EditText edtQtdeAvesPesar = (EditText) dialog
				.findViewById(R.id.box_cadastro_edt_qtde_aves_pesar);
		/* Classifica��o */
		final Spinner spinner = (Spinner) dialog
				.findViewById(R.id.box_cadastro_spClassificacao);

		DAOClassificacao daoClassificacoes = new DAOClassificacao(
				ViewAmostragemSemanalConfiguracaoBoxesLote.this);

		final List<Classificacao> _dataListClassificacoes = daoClassificacoes
				.getClassificacoes(true);

		String[] spinnerArray;

		if (_dataListClassificacoes.isEmpty())
		{
			spinner.setEnabled(false);
			spinnerArray = new String[1];
			spinnerArray[0] = Constantes.MSG_NAO_HA_CLASSIFICACOES;
		} else
		{
			spinnerArray = montarSpinnerClassificacoes(_dataListClassificacoes);
		}

		ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
				ViewAmostragemSemanalConfiguracaoBoxesLote.this,
				R.layout.spinner_item, spinnerArray);

		spinner.setAdapter(spinnerArrayAdapter);

		/* Repassando os valores para os campos */

		// edtCodigo.setText(String.valueOf(_amostragembox.getCodigo()));
		edtNroBox.setText(String.valueOf(_amostragembox.getNrbox()));
		edtQtdeAvesPesar.setText(String.valueOf(_amostragembox
				.getQtdAvesPesar()));

		int spinner_classificacao_position = procuraClassificacaoEmSpinner(
				_dataListClassificacoes,
				_amostragembox.getClassificacao_codigo());

		if (spinner_classificacao_position < 0)
		{
			FuncoesCompartilhadas.exibirToastMsgLong(context,
					"Classifica��o n�o encontrada");
		} else
		{
			spinner.setSelection(spinner_classificacao_position);
		}

		/* Cancelar */
		Button btnCancelar = (Button) dialog
				.findViewById(R.id.box_cadastro_btnCancelar);
		btnCancelar.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{
				dialog.dismiss();
			}
		});

		/* Gravar */

		Button btnGravar = (Button) dialog
				.findViewById(R.id.box_cadastro_btnGravar);
		btnGravar.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				/* Codigo */

				// final EditText edtCodigo = (EditText) dialog
				// .findViewById(R.id.box_cadastro_edtCodigo);
				// String scodigo = edtCodigo.getText().toString();

				/* Nro box */

				final EditText edtNroBox = (EditText) dialog
						.findViewById(R.id.box_cadastro_edtNro_box);

				String snro_box = edtNroBox.getText().toString();
				if (snro_box == null || snro_box.equals(""))
				{
					FuncoesCompartilhadas.exibirToastMsgLong(
							ViewAmostragemSemanalConfiguracaoBoxesLote.this,
							String.format(Constantes.MSG_CAMPO_NAO_INFORMADO,
									"N� do box"));
					edtNroBox.requestFocus();
					return;
				}

				/* Qtde aves no box */

				final EditText edtQtdeAvesPesar = (EditText) dialog
						.findViewById(R.id.box_cadastro_edt_qtde_aves_pesar);

				String sqtde_aves_pesar = edtQtdeAvesPesar.getText().toString();
				if (sqtde_aves_pesar == null || sqtde_aves_pesar.equals(""))
				{
					FuncoesCompartilhadas.exibirToastMsgLong(
							ViewAmostragemSemanalConfiguracaoBoxesLote.this,
							String.format(Constantes.MSG_CAMPO_NAO_INFORMADO,
									"Aves � pesar"));
					edtQtdeAvesPesar.requestFocus();
					return;
				} else
				{
					if (Integer.parseInt(sqtde_aves_pesar) == 0)
					{
						FuncoesCompartilhadas
								.exibirToastMsgLong(
										ViewAmostragemSemanalConfiguracaoBoxesLote.this,
										"O n�mero de Aves � Pesar deve ser maior que 0");
						edtQtdeAvesPesar.requestFocus();
						return;
					}
				}

				if (Integer.parseInt(sqtde_aves_pesar) > _amostragemlote
						.getQtdAvesPorBox())
				{
					FuncoesCompartilhadas.MensagemAlerta(context,
							"O n�mero de Aves � Pesar n�o pode ser maior que o valor configurado por lote."
									+ Constantes.TXT_NOVA_LINHA
									+ Constantes.TXT_NOVA_LINHA
									+ "Valor configurado para o Lote: "
									+ _amostragemlote.getQtdAvesPorBox());
					edtQtdeAvesPesar.requestFocus();
					return;
				}

				/* numero de aves configurado menor que o pesado */
				if (_amostragembox.getCodigo() > 0)
				{
					int numeroPesosColetasPorBox = new DAOAmostragemPesos(
							context).boxTemPesos(_amostragembox.getCodigo());

					if (Integer.parseInt(sqtde_aves_pesar) < numeroPesosColetasPorBox)
					{
						FuncoesCompartilhadas.exibirToastMsgLong(context,
								Constantes.MSG_IMPOSSIVEL_ALTERAR);
						FuncoesCompartilhadas.exibirToastMsgLong(
								context,
								String.format(
										"N�mero de aves inv�lido. Este box possu� %d peso(s) coletado(s).",
										numeroPesosColetasPorBox));
						return;
					}
				}

				/* Classifica��o */

				final Spinner spinner = (Spinner) dialog
						.findViewById(R.id.box_cadastro_spClassificacao);

				if (_dataListClassificacoes.isEmpty()
						|| spinner.getSelectedItemPosition() <= 0)
				{
					FuncoesCompartilhadas.exibirToastMsgLong(
							ViewAmostragemSemanalConfiguracaoBoxesLote.this,
							String.format(Constantes.MSG_CAMPO_NAO_INFORMADO,
									"Classifica��o"));
					spinner.requestFocus();
					return;
				}

				Classificacao c = _dataListClassificacoes.get(spinner
						.getSelectedItemPosition());

				AmostragemBox amostragemBox = new AmostragemBox();
				if (_amostragembox != null && _amostragembox.getCodigo() > 0)
				{
					amostragemBox.setCodigo(_amostragembox.getCodigo());
				}
				amostragemBox.setAmostragemlote_codigo(_amostragemlote
						.getCodigo());
				amostragemBox.setNrbox(Short.parseShort(snro_box));
				amostragemBox.setQtdAvesPesar(Integer
						.parseInt(sqtde_aves_pesar));
				amostragemBox.setClassificacao_codigo(c.getCodigo());

				if (dao.gravar(amostragemBox) == 0)
				{
					FuncoesCompartilhadas.exibirToastMsgLong(
							ViewAmostragemSemanalConfiguracaoBoxesLote.this,
							Constantes.MSG_ERRO_GRAVACAO);
					return;
				}

				dialog.dismiss();
				carregaDados();
			}
		});

		dialog.show();
	}

	public int procuraClassificacaoEmSpinner(
			List<Classificacao> dataListClassificacoes, int classificacaoCodigo)
	{
		for (int i = 0; i < dataListClassificacoes.size(); i++)
		{
			if (dataListClassificacoes.get(i).getCodigo() == classificacaoCodigo)
				return i;
		}
		return -1;
	}
}
