package net.automatize.aves.view;

import java.util.ArrayList;
import java.util.Date;

import net.automatize.aves.R;
import net.automatize.aves.dao.DAOClassificacao;
import net.automatize.aves.dao.DAOPreAmostragem;
import net.automatize.aves.dao.DAOPreAmostragemClassificacao;
import net.automatize.aves.dao.DAOPreAmostragemPesos;
import net.automatize.aves.model.PreAmostragem;
import net.automatize.aves.model.PreAmostragemClassificacaoItem;
import net.automatize.aves.model.PreAmostragemPeso;
import net.automatize.aves.model.PreAmostragemPesoEstatistica;
import net.automatize.aves.util.Constantes;
import net.automatize.aves.util.FuncoesCompartilhadas;
import net.automatize.aves.util.FuncoesEspecificas;
import net.automatize.aves.util.Mask;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

public class ViewPreAmostragemSelecao100 extends Activity
{
	final Context context = this;

	private ArrayList<PreAmostragemClassificacaoItem> _dataList = null;
	private PreAmostragemClassificacaoItemAdapter adapter = null;
	private DAOPreAmostragem dao = new DAOPreAmostragem(context);
	private DAOPreAmostragemPesos daoPreAmostragemPesos = new DAOPreAmostragemPesos(
			context);
	private DAOPreAmostragemClassificacao daoPreAmostragemClassificacao = new DAOPreAmostragemClassificacao(
			context);
	private PreAmostragem preAmostragem = null;
	private ArrayList<PreAmostragemPeso> listaPreAmostragemPeso = new ArrayList<PreAmostragemPeso>();
	private PreAmostragemPesoEstatistica preAmostragemPesoEstatistica = null;

	private EditText edtLote, edtAves, edtCortes, edtData;
	private Spinner spSexo, spTipo;
	private ListView lv;

	private ArrayAdapter<CharSequence> adapterSexo = null;
	private ArrayAdapter<CharSequence> adapterTipo = null;

	private TextWatcher dataMask = null;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_pre_amostragem_selecao100);

		/* adapter */
		adapterSexo = ArrayAdapter.createFromResource(context,
				R.array.sexo_array, R.layout.spinner_item);

		adapterTipo = ArrayAdapter.createFromResource(context,
				R.array.amostragem_tipo_array, R.layout.spinner_item);

		/* widgets */
		edtLote = (EditText) findViewById(R.id.preamostragem_edtlote);
		spSexo = (Spinner) findViewById(R.id.preamostragem_spsexo);
		spSexo.setAdapter(adapterSexo);

		edtAves = (EditText) findViewById(R.id.preamostragem_edtaves);
		edtCortes = (EditText) findViewById(R.id.preamostragem_edtcortes);

		edtData = (EditText) findViewById(R.id.preamostragem_edtdata);
		dataMask = Mask.insert("##/##/####", edtData);
		edtData.addTextChangedListener(dataMask);

		spTipo = (Spinner) findViewById(R.id.preamostragem_sptipo);
		spTipo.setAdapter(adapterTipo);
		spTipo.setOnItemSelectedListener(new OnItemSelectedListener()
		{

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3)
			{
				edtAves = (EditText) findViewById(R.id.preamostragem_edtaves);

				if (arg2 == Constantes.TIPOAMOSTRAGEM_INDEX_FIXO)
				{
					edtAves.setEnabled(true);
				} else
				{
					edtAves.setEnabled(false);

					if (preAmostragem != null)
						if (preAmostragem.getNumeroAves() == 0)
							edtAves.setText("0");
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0)
			{

			}
		});

		CarregaClassificacoes();
		carregaPreAmostragem();

		listaPreAmostragemPeso.addAll(daoPreAmostragemPesos
				.getPreAmostragemPesos());

		preAmostragemPesoEstatistica = daoPreAmostragemPesos
				.getPreAmostragemPesosEstatistica();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.view_pre_amostragem_selecao100, menu);
		return true;
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		atualizaQtdAves();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId()) {
		case R.id.preamostragem_ab_btncontinuar:
			continuar();
			return true;

		case R.id.preamostragem_ab_btnresultado:
			calc_faixas_pre_amostragem();
			return true;

		case R.id.preamostragem_ab_btnsair:
			finish();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void continuar()
	{
		if (gravarPreAmostragem())
		{// para atualizar o campo codigo
			preAmostragem.setCodigo(Constantes.PREAMOSTRAGEM_CODIGO);
			if (gravarClassificacoes())
			{
				if (preAmostragemPesoEstatistica != null)
				{
					if (preAmostragemPesoEstatistica.getQtd() > 0)
					{
						// menu de op��es (context) - quando j� tem pesos
						// coletados
						abreMenuContexto(preAmostragemPesoEstatistica.getQtd());
					} else
					{
						// pesagem
						abreCapturaPeso();
					}
				}
			}
		}
	}

	private void CarregaClassificacoes()
	{
		_dataList = new DAOPreAmostragemClassificacao(context)
				.carregaClassificacoesGrid();

		if (_dataList.isEmpty())
		{
			FuncoesCompartilhadas.exibirToastMsgLong(context,
					Constantes.MSG_NAO_HA_CLASSIFICACOES);
			return;
		}

		lv = (ListView) findViewById(R.id.preamostragem_lvclassificacoes);
		adapter = new PreAmostragemClassificacaoItemAdapter(context,
				R.id.preamostragem_lvclassificacoes, _dataList);
		lv.setAdapter(adapter);

		atualizaBarraPercentual();
	}

	private void carregaPreAmostragem()
	{
		preAmostragem = dao.getPreAmostragem();

		if (preAmostragem != null)
		{
			edtLote.setText(preAmostragem.getNumeroLote());

			if (preAmostragem.getSexo().equals(Constantes.SEXO_VALOR_FEMEA))
				spSexo.setSelection(Constantes.SEXO_INDEX_FEMEA);
			else if (preAmostragem.getSexo()
					.equals(Constantes.SEXO_VALOR_MACHO))
				spSexo.setSelection(Constantes.SEXO_INDEX_MACHO);
			else
				spSexo.setSelection(Constantes.SEXO_INDEX_AMBOS);

			if (preAmostragem.getTipo().equals(
					Constantes.TIPOAMOSTRAGEM_VALOR_FIXO))
				spTipo.setSelection(Constantes.TIPOAMOSTRAGEM_INDEX_FIXO);
			else
				spTipo.setSelection(Constantes.TIPOAMOSTRAGEM_INDEX_PERCENTUAL);

			edtAves.setText(String.valueOf(preAmostragem.getNumeroAves()));
			edtCortes.setText(String.valueOf(preAmostragem.getNumeroCortes()));

			edtData.removeTextChangedListener(dataMask);
			edtData.setText(FuncoesCompartilhadas.formataData(preAmostragem
					.getData()));
			edtData.addTextChangedListener(dataMask);

		} else
		{
			edtData.removeTextChangedListener(dataMask);
			edtData.setText(FuncoesCompartilhadas.formataData(new Date()));
			edtData.addTextChangedListener(dataMask);
		}
	}

	private boolean gravarPreAmostragem()
	{
		String lote = edtLote.getText().toString();
		if (lote.trim().equals(""))
		{
			FuncoesCompartilhadas.exibirToastMsgLong(context,
					String.format(Constantes.MSG_CAMPO_NAO_INFORMADO, "Lote"));
			edtLote.requestFocus();
			return false;
		}

		// valida o lote
		if (FuncoesEspecificas.ValidaNumeroLote(lote) == false)
		{
			FuncoesCompartilhadas.exibirToastMsgLong(context,
					"N�mero do Lote inv�lido!");
			edtLote.requestFocus();
			return false;
		}

		/* n�mero de cortes */
		if (edtCortes.getText().toString().equals(""))
		{
			FuncoesCompartilhadas.exibirToastMsgLong(context,
					String.format(Constantes.MSG_CAMPO_NAO_INFORMADO, "Corte"));
			edtCortes.requestFocus();
			return false;
		}

		int numerocortes = Integer.parseInt(edtCortes.getText().toString());
		if (numerocortes < 2)
		{
			FuncoesCompartilhadas.exibirToastMsgLong(context,
					"N�mero m�nimo de Cortes deve ser 2!");
			edtCortes.requestFocus();
			return false;
		} else
		{
			int nrclassificacoes = new DAOClassificacao(context).count();
			if (numerocortes > nrclassificacoes)
			{
				FuncoesCompartilhadas
						.exibirToastMsgLong(context,
								"N�mero de cortes superior a quantidade de classifica��es cadastradas!");
				edtCortes.requestFocus();
				return false;
			}
		}

		/* sexo */
		String sexo = null;
		switch (spSexo.getSelectedItemPosition()) {
		case Constantes.SEXO_INDEX_FEMEA:
			sexo = Constantes.SEXO_VALOR_FEMEA;
			break;

		case Constantes.SEXO_INDEX_MACHO:
			sexo = Constantes.SEXO_VALOR_MACHO;
			break;

		case Constantes.SEXO_INDEX_AMBOS:
			sexo = Constantes.SEXO_VALOR_AMBOS;
			break;

		default:
			break;
		}

		if (sexo == null)
		{
			FuncoesCompartilhadas.exibirToastMsgLong(context,
					String.format(Constantes.MSG_CAMPO_NAO_INFORMADO, "Sexo"));
			spSexo.requestFocus();
			return false;
		}

		/* tipo */
		String tipo = null;
		switch (spTipo.getSelectedItemPosition()) {
		case Constantes.TIPOAMOSTRAGEM_INDEX_FIXO:
			tipo = Constantes.TIPOAMOSTRAGEM_VALOR_FIXO;
			break;

		case Constantes.TIPOAMOSTRAGEM_INDEX_PERCENTUAL:
			tipo = Constantes.TIPOAMOSTRAGEM_VALOR_PERCENTUAL;
			break;
		default:
			break;
		}

		if (tipo == null)
		{
			FuncoesCompartilhadas.exibirToastMsgLong(context,
					String.format(Constantes.MSG_CAMPO_NAO_INFORMADO, "Tipo"));
			spTipo.requestFocus();
			return false;
		}

		/* numero de aves */
		int numeroaves = 0;
		if (edtAves.isEnabled() && edtAves.getText().toString().equals(""))
		{
			FuncoesCompartilhadas.exibirToastMsgLong(context,
					String.format(Constantes.MSG_CAMPO_NAO_INFORMADO, "Aves"));
			edtAves.requestFocus();
			return false;
		}

		/* tipo de amostragem */
		if (tipo.equals(Constantes.TIPOAMOSTRAGEM_VALOR_FIXO))
		{
			numeroaves = Integer.parseInt(edtAves.getText().toString());
			if (numeroaves < 10)
			{
				FuncoesCompartilhadas.exibirToastMsgLong(context,
						"N�mero de aves n�o pode ser inferior a 10!");
				return false;
			}

			if (numeroaves % numerocortes > 0)
			{
				edtAves.requestFocus();
				FuncoesCompartilhadas.log("numeroaves % numerocortes > 0");
				return false;
			}

		} else
		{
			numeroaves = daoPreAmostragemPesos.count();
		}

		/* data */
		String sdata = edtData.getText().toString();
		if (sdata.equals(""))
		{
			FuncoesCompartilhadas.exibirToastMsgLong(context,
					String.format(Constantes.MSG_CAMPO_NAO_INFORMADO, "Data"));
			edtData.requestFocus();
			return false;
		}

		Date data = FuncoesCompartilhadas.strToDate(sdata);

		if (data == null)
		{
			FuncoesCompartilhadas.exibirToastMsgLong(context, "Data inv�lida");
			return false;
		}

		if (preAmostragem == null)
			preAmostragem = new PreAmostragem();

		if (preAmostragem.getCodigo() > 0)
		{
			if (preAmostragem.getData().compareTo(data) != 0)
			{
				data = preAmostragem.getData();
				edtData.removeTextChangedListener(dataMask);
				edtData.setText(FuncoesCompartilhadas.formataData(data));
				edtData.addTextChangedListener(dataMask);
			}
		}

		/* grava��o bd */
		preAmostragem.setNumeroLote(lote);
		preAmostragem.setData(data);
		preAmostragem.setNumeroAves(numeroaves);
		preAmostragem.setNumeroCortes((short) numerocortes);
		preAmostragem.setTipo(tipo);
		preAmostragem.setSexo(sexo);

		if (dao.gravar(preAmostragem) == 0)
		{
			FuncoesCompartilhadas.exibirToastMsgLong(context,
					Constantes.MSG_ERRO_GRAVACAO);
			return false;
		}

		return true;
	}

	private boolean gravarClassificacoes()
	{
		// verifica se fecha 100%
		int percentual = 0;
		int count = 0;
		for (int i = 0; i < _dataList.size(); i++)
		{
			if (_dataList.get(i).getSelecionado())
			{
				percentual += _dataList.get(i).getPercentual();
				count++;
			}
		}

		if (preAmostragem.getNumeroCortes() != count)
		{
			FuncoesCompartilhadas
					.exibirToastMsgLong(
							context,
							String.format(
									"O n�mero de cortes informado: %d est� diferente do n�mero de classifica��es selecionadas: %d!",
									preAmostragem.getNumeroCortes(), count));
			return false;
		}

		if (preAmostragem.getTipo().equals(
				Constantes.TIPOAMOSTRAGEM_VALOR_PERCENTUAL))
		{

			if (percentual != 100)
			{
				FuncoesCompartilhadas.exibirToastMsgLong(context,
						"A soma dos percentuais deve ser 100%!");
				return false;
			}
		}

		if (daoPreAmostragemClassificacao.gravarClassificacao(_dataList) == false)
		{
			FuncoesCompartilhadas.exibirToastMsgLong(context,
					Constantes.MSG_ERRO_GRAVACAO);
			return false;
		}

		return true;
	}

	private void abreCapturaPeso()
	{
		// pesagem
		Intent intent = new Intent(context,
				ViewPreAmostragemSelecao100CapturaPeso.class);
		startActivity(intent);
	}

	private void atualizaBarraPercentual()
	{
		int ipercentual = 0;
		for (int i = 0; i < _dataList.size(); i++)
		{
			if (_dataList.get(i).getSelecionado())
			{
				ipercentual += _dataList.get(i).getPercentual();
			}
		}

		final TextView tvPercent = (TextView) findViewById(R.id.preamostragem_barra_percentual);
		tvPercent.setText("Total %: " + ipercentual);

		if (ipercentual < 100)
		{
			tvPercent.setBackgroundColor(Color.GRAY);
		} else if (ipercentual > 100)
		{
			tvPercent.setBackgroundColor(Color.RED);
		} else
		{
			tvPercent
					.setBackgroundColor(getResources().getColor(R.color.verde));
		}
	}

	private void calc_faixas_pre_amostragem()
	{
		if (preAmostragem != null)
		{
			String retorno = dao.calc_faixas_pre_amostragem(preAmostragem);
			if (retorno != null)
			{
				FuncoesCompartilhadas.exibirToastMsgLong(context, retorno);
				return;
			}

			// chama activity para exibir resultado
			Intent intent = new Intent(context,
					ViewPreAmostragemSelecao100Resultado.class);
			intent.putExtra(PreAmostragem.NRLOTE, preAmostragem.getNumeroLote());
			startActivity(intent);
		}
	}

	private void abreMenuContexto(int iQtdPesos)
	{
		final CharSequence[] items = { "1 - Manter Pesos", "2 - Limpar Pesos",
				"3 - Eliminar �ltimo Peso", "4 - Cancelar Leitura" };

		final AlertDialog.Builder dialog = new AlertDialog.Builder(context);
		dialog.setTitle(String.format(
				Constantes.MSG_EXISTEM_PESADOS_ACAO_TITLE, iQtdPesos));

		dialog.setItems(items, new DialogInterface.OnClickListener()
		{

			@Override
			public void onClick(DialogInterface dialog, int position)
			{
				switch (position) {
				case 0: // manter pesos
					abreCapturaPeso();
					break;

				case 1: // limpar pesos

					final AlertDialog.Builder dialog1 = FuncoesCompartilhadas
							.criaAlertDialogCustom(context,
									Constantes.MSG_CONFIRMACAO_LIMPEZA_PESOS,
									Constantes.MSG_CONFIRMACAO_EXCLUSAO_TITLE);

					dialog1.setIcon(android.R.drawable.ic_delete);
					dialog1.setNegativeButton("N�o", null);
					dialog1.setPositiveButton("Sim",
							new DialogInterface.OnClickListener()
							{

								@Override
								public void onClick(DialogInterface arg0,
										int arg1)
								{
									if (daoPreAmostragemPesos.limparPesos() == false)
									{
										FuncoesCompartilhadas
												.exibirToastMsgLong(
														context,
														Constantes.MSG_ERRO_GRAVACAO);
									} else
									{
										atualizaQtdAves();
									}
								}
							});

					dialog1.create().show();

					break;
				case 2: // eliminar ultimo peso

					final AlertDialog.Builder dialog2 = FuncoesCompartilhadas
							.criaAlertDialogCustom(
									context,
									Constantes.MSG_CONFIRMACAO_LIMPEZA_ULTIMO_PESO,
									Constantes.MSG_CONFIRMACAO_EXCLUSAO_TITLE);
					dialog2.setIcon(android.R.drawable.ic_delete);
					dialog2.setNegativeButton("N�o", null);
					dialog2.setPositiveButton("Sim",
							new DialogInterface.OnClickListener()
							{

								@Override
								public void onClick(DialogInterface arg0,
										int arg1)
								{
									String msg = daoPreAmostragemPesos
											.limparUltimoPeso();

									if (msg != null)
									{
										FuncoesCompartilhadas
												.exibirToastMsgLong(context,
														msg);
									} else
									{
										atualizaQtdAves();
									}
								}
							});

					dialog2.create().show();

					break;

				default:
					break;
				}
			}
		});

		dialog.create().show();
	}

	private void atualizaQtdAves()
	{
		if (preAmostragem != null)
		{
			if (preAmostragem.getTipo().equals(
					Constantes.TIPOAMOSTRAGEM_VALOR_PERCENTUAL))
			{
				int numeroaves = daoPreAmostragemPesos.count();
				if (dao.atualizarNumeroAvesTipoPercentual(numeroaves) == false)
				{
					FuncoesCompartilhadas.exibirToastMsgLong(context,
							Constantes.MSG_ERRO_GRAVACAO);
				} else
				{
					edtAves.setText(String.valueOf(numeroaves));
				}
			}
		}
	}

	class PreAmostragemClassificacaoItemAdapter extends
			ArrayAdapter<PreAmostragemClassificacaoItem>
	{
		private ArrayList<PreAmostragemClassificacaoItem> entries;
		private Context context;

		public PreAmostragemClassificacaoItemAdapter(Context context,
				int textViewResourceId,
				ArrayList<PreAmostragemClassificacaoItem> entries)
		{
			super(context, textViewResourceId, entries);
			this.entries = entries;
			this.context = context;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent)
		{
			View v = convertView;
			final ViewHolder holder;
			if (v == null)
			{
				LayoutInflater vi = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

				v = vi.inflate(
						R.layout.activity_view_pre_amostragem_selecao100_classificacao_grid_item,
						null);

				holder = new ViewHolder();
				holder.item1 = (CheckBox) v
						.findViewById(R.id.preamostragem_classificacao_selecionado);

				holder.item2 = (TextView) v
						.findViewById(R.id.preamostragem_classificacao_sigla);

				holder.item3 = (TextView) v
						.findViewById(R.id.preamostragem_classificacao_percentual);

				holder.item4 = (ImageButton) v
						.findViewById(R.id.preamostragem_classificacao_btnedit);

				v.setTag(holder);
			} else
				holder = (ViewHolder) v.getTag();

			holder.item1.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					CheckBox chk = (CheckBox) v;
					if (chk.isChecked())
					{
						entries.get(position).setSelecionado(true);
					} else
					{
						entries.get(position).setSelecionado(false);
					}

					notifyDataSetChanged();
				}
			});

			if (spTipo.getSelectedItemPosition() == Constantes.TIPOAMOSTRAGEM_INDEX_PERCENTUAL
					&& holder.item1.isChecked())
			{
				holder.item4.setEnabled(true);
			} else
			{
				holder.item4.setEnabled(false);
			}

			holder.item4.setOnClickListener(new View.OnClickListener()
			{

				@Override
				public void onClick(View v)
				{
					final PreAmostragemClassificacaoItem item = entries
							.get(position);

					if (item != null)
					{
						AlertDialog.Builder builder = new AlertDialog.Builder(
								context);
						builder.setTitle("Informe o percentual");

						final EditText input = new EditText(context);
						input.setTextSize(35);

						if (item.getPercentual() > 0)
							input.setText(String.valueOf(item.getPercentual()));

						input.setInputType(InputType.TYPE_CLASS_NUMBER);
						builder.setView(input);

						// Set up the buttons
						builder.setPositiveButton("OK",
								new DialogInterface.OnClickListener()
								{
									@Override
									public void onClick(DialogInterface dialog,
											int which)
									{

										if (input.getText().toString()
												.equals("") == false)
										{
											int s = Integer.parseInt(input
													.getText().toString());
											entries.get(position)
													.setPercentual(s);

											notifyDataSetChanged();
											atualizaBarraPercentual();
										}
									}
								});
						builder.setNegativeButton("Cancelar",
								new DialogInterface.OnClickListener()
								{
									@Override
									public void onClick(DialogInterface dialog,
											int which)
									{
										dialog.cancel();
									}
								});

						builder.show();
					}
				}
			});

			/* para ativar/desativar campos da tela */
			notifyDataSetChanged();

			final PreAmostragemClassificacaoItem custom = entries.get(position);
			if (custom != null)
			{
				holder.item1.setChecked(custom.getSelecionado());
				holder.item2.setText(custom.getClassificacao_sigla());
				if (custom.getPercentual() > 0)
					holder.item3
							.setText(String.valueOf(custom.getPercentual()));
				else
					holder.item3.setText("");
			}
			return v;
		}

		public class ViewHolder
		{
			public CheckBox item1;
			public TextView item2;
			public TextView item3;
			public ImageButton item4;
		}
	}
}
