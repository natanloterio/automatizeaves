package net.automatize.aves.view;

import net.automatize.aves.R;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ViewSelecao100 extends Activity
{
	final Context context = this;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_selecao100);

		final Button btnSelecao100 = (Button) findViewById(R.id.selecao100_btnSelecao100);
		btnSelecao100.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				startActivity(new Intent(context,
						ViewSelecao100Selecao100.class));
			}
		});

		final Button btnTransferencia = (Button) findViewById(R.id.selecao100_btnTransferencia);
		btnTransferencia.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{
				startActivity(new Intent(context,
						ViewSelecao100TransferenciaPeso.class));
			}
		});

		final Button btnRetornar = (Button) findViewById(R.id.selecao100_btnRetornar);
		btnRetornar.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				finish();
			}
		});
	}
//
//	@Override
//	public boolean onCreateOptionsMenu(Menu menu)
//	{
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.view_selecao100, menu);
//		return true;
//	}
}
