package net.automatize.aves.view;

import java.util.ArrayList;
import java.util.Date;

import net.automatize.aves.R;
import net.automatize.aves.dao.DAOPreAmostragem;
import net.automatize.aves.dao.DAOPreAmostragemClassificacao;
import net.automatize.aves.dao.DAOSelecao100;
import net.automatize.aves.dao.DAOSelecao100Classificacao;
import net.automatize.aves.dao.DAOSelecao100Pesos;
import net.automatize.aves.model.PreAmostragem;
import net.automatize.aves.model.PreAmostragemClassificacao;
import net.automatize.aves.model.Selecao100;
import net.automatize.aves.model.Selecao100ClassificacaoItem;
import net.automatize.aves.model.Selecao100PesoEstatistica;
import net.automatize.aves.util.Constantes;
import net.automatize.aves.util.FuncoesCompartilhadas;
import net.automatize.aves.util.FuncoesEspecificas;
import net.automatize.aves.util.Mask;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

public class ViewSelecao100Selecao100 extends Activity
{
	final Context context = this;

	private DAOSelecao100 dao = new DAOSelecao100(context);
	private DAOSelecao100Pesos daoSelecao100Pesos = new DAOSelecao100Pesos(
			context);
	private DAOSelecao100Classificacao daoSelecao100Classificacao = new DAOSelecao100Classificacao(
			context);
	private DAOPreAmostragemClassificacao daoPreAmostragemClassificacao = new DAOPreAmostragemClassificacao(
			context);

	private PreAmostragem preAmostragemCopiar = null;
	private Selecao100 selecao100 = null;
	private Selecao100PesoEstatistica selecao100PesoEstatistica = null;
	private Selecao100ClassificacaoItemAdapter adapter = null;
	private ArrayList<Selecao100ClassificacaoItem> _dataList = null;

	private ArrayAdapter<CharSequence> adapterSexo = null;

	private EditText edtLote, edtData;
	private Spinner spSexo;
	private ListView lv;

	private TextWatcher dataMask;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_selecao100_selecao100);

		adapterSexo = ArrayAdapter.createFromResource(context,
				R.array.sexo_array, R.layout.spinner_item);

		edtLote = (EditText) findViewById(R.id.selecao100_edtlote);
		spSexo = (Spinner) findViewById(R.id.selecao100_spsexo);
		spSexo.setAdapter(adapterSexo);

		edtData = (EditText) findViewById(R.id.selecao100_edtdata);
		dataMask = Mask.insert("##/##/####", edtData);
		edtData.addTextChangedListener(dataMask);

		carregaClassificacoes();
		carregaSelecao100();

		selecao100PesoEstatistica = daoSelecao100Pesos
				.getSelecao100PesosEstatistica();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.view_selecao100_selecao100, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Handle presses on the action bar items
		switch (item.getItemId()) {
		case R.id.selecao100_ab_btnsalvar:
			continuar();
			return true;

		case R.id.selecao100_ab_btncontinuar:
			continuar();
			return true;

		case R.id.selecao100_ab_btnresultado:
			abrirResultado();
			return true;

		case R.id.selecao100_ab_btncopiarpreamostragem:
			eventCopiarPreAmostragem();
			return true;

		case R.id.selecao100_ab_btnsair:
			finish();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void continuar()
	{
		if (gravarSelecao100())
		{
			if (gravarClassificacoes())
			{
				if (validaFaixa() == false)
					return;

				if (selecao100PesoEstatistica != null)
				{
					if (selecao100PesoEstatistica.getQtd() > 0)
					{
						// menu de op��es (context) - quando j� tem pesos //
						// coletados
						abreMenuContexto(selecao100PesoEstatistica.getQtd());

					} else
					{
						abreCapturaPeso();
					}
				}
			}
		}
	}

	private void carregaClassificacoes()
	{
		_dataList = new DAOSelecao100Classificacao(context)
				.carregaClassificacoesGrid();

		if (_dataList.isEmpty())
		{
			FuncoesCompartilhadas.exibirToastMsgLong(context,
					Constantes.MSG_NAO_HA_CLASSIFICACOES);
			return;
		}

		lv = (ListView) findViewById(R.id.selecao100_lvclassificacoes);
		adapter = new Selecao100ClassificacaoItemAdapter(context,
				R.id.selecao100_lvclassificacoes, _dataList);
		lv.setAdapter(adapter);
	}

	private boolean validaFaixa()
	{
		String retorno = daoSelecao100Classificacao.validaFaixa();
		if (retorno != null)
		{
			FuncoesCompartilhadas.log(retorno);
			FuncoesCompartilhadas.exibirToastMsgLong(context, retorno);
			return false;
		}
		return true;
	}

	private void abrirResultado()
	{
		if (selecao100 != null)
		{
			Intent intent = new Intent(context,
					ViewSelecao100Selecao100Resultado.class);
			intent.putExtra(Selecao100.NRLOTE, selecao100.getNrlote());
			startActivity(intent);
		}
	}

	private void carregaSelecao100()
	{
		selecao100 = dao.getSelecao100();

		if (selecao100 != null)
		{
			edtLote.setText(selecao100.getNrlote());

			if (selecao100.getSexo().equals(Constantes.SEXO_VALOR_FEMEA))
				spSexo.setSelection(Constantes.SEXO_INDEX_FEMEA);
			else if (selecao100.getSexo().equals(Constantes.SEXO_VALOR_MACHO))
				spSexo.setSelection(Constantes.SEXO_INDEX_MACHO);
			else
				spSexo.setSelection(Constantes.SEXO_INDEX_AMBOS);

			edtData.removeTextChangedListener(dataMask);
			edtData.setText(FuncoesCompartilhadas.formataData(selecao100
					.getData()));
			edtData.addTextChangedListener(dataMask);
		} else
		{
			edtData.removeTextChangedListener(dataMask);
			edtData.setText(FuncoesCompartilhadas.formataData(new Date()));
			edtData.addTextChangedListener(dataMask);
		}
	}

	private void eventCopiarPreAmostragem()
	{
		preAmostragemCopiar = new DAOPreAmostragem(context).getPreAmostragem();
		if (preAmostragemCopiar == null)
		{
			FuncoesCompartilhadas.exibirToastMsgLong(context,
					"N�o h� pr�-amostragem para ser copiada");
			return;
		}

		if (contarClassificacoesSelecionadas() > 0)
		{
			final AlertDialog.Builder dialog = FuncoesCompartilhadas
					.criaAlertDialogCustom(
							context,
							"Deseja substituir as faixas existentes pelo resultado da pr� amostragem?",
							"Confirma��o");

			dialog.setPositiveButton("Sim",
					new DialogInterface.OnClickListener()
					{

						@Override
						public void onClick(DialogInterface arg0, int arg1)
						{
							copiarPreAmostragem();
						}
					});
			dialog.setNegativeButton("N�o", null);
			dialog.create().show();
		} else
		{
			copiarPreAmostragem();
		}
	}

	private void copiarPreAmostragem()
	{
		if (preAmostragemCopiar != null)
		{
			String retorno = new DAOPreAmostragem(context)
					.calc_faixas_pre_amostragem(preAmostragemCopiar);
			if (retorno != null)
			{
				FuncoesCompartilhadas.log(retorno);
				FuncoesCompartilhadas.exibirToastMsgLong(context, retorno);
				return;
			}

			ArrayList<PreAmostragemClassificacao> lista_faixas = daoPreAmostragemClassificacao
					.getClassificacaoParaCopiaSelecao100();

			if (lista_faixas.isEmpty())
			{
				FuncoesCompartilhadas.log("N�o h� classifica��es calculadas");
				return;
			}

			retorno = dao
					.copiarPreAmostragem(lista_faixas, preAmostragemCopiar);
			if (retorno != null)
			{
				FuncoesCompartilhadas.log(retorno);
				FuncoesCompartilhadas.exibirToastMsgLong(context, retorno);
				return;
			}

			carregaSelecao100();
			atualizaClassificacoes();
		}
	}

	private void atualizaClassificacoes()
	{
		if (adapter != null)
		{
			adapter.clear();
			adapter.addAll(new DAOSelecao100Classificacao(context)
					.carregaClassificacoesGrid());
			adapter.notifyDataSetChanged();
		}
	}

	private int contarClassificacoesSelecionadas()
	{
		if (_dataList == null)
			return 0;

		int count = 0;
		for (int i = 0; i < _dataList.size(); i++)
		{
			if (_dataList.get(i).getSelecionado())
				count++;
		}
		return count;
	}

	private boolean gravarSelecao100()
	{
		// edtLote = (EditText) findViewById(R.id.selecao100_edtlote);
		// spSexo = (Spinner) findViewById(R.id.selecao100_spsexo);
		// spSexo.setAdapter(adapterSexo);

		// edtData = (EditText) findViewById(R.id.selecao100_edtdata);

		String lote = edtLote.getText().toString();
		if (lote.trim().equals(""))
		{
			FuncoesCompartilhadas.exibirToastMsgLong(context,
					String.format(Constantes.MSG_CAMPO_NAO_INFORMADO, "Lote"));
			edtLote.requestFocus();
			return false;
		}

		// valida o lote
		if (FuncoesEspecificas.ValidaNumeroLote(lote) == false)
		{
			FuncoesCompartilhadas.exibirToastMsgLong(context,
					"N�mero do Lote inv�lido!");
			edtLote.requestFocus();
			return false;
		}

		/* sexo */

		String sexo = null;
		switch (spSexo.getSelectedItemPosition()) {
		case Constantes.SEXO_INDEX_FEMEA:
			sexo = Constantes.SEXO_VALOR_FEMEA;
			break;

		case Constantes.SEXO_INDEX_MACHO:
			sexo = Constantes.SEXO_VALOR_MACHO;
			break;

		case Constantes.SEXO_INDEX_AMBOS:
			sexo = Constantes.SEXO_VALOR_AMBOS;
			break;

		default:
			break;
		}

		if (sexo == null)
		{
			FuncoesCompartilhadas.exibirToastMsgLong(context,
					String.format(Constantes.MSG_CAMPO_NAO_INFORMADO, "Sexo"));
			spSexo.requestFocus();
			return false;
		}

		/* data */

		String sdata = edtData.getText().toString();
		if (sdata.equals(""))
		{
			FuncoesCompartilhadas.exibirToastMsgLong(context,
					String.format(Constantes.MSG_CAMPO_NAO_INFORMADO, "Data"));
			edtData.requestFocus();
			return false;
		}

		Date data = FuncoesCompartilhadas.strToDate(sdata);

		if (data == null)
		{
			FuncoesCompartilhadas.exibirToastMsgLong(context, "Data inv�lida");
			return false;
		}

		if (selecao100 == null)
			selecao100 = new Selecao100();

		if (selecao100.getCodigo() > 0)
		{
			if (selecao100.getData().compareTo(data) != 0)
			{
				data = selecao100.getData();
			}
		}

		/* grava��o bd */

		selecao100.setNrlote(lote);
		selecao100.setData(data);
		selecao100.setSexo(sexo);

		if (dao.gravar(selecao100) == 0)
		{
			FuncoesCompartilhadas.exibirToastMsgLong(context,
					Constantes.MSG_ERRO_GRAVACAO);
			return false;
		}

		return true;
	}

	private boolean gravarClassificacoes()
	{
		String retorno = daoSelecao100Classificacao
				.gravarClassificacao(_dataList);
		if (retorno != null)
		{
			FuncoesCompartilhadas.exibirToastMsgLong(context, retorno);
			return false;
		}

		return true;
	}

	private boolean contaPesosNaFaixa(int classificacao_codigo)
	{
		if (classificacao_codigo > 0)
		{
			int qtd = daoSelecao100Pesos
					.contaPesosNaFaixa(classificacao_codigo);
			if (qtd > 0)
			{
				// FuncoesCompartilhadas.log(qtd);
				FuncoesCompartilhadas.exibirToastMsgShort(context,
						"Imposs�vel desmarc�-la!");
				FuncoesCompartilhadas.exibirToastMsgLong(context,
						"Existem pesos capturados nesta classifica��o!");

				return true;
			}
		}

		return false;
	}

	private void abreCapturaPeso()
	{
		// pesagem
		Intent intent = new Intent(context,
				ViewSelecao100Selecao100CapturaPeso.class);
		startActivity(intent);
	}

	private void abreMenuContexto(int iQtdPesos)
	{
		final CharSequence[] items = { "1 - Manter Pesos", "2 - Limpar Pesos",
				"3 - Eliminar �ltimo Peso", "4 - Cancelar Leitura" };

		final AlertDialog.Builder dialog = new AlertDialog.Builder(context);
		dialog.setTitle(String.format(
				Constantes.MSG_EXISTEM_PESADOS_ACAO_TITLE, iQtdPesos));
		dialog.setItems(items, new DialogInterface.OnClickListener()
		{

			@Override
			public void onClick(DialogInterface dialog, int position)
			{
				FuncoesCompartilhadas.log(position);

				switch (position) {
				case 0: // manter pesos
					abreCapturaPeso();
					break;

				case 1: // limpar pesos

					// AlertDialog.Builder dialog1 = new AlertDialog.Builder(
					// context);
					// dialog1.setTitle(Constantes.MSG_CONFIRMACAO_EXCLUSAO_TITLE);
					// dialog1.setMessage(Constantes.MSG_CONFIRMACAO_LIMPEZA_PESOS);

					final AlertDialog.Builder dialog1 = FuncoesCompartilhadas
							.criaAlertDialogCustom(context,
									Constantes.MSG_CONFIRMACAO_LIMPEZA_PESOS,
									Constantes.MSG_CONFIRMACAO_EXCLUSAO_TITLE);

					dialog1.setIcon(android.R.drawable.ic_delete);
					dialog1.setNegativeButton("N�o", null);
					dialog1.setPositiveButton("Sim",
							new DialogInterface.OnClickListener()
							{

								@Override
								public void onClick(DialogInterface arg0,
										int arg1)
								{
									if (daoSelecao100Pesos.limparPesos() == false)
									{
										FuncoesCompartilhadas
												.exibirToastMsgLong(
														context,
														Constantes.MSG_ERRO_GRAVACAO);
									}
								}
							});

					dialog1.create().show();

					break;
				case 2: // eliminar ultimo peso

					// AlertDialog.Builder dialog2 = new AlertDialog.Builder(
					// context);
					// dialog2.setTitle(Constantes.MSG_CONFIRMACAO_EXCLUSAO_TITLE);
					// dialog2.setMessage(Constantes.MSG_CONFIRMACAO_LIMPEZA_ULTIMO_PESO);

					final AlertDialog.Builder dialog2 = FuncoesCompartilhadas
							.criaAlertDialogCustom(
									context,
									Constantes.MSG_CONFIRMACAO_LIMPEZA_ULTIMO_PESO,
									Constantes.MSG_CONFIRMACAO_EXCLUSAO_TITLE);

					dialog2.setIcon(android.R.drawable.ic_delete);
					dialog2.setNegativeButton("N�o", null);
					dialog2.setPositiveButton("Sim",
							new DialogInterface.OnClickListener()
							{

								@Override
								public void onClick(DialogInterface arg0,
										int arg1)
								{
									String msg = daoSelecao100Pesos
											.limparUltimoPeso();

									if (msg != null)
									{
										FuncoesCompartilhadas
												.exibirToastMsgLong(context,
														msg);
									}
								}
							});

					dialog2.create().show();

					break;

				default:
					break;
				}

			}
		});

		dialog.create().show();
	}

	class Selecao100ClassificacaoItemAdapter extends
			ArrayAdapter<Selecao100ClassificacaoItem>
	{
		private ArrayList<Selecao100ClassificacaoItem> entries;
		private Context context;

		public Selecao100ClassificacaoItemAdapter(Context context,
				int textViewResourceId,
				ArrayList<Selecao100ClassificacaoItem> entries)
		{
			super(context, textViewResourceId, entries);
			this.entries = entries;
			this.context = context;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent)
		{
			View v = convertView;
			final ViewHolder holder;
			if (v == null)
			{
				LayoutInflater vi = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

				v = vi.inflate(
						R.layout.activity_view_selecao100_classificacao_grid_item,
						null);

				holder = new ViewHolder();
				holder.item1 = (CheckBox) v
						.findViewById(R.id.selecao100_classificacao_selecionado);

				holder.item2 = (TextView) v
						.findViewById(R.id.selecao100_classificacao_sigla);

				holder.item3 = (TextView) v
						.findViewById(R.id.selecao100_classificacao_faixainicial);
				holder.item4 = (TextView) v
						.findViewById(R.id.selecao100_classificacao_faixafinal);

				holder.item5 = (ImageButton) v
						.findViewById(R.id.selecao100_classificacao_btnedit);

				v.setTag(holder);
			} else
				holder = (ViewHolder) v.getTag();

			holder.item1.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					CheckBox chk = (CheckBox) v;
					if (chk.isChecked())
					{
						entries.get(position).setSelecionado(true);
						FuncoesCompartilhadas.log(position);
					} else
					{
						if (contaPesosNaFaixa(entries.get(position)
								.getClassificacao_codigo()) == false)
						{
							entries.get(position).setSelecionado(false);
						}
					}

					notifyDataSetChanged();
				}
			});

			holder.item5.setEnabled(holder.item1.isChecked());

			holder.item5.setOnClickListener(new View.OnClickListener()
			{

				@Override
				public void onClick(View v)
				{
					final Selecao100ClassificacaoItem item = entries
							.get(position);

					if (item != null && item.getSelecionado())
					{
						final Dialog dialog = new Dialog(context);
						dialog.setContentView(R.layout.activity_view_selecao100_classificacao_configura_faixa);
						dialog.setCanceledOnTouchOutside(false);
						dialog.setTitle("Configura��o para a classifica��o "
								+ item.getClassificacao_sigla());

						final EditText edt1 = (EditText) dialog
								.findViewById(R.id.selecao100_classificacao_configura_faixainicial);
						if (item.getFaixainicial() > 0)
							edt1.setText(String.valueOf(item.getFaixainicial()));

						final EditText edt2 = (EditText) dialog
								.findViewById(R.id.selecao100_classificacao_configura_faixafinal);
						if (item.getFaixafinal() > 0)
							edt2.setText(String.valueOf(item.getFaixafinal()));

						final Button btnCancelar = (Button) dialog
								.findViewById(R.id.selecao100_classificacao_configura_btncancelar);
						btnCancelar
								.setOnClickListener(new View.OnClickListener()
								{

									@Override
									public void onClick(View v)
									{
										dialog.dismiss();
									}
								});

						final Button btnGravar = (Button) dialog
								.findViewById(R.id.selecao100_classificacao_configura_btngravar);
						btnGravar.setOnClickListener(new View.OnClickListener()
						{

							@Override
							public void onClick(View v)
							{
								if (FuncoesCompartilhadas.EditTextIsEmpty(edt1))
								{
									FuncoesCompartilhadas.exibirToastMsgLong(
											context,
											String.format(
													Constantes.MSG_CAMPO_NAO_INFORMADO,
													getResources()
															.getString(
																	R.string.labelFaixainicial)));
									edt1.requestFocus();
									return;
								}

								if (FuncoesCompartilhadas.EditTextIsEmpty(edt2))
								{
									FuncoesCompartilhadas.exibirToastMsgLong(
											context,
											String.format(
													Constantes.MSG_CAMPO_NAO_INFORMADO,
													getResources()
															.getString(
																	R.string.labelFaixafinal)));
									edt2.requestFocus();
									return;
								}

								int faixainicial = Integer.parseInt(edt1
										.getText().toString());
								int faixafinal = Integer.parseInt(edt2
										.getText().toString());

								if (faixainicial <= 0)
								{
									FuncoesCompartilhadas
											.exibirToastMsgLong(context,
													"Faixa Inicial deve ser igual ou maior que 1!");
									return;
								}

								if (faixafinal <= faixainicial)
								{
									FuncoesCompartilhadas
											.exibirToastMsgLong(context,
													"Faixa Final deve ser maior que a faixa inicial!");
									return;
								}

								Selecao100ClassificacaoItem selecao100Classificacao = new Selecao100ClassificacaoItem();
								selecao100Classificacao.setSelecionado(true);
								selecao100Classificacao
										.setClassificacao_codigo(item
												.getClassificacao_codigo());
								selecao100Classificacao
										.setFaixainicial(faixainicial);
								selecao100Classificacao
										.setFaixafinal(faixafinal);

								String retorno = daoSelecao100Classificacao
										.gravarClassificacao(selecao100Classificacao);
								if (retorno != null)
								{
									FuncoesCompartilhadas.exibirToastMsgLong(
											context, retorno);
									return;
								}

								dialog.dismiss();

								carregaClassificacoes();
							}

						});

						dialog.show();
					}
				}
			});

			/* para ativar/desativar campos da tela */
			notifyDataSetChanged();

			final Selecao100ClassificacaoItem custom = entries.get(position);
			if (custom != null)
			{
				holder.item1.setChecked(custom.getSelecionado());
				holder.item2.setText(custom.getClassificacao_sigla());

				if (custom.getFaixainicial() > 0)
					holder.item3.setText(String.valueOf(custom
							.getFaixainicial()));
				else
					holder.item3.setText("");

				if (custom.getFaixafinal() > 0)
					holder.item4
							.setText(String.valueOf(custom.getFaixafinal()));
				else
					holder.item4.setText("");
			}
			return v;
		}

		public class ViewHolder
		{
			public CheckBox item1;
			public TextView item2;
			public TextView item3;
			public TextView item4;
			public ImageButton item5;
		}
	}
}
