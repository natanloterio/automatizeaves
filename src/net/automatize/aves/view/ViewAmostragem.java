package net.automatize.aves.view;

import net.automatize.aves.R;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ViewAmostragem extends Activity
{
	final Context context = this;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_amostragem);

		final Button btnAmostragemSemanalAmostragemSemanal = (Button) findViewById(R.id.btnAmostragemSemanalAmostragemSemanal);
		btnAmostragemSemanalAmostragemSemanal
				.setOnClickListener(new View.OnClickListener()
				{

					@Override
					public void onClick(View arg0)
					{
						startActivity(new Intent(context,
								ViewAmostragemSemanalSelecaoLote.class));
					}
				});

		final Button btnTransferenciaPesos = (Button) findViewById(R.id.btnAmostragemSemanalTransferenciaPeso);
		btnTransferenciaPesos.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{
				startActivity(new Intent(context,
						ViewAmostragemTransferenciaPeso.class));
			}
		});

		final Button btnRetornar = (Button) findViewById(R.id.btnAmostragemSemanalRetornar);
		btnRetornar.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				finish();
			}
		});
	}
//
//	@Override
//	public boolean onCreateOptionsMenu(Menu menu)
//	{
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.view_amostragem, menu);
//		return true;
//	}
}
