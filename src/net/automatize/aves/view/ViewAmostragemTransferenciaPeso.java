package net.automatize.aves.view;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.automatize.aves.R;
import net.automatize.aves.dao.DAOAmostragemData;
import net.automatize.aves.dao.DAOAmostragemPesos;
import net.automatize.aves.dao.DAOParametros;
import net.automatize.aves.filechooser.FileChooser;
import net.automatize.aves.model.AmostragemTransferenciaPeso;
import net.automatize.aves.model.Parametros;
import net.automatize.aves.util.Constantes;
import net.automatize.aves.util.FuncoesCompartilhadas;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class ViewAmostragemTransferenciaPeso extends Activity
{
	final Context context = this;
	private Parametros parametros = null;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_amostragem_transferencia_pesos);

		getActionBar().setTitle("Transfer�ncia de Pesos da Amostragem");

		/* Carrega itens transferencia de pesos */
		/* Inicializa campos */
		final Spinner spTransferenciaPesoNumeroLote = (Spinner) findViewById(R.id.spAmostragemTransferenciaPesoNrLote);
		final Spinner spTransferenciaPesoTamanho = (Spinner) findViewById(R.id.spAmostragemTransferenciaPesoTamanho);

		// Numero do lote
		List<String> listNrLote = new ArrayList<String>();
		listNrLote.addAll(Constantes.TRANSFERENCIA_PESO_NRLOTE);

		ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(context,
				android.R.layout.simple_spinner_item, listNrLote);
		dataAdapter1
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spTransferenciaPesoNumeroLote.setAdapter(dataAdapter1);

		// Tamanho
		List<String> listTamanho = new ArrayList<String>();
		listTamanho.addAll(Constantes.TRANSFERENCIA_PESO_TAMANHO);

		ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(context,
				android.R.layout.simple_spinner_item, listTamanho);
		dataAdapter2
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spTransferenciaPesoTamanho.setAdapter(dataAdapter2);

		final EditText edtTransferenciaPesoBalanca = (EditText) findViewById(R.id.edtAmostragemTransferenciaPesoBalanca);

		/* Recupera parametros */
		DAOParametros daoParametros = new DAOParametros(context);
		Parametros p = daoParametros.getParametros();

		if (p != null)
		{
			spTransferenciaPesoNumeroLote.setSelection(p.getNumeroLote());
			spTransferenciaPesoTamanho.setSelection(p.getTamanhoNumeroLote());
			edtTransferenciaPesoBalanca.setText(String.valueOf(p
					.getCodigoBalanca()));
		}

		// bot�es
		final Button btnOk = (Button) findViewById(R.id.btnAmostragemTransferenciaPesoOk);
		btnOk.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{
				short iNumeroLote = (short) spTransferenciaPesoNumeroLote
						.getSelectedItemPosition();
				short iTamanho = (short) spTransferenciaPesoTamanho
						.getSelectedItemPosition();

				if (edtTransferenciaPesoBalanca.getText().toString() == null
						|| edtTransferenciaPesoBalanca.getText().toString()
								.equals(""))
				{
					FuncoesCompartilhadas.exibirToastMsgLong(context, String
							.format(Constantes.MSG_CAMPO_NAO_INFORMADO,
									"Balan�a"));
					return;
				}

				short codigoBalanca = Short
						.parseShort(edtTransferenciaPesoBalanca.getText()
								.toString());

				if (codigoBalanca == 0)
				{
					FuncoesCompartilhadas.exibirToastMsgLong(context,
							Constantes.MSG_CODIGO_BALANCA_INVALIDO);
					edtTransferenciaPesoBalanca.requestFocus();
					return;
				}

				parametros = new Parametros(
						Constantes.PARAMETROS_TRANSFERENCIA_CODIGO,
						iNumeroLote, iTamanho, codigoBalanca);

				if (gravarCamposTransferenciaPeso() == false)
					return;

				transferenciaPesos();
			}
		});

		final Button btnCancelar = (Button) findViewById(R.id.btnAmostragemTransferenciaPesoCancelar);
		btnCancelar.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{
				finish();
			}
		});
	}

//	@Override
//	public boolean onCreateOptionsMenu(Menu menu)
//	{
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.view_amostragem_transferencia_peso,
//				menu);
//		return true;
//	}

	private boolean gravarCamposTransferenciaPeso()
	{
		DAOParametros daoParametros = new DAOParametros(context);
		if (daoParametros.gravar(parametros) == 0)
		{
			FuncoesCompartilhadas.exibirToastMsgLong(context,
					Constantes.MSG_ERRO_GRAVACAO);
			return false;
		}

		return true;
	}

	private void transferenciaPesos()
	{
		Intent intent = new Intent(context, FileChooser.class);
		startActivityForResult(intent, 1);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if (requestCode == 1)
		{
			if (resultCode == RESULT_OK)
			{
				if (data.getExtras().containsKey(FileChooser.FILECHOOSER_KEY))
				{
					String s = data.getExtras().getString(
							FileChooser.FILECHOOSER_KEY);
					if (exporta(s))
					{
						FuncoesCompartilhadas.exibirToastMsgLong(context,
								Constantes.MSG_SALVA_ARQUIVO_SUCESSO);
					}
				}
			}
		} else
		{
			FuncoesCompartilhadas.exibirToastMsgLong(context,
					"Opera��o cancelada");
		}
	}

	private boolean exporta(String path)
	{
		// listagem dos dados
		List<AmostragemTransferenciaPeso> dados = new DAOAmostragemPesos(
				context).lerPesosParaTransferencia();
		if (dados.isEmpty())
		{
			FuncoesCompartilhadas.exibirToastMsgLong(context,
					Constantes.MSG_TRANSFERENCIA_NAO_HA_DADOS);
			return false;
		}

		if (parametros != null)
		{
			Date vdata = new DAOAmostragemData(context).getAmostragemData();
			if (vdata != null)
			{
				try
				{
					String snomearquivo = FuncoesCompartilhadas
							.formataNomeArquivoTransferenciaPeso("PesoAves",
									parametros.getCodigoBalanca(), vdata);

					File f = new File(path + File.separator + snomearquivo);
					if (f.exists())
						f.delete();

					f.createNewFile();

					FileWriter fw = new FileWriter(f);
					BufferedWriter bw = new BufferedWriter(fw);

					String vStrData, vLin, vLastLote, vLote, vPesoStr;
					int vSeqPeso;
					short vLastBox;

					bw.write(FuncoesCompartilhadas.formataDataHora(new Date()));
					bw.write(Constantes.TXT_NOVA_LINHA);

					vLastLote = "";
					vLastBox = -1;
					vSeqPeso = -1;

					vStrData = FuncoesCompartilhadas.formataData("dd/MM/yy",
							vdata);

					for (int i = 0; i < dados.size(); i++)
					{
						vLote = dados.get(i).getNr_lote();
						vLote = VerificaLote(vLote);

						if (vLote == null)
							continue;

						if ((vLastLote.equals(vLote) == false)
								|| vLastBox != dados.get(i).getNr_box())
						{
							vLastLote = vLote;
							vLastBox = dados.get(i).getNr_box();
							vSeqPeso = 1;
						}

						vPesoStr = FuncoesCompartilhadas
								.formataPesoTransferencia(FuncoesCompartilhadas
										.grToKg(dados.get(i).getPeso()));

						vPesoStr = vPesoStr.replace(",", ".");

						vLin = vLote + " " + vStrData + " "
								+ dados.get(i).getSexo() + " "
								+ String.format("%02d", vLastBox) + " "
								+ String.format("%05d", vSeqPeso) + " "
								+ dados.get(i).getClassificacao_codigo()
								+ vPesoStr;

						bw.write(vLin);
						bw.write(Constantes.TXT_NOVA_LINHA);

						vSeqPeso++;
					}

					bw.flush();
					bw.close();

					/* criando a copia do arquivo */
					File fOriginal = new File(
							Constantes.DIRETORIO_AMOSTRAGEMSEMANAL
									+ snomearquivo.replace(".txt", ".ori"));
					if (FuncoesCompartilhadas.copiarArquivo(f, fOriginal) == false)
					{
						FuncoesCompartilhadas.exibirToastMsgLong(
								context,
								"Erro ao criar o arquivo "
										+ fOriginal.getName());
						return false;
					}

					return true;

				} catch (IOException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return false;
	}

	private String VerificaLote(String lote)
	{
		lote = lote.replace("-", "");
		int vTam = lote.length();
		int vMax = parametros.getTamanhoNumeroLote() + 1;

		if (vTam > vMax)
		{
			FuncoesCompartilhadas.exibirToastMsgLong(context,
					"Tamanho do Lote superior ao especificado!");
		} else
		{
			if (vTam != vMax)
			{
				if (parametros.getNumeroLote() == Constantes.TRANSFERENCIA_NUMEROLOTE_INDEX_ZERO_ESQUERDA)
				{
					while (vMax > vTam)
					{
						lote = "0" + lote;
						vTam = lote.length();
					}
				} else
				{
					while (vMax > vTam)
					{
						lote = lote + " ";
						vTam = lote.length();
					}
				}

				return lote;
			}
		}
		return null;
	}
}
