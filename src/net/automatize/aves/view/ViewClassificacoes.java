package net.automatize.aves.view;

import java.util.ArrayList;

import net.automatize.aves.R;
import net.automatize.aves.adapter.ClassificacoesAdapter;
import net.automatize.aves.dao.DAOClassificacao;
import net.automatize.aves.dao.DAOUtilitarios;
import net.automatize.aves.model.Classificacao;
import net.automatize.aves.util.Constantes;
import net.automatize.aves.util.FuncoesCompartilhadas;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

public class ViewClassificacoes extends Activity
{
	final Context context = this;

	private ArrayList<Classificacao> _dataList = null;
	private Classificacao _classificacaoSelecionada = null;
	private ClassificacoesAdapter adapter = null;
	private DAOClassificacao dao = new DAOClassificacao(this);
	private boolean bAlterar = false;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_classificacoes);

		_dataList = new ArrayList<Classificacao>();

		final ListView lv = (ListView) findViewById(R.id.lvClassificacoes);
		adapter = new ClassificacoesAdapter(ViewClassificacoes.this,
				R.id.lvClassificacoes, _dataList);

		carregaDados();

		lv.setAdapter(adapter);

		registerForContextMenu(lv);

		lv.setOnItemLongClickListener(new OnItemLongClickListener()
		{

			@Override
			public boolean onItemLongClick(AdapterView<?> adapter, View view,
					int position, long id)
			{
				_classificacaoSelecionada = _dataList.get(position);
				return false;
			}
		});

		final Button btnNovo = (Button) findViewById(R.id.btnClassificacoesNovo);
		btnNovo.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				abrirClassificacoesCadastro(null);
			}
		});

		final Button btnVoltar = (Button) findViewById(R.id.btnClassificacoesVoltar);
		btnVoltar.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				finish();
			}
		});

		final Button btnClassificacoesPadrao = (Button) findViewById(R.id.btnClassificacoesPadrao);
		btnClassificacoesPadrao.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if (dao.inserirPadrao() == false)
				{
					FuncoesCompartilhadas.exibirToastMsgLong(
							ViewClassificacoes.this,
							Constantes.MSG_ERRO_GRAVACAO);
				} else
				{
					carregaDados();
				}
			}
		});
	}

	/*
	 * Menu de contexto (suspenso)
	 */

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo)
	{
		super.onCreateContextMenu(menu, v, menuInfo);
		menu.setHeaderTitle(Constantes.MSG_TITLE_MENU_CONTEXT);
		menu.add(0, v.getId(), 0, "Alterar");
		menu.add(0, v.getId(), 0, "Excluir");
		menu.add(0, v.getId(), 0, "Cancelar");
	}

	@Override
	public boolean onContextItemSelected(MenuItem item)
	{
		try
		{
			if (_classificacaoSelecionada != null)
			{
				if (item.getTitle() == "Alterar")
				{
					abrirClassificacoesCadastro(_classificacaoSelecionada);
					carregaDados();

				} else if (item.getTitle() == "Excluir")
				{

					if (new DAOUtilitarios(context)
							.classificacaoEmUso(_classificacaoSelecionada
									.getCodigo()))
					{
						FuncoesCompartilhadas.exibirToastMsgLong(context,
								"Imposs�vel excluir!");
						FuncoesCompartilhadas.exibirToastMsgLong(context,
								"Este registro possui movimento associado.");
						return false;
					}

					// AlertDialog.Builder alertDialog = new
					// AlertDialog.Builder(
					// ViewClassificacoes.this);
					//
					// alertDialog
					// .setTitle(Constantes.MSG_CONFIRMACAO_EXCLUSAO_TITLE);
					// alertDialog
					// .setMessage(Constantes.MSG_CONFIRMACAO_EXCLUSAO_MSG);

					final AlertDialog.Builder dialog = FuncoesCompartilhadas
							.criaAlertDialogCustom(context,
									Constantes.MSG_CONFIRMACAO_EXCLUSAO_MSG,
									Constantes.MSG_CONFIRMACAO_EXCLUSAO_TITLE);

					dialog.setIcon(android.R.drawable.ic_delete);
					dialog.setPositiveButton("Sim",
							new DialogInterface.OnClickListener()
							{

								@Override
								public void onClick(DialogInterface dialog,
										int which)
								{

									DAOClassificacao dao = new DAOClassificacao(
											ViewClassificacoes.this);
									if (dao.excluir(_classificacaoSelecionada
											.getCodigo()) > 0)
									{
										FuncoesCompartilhadas
												.exibirToastMsgLong(
														context,
														Constantes.MSG_REGISTRO_EXCLUIDO_SUCESSO);
										_dataList
												.remove(_classificacaoSelecionada);
										adapter.notifyDataSetChanged();
									} else
									{
										FuncoesCompartilhadas
												.exibirToastMsgLong(
														context,
														Constantes.MSG_ERRO_GRAVACAO);
									}
								}
							});

					dialog.setNegativeButton("N�o",
							new DialogInterface.OnClickListener()
							{

								@Override
								public void onClick(DialogInterface dialog,
										int which)
								{
									dialog.cancel();
								}
							});
					dialog.create().show();
				}
			}
			return true;
		} catch (Exception e)
		{
			return true;
		}
	}

	private void abrirClassificacoesCadastro(Classificacao c)
	{
		final Dialog dialog = new Dialog(context);
		dialog.setTitle(getString(R.string.title_activity_view_classificacoes_cadastro));
		dialog.setContentView(R.layout.activity_view_classificacoes_cadastro);
		dialog.setCanceledOnTouchOutside(false);

		final EditText edtCodigo = (EditText) dialog
				.findViewById(R.id.classificacoes_cadastro_edtCodigo);
		final EditText edtSigla = (EditText) dialog
				.findViewById(R.id.classificacoes_cadastro_edtSigla);

		bAlterar = false;
		if (c != null)
		{
			edtCodigo.setText(String.valueOf(c.getCodigo()));
			edtSigla.setText(c.getSigla());
			bAlterar = true;
			edtCodigo.setEnabled(false);
		}

		final Button btnSalvar = (Button) dialog
				.findViewById(R.id.classificacoes_cadastro_btnSalvar);
		btnSalvar.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				short iCodigo = 0;

				if (edtCodigo.getText().toString().equals("") == false)
				{
					iCodigo = Short.parseShort(edtCodigo.getText().toString());
					if (iCodigo < 1)
					{
						FuncoesCompartilhadas
								.exibirToastMsgLong(context,
										"Imposs�vel registrar c�digo de classifica��o inferior a 1!");
						return;
					}
				}

				if (iCodigo == 0)
				{
					iCodigo = dao.proximoCodigo();
					if (iCodigo <= 0)
					{
						FuncoesCompartilhadas.exibirToastMsgLong(context,
								Constantes.MSG_ERRO_GRAVACAO);
						return;
					}
				} else
				{
					/* Procura registro j� cadastrado com mesmo Codigo */
					if (bAlterar == false)
					{
						Classificacao procuraRegistro = dao
								.getClassificacao(iCodigo);
						if (procuraRegistro != null)
						{
							FuncoesCompartilhadas.exibirToastMsgLong(context,
									String.format("C�digo %d j� cadastrado.",
											iCodigo));
							edtCodigo.requestFocus();
							return;
						}
					}
				}

				String sSigla = edtSigla.getText().toString();
				if (sSigla == null || sSigla.trim().equals(""))
				{
					FuncoesCompartilhadas.exibirToastMsgLong(
							context,
							"Campo Descri��o obrigat�rio");
					edtSigla.requestFocus();
					return;
				}

				if (dao.siglaCadastrada(iCodigo, sSigla))
				{
					FuncoesCompartilhadas.exibirToastMsgLong(context,
							"Sigla j� cadastrada");
					return;
				}

				Classificacao classificacoes = new Classificacao(iCodigo,
						sSigla);
				if (bAlterar)
				{
					if (dao.alterar(classificacoes) == 0)
					{
						FuncoesCompartilhadas.exibirToastMsgLong(context,
								Constantes.MSG_ERRO_GRAVACAO);
						return;
					}
				} else
				{

					if (dao.inserir(classificacoes) == 0)
					{
						FuncoesCompartilhadas.exibirToastMsgLong(context,
								Constantes.MSG_ERRO_GRAVACAO);
						return;
					}
				}

				carregaDados();
				dialog.dismiss();
			}
		});

		final Button btnVoltar = (Button) dialog
				.findViewById(R.id.classificacoes_cadastro_btnCancelar);
		btnVoltar.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				dialog.dismiss();
			}
		});

		dialog.show();
	}

	private void carregaDados()
	{
		if (adapter != null)
		{
			adapter.clear();
			adapter.addAll(dao.getClassificacoes(false));
			adapter.notifyDataSetChanged();
		}
	}
}
