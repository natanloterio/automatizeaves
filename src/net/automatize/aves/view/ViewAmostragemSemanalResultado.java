package net.automatize.aves.view;

import java.util.ArrayList;

import net.automatize.aves.R;
import net.automatize.aves.adapter.AmostragemResultadoAdapter;
import net.automatize.aves.dao.DAOAmostragemResultado;
import net.automatize.aves.model.AmostragemLote;
import net.automatize.aves.model.AmostragemResultado;
import net.automatize.aves.util.Constantes;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

public class ViewAmostragemSemanalResultado extends Activity
{
	final Context context = this;

	private DAOAmostragemResultado dao = new DAOAmostragemResultado(context);
	private int amostragemLote_codigo;
	private String amostragemLote_nrlote;
	private AmostragemResultadoAdapter adapterBoxes = null;
	private ArrayList<AmostragemResultado> _dataList = null;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_amostragem_semanal_resultado);

		Bundle b = getIntent().getExtras();
		if (b != null)
		{
			this.amostragemLote_codigo = b.getInt(AmostragemLote.CODIGO);
			this.amostragemLote_nrlote = b.getString(AmostragemLote.NR_LOTE);

			getActionBar().setTitle(
					String.format(
							Constantes.MSG_AMOSTRAGEMSEMANAL_RESULTADO_TITLE,
							amostragemLote_nrlote));
			init();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.view_amostragem_semanal_resultado,
				menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Handle presses on the action bar items
		switch (item.getItemId()) {
		case R.id.amostragemsemanal_resultado_ab_btnsair:
			finish();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void init()
	{
		/* Lista de pesos */
		final ListView lvBoxes = (ListView) findViewById(R.id.amostragemsemanal_resultado_lvboxes);

		_dataList = new ArrayList<AmostragemResultado>();
		_dataList.addAll(dao.getResultados(amostragemLote_codigo));

		adapterBoxes = new AmostragemResultadoAdapter(context,
				R.id.amostragemsemanal_resultado_lvboxes, _dataList);

		lvBoxes.setAdapter(adapterBoxes);
	}
}
