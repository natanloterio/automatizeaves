package net.automatize.aves.view;

import java.util.ArrayList;

import net.automatize.aves.R;
import net.automatize.aves.adapter.PreAmostragemSelecao100ResultadoAdapter;
import net.automatize.aves.dao.DAOPreAmostragemResultado;
import net.automatize.aves.model.PreAmostragem;
import net.automatize.aves.model.PreAmostragemSelecao100Resultado;
import net.automatize.aves.util.FuncoesCompartilhadas;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

public class ViewPreAmostragemSelecao100Resultado extends Activity
{
	final Context context = this;

	private PreAmostragemSelecao100ResultadoAdapter adapter = null;
	private ListView lv = null;
	private DAOPreAmostragemResultado dao = new DAOPreAmostragemResultado(
			context);
	private ArrayList<PreAmostragemSelecao100Resultado> _dataList = null;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_pre_amostragem_selecao100_resultado);

		Bundle b = getIntent().getExtras();
		if (b != null)
		{
			String title = getResources()
					.getString(
							R.string.title_activity_view_pre_amostragem_selecao100_resultado);
			title = String.format(title, b.getString(PreAmostragem.NRLOTE));
			getActionBar().setTitle(title);
		}
		carregaResultado();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(
				R.menu.view_pre_amostragem_selecao100_resultado, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Handle presses on the action bar items
		switch (item.getItemId()) {
		case R.id.preamostragem_resultado_ab_sair:
			finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void carregaResultado()
	{
		_dataList = dao.getResultado();

		if (_dataList.isEmpty())
		{
			FuncoesCompartilhadas.exibirToastMsgLong(context,
					"N�o h� resultados");
			return;
		}

		lv = (ListView) findViewById(R.id.preamostragem_resultado_lv);
		adapter = new PreAmostragemSelecao100ResultadoAdapter(context,
				R.id.preamostragem_resultado_lv, _dataList);
		lv.setAdapter(adapter);
	}
}
