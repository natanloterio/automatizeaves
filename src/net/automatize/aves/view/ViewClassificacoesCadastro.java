package net.automatize.aves.view;

import net.automatize.aves.R;
import net.automatize.aves.dao.DAOClassificacao;
import net.automatize.aves.model.Classificacao;
import net.automatize.aves.util.Constantes;
import net.automatize.aves.util.FuncoesCompartilhadas;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ViewClassificacoesCadastro extends Activity
{
	final Context context = this;

	private boolean bAlterar = false;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_classificacoes_cadastro);

		Bundle bundle = getIntent().getExtras();
		if (bundle != null)
		{
			bAlterar = true;

			Short sCodigo = bundle.getShort(Classificacao.CODIGO);
			String sSigla = bundle.getString(Classificacao.SIGLA);

			Classificacao c = new Classificacao(sCodigo, sSigla);
			carregaDadosForm(c);
		}

		final Button btnSalvar = (Button) findViewById(R.id.classificacoes_cadastro_btnSalvar);
		btnSalvar.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				if (salvar())
				{
					if (bAlterar)
					{
						FuncoesCompartilhadas.exibirToastMsgLong(context,
								Constantes.MSG_REGISTRO_ALTERADO_SUCESSO);
					} else
					{
						FuncoesCompartilhadas.exibirToastMsgLong(context,
								Constantes.MSG_REGISTRO_INCLUIDO_SUCESSO);
					}
					setResult(1);
					finish();
				}
			}
		});

		final Button btnVoltar = (Button) findViewById(R.id.classificacoes_cadastro_btnCancelar);
		btnVoltar.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				setResult(0);
				finish();
			}
		});

	}

	public void carregaDadosForm(Classificacao c)
	{
		final EditText edtCodigo = (EditText) findViewById(R.id.classificacoes_cadastro_edtCodigo);
		edtCodigo.setText(String.valueOf(c.getCodigo()));
		edtCodigo.setEnabled(false);

		final EditText edtSigla = (EditText) findViewById(R.id.classificacoes_cadastro_edtSigla);
		edtSigla.setText(c.getSigla());
	}

	public boolean salvar()
	{
		final EditText edtCodigo = (EditText) findViewById(R.id.classificacoes_cadastro_edtCodigo);
		final EditText edtSigla = (EditText) findViewById(R.id.classificacoes_cadastro_edtSigla);

		short iCodigo = 0;

		if (edtCodigo.getText().toString().equals("") == false)
			iCodigo = Short.parseShort(edtCodigo.getText().toString());

		String sSigla = edtSigla.getText().toString();

		DAOClassificacao dao = new DAOClassificacao(this);

		if (iCodigo == 0)
		{
			iCodigo = dao.proximoCodigo();
			if (iCodigo <= 0)
			{
				FuncoesCompartilhadas.exibirToastMsgLong(context,
						Constantes.MSG_ERRO_GRAVACAO);
				return false;
			}
		} else
		{
			/* Procura registro j� cadastrado com mesmo Codigo */
			if (bAlterar == false)
			{
				Classificacao procuraRegistro = dao.getClassificacao(iCodigo);
				if (procuraRegistro != null)
				{
					FuncoesCompartilhadas.exibirToastMsgLong(context,
							String.format("C�digo %d j� cadastrado.", iCodigo));
					edtCodigo.requestFocus();
					return false;
				}
			}
		}

		if (sSigla == null || sSigla.trim().equals(""))
		{
			FuncoesCompartilhadas.exibirToastMsgLong(context,
					"Campo Descri��o obrigat�rio");
			edtSigla.requestFocus();
			return false;
		}

		Classificacao classificacoes = new Classificacao(iCodigo, sSigla);
		if (bAlterar)
		{
			if (dao.alterar(classificacoes) > 0)
			{
				return true;
			}
		} else
		{

			if (dao.inserir(classificacoes) > 0)
			{
				return true;
			}
		}
		return false;
	}
}
