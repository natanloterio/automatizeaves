package net.automatize.aves.view;

import java.util.ArrayList;
import java.util.Set;

import net.automatize.aves.R;
import net.automatize.aves.comunicacao.ComunicacaoBT;
import net.automatize.aves.dao.DAOTipoPesagem;
import net.automatize.aves.dao.DAOUtilitarios;
import net.automatize.aves.model.TipoPesagem;
import net.automatize.aves.util.Constantes;
import net.automatize.aves.util.FuncoesCompartilhadas;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Toast;

public class ViewUtilitarios extends Activity
{
	final Context context = this;

	private ArrayAdapter<String> listAdapter;
	private Set<BluetoothDevice> devicesArray;
	private ArrayList<String> pairedDevices;
	private ArrayList<BluetoothDevice> devices;
	private IntentFilter filter;
	private BroadcastReceiver receiver;

	/* Parametros de configura��o */
	private Dialog dialogParametrosPesagem;
	private EditText edtPesoMinimo;
	private RadioGroup rgTipoPesagem;
	private RadioButton rbAutomatico;
	private RadioButton rbManual;
	private EditText edtMacAddress;

	/* Calibracao */
	private Dialog dialog;
	private EditText edtCapacidade;
	private EditText edtDivisao;
	private EditText edtPesoConhecido;
	private EditText edtTempoPesagem;
	private int calibracaoCapacidade;
	private int calibracaoDivisao;
	private int calibracaoPesoconhecido;
	private int calibracaoTempo;

	/* objetos para a calibra��o e teste de comunica��o */
	private String macAddress = null;
	private ComunicacaoBT comunicacao = new ComunicacaoBT();
	private ProgressDialog mDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_utilitarios);

		final Button btnLimparMemoriaPesos = (Button) findViewById(R.id.btnLimparMemoriaPesos);
		btnLimparMemoriaPesos.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				final AlertDialog.Builder dialog = FuncoesCompartilhadas
						.criaAlertDialogCustom(context,
								"Confirma a limpeza da Mem�ria de Pesos?",
								Constantes.MSG_CONFIRMACAO_EXCLUSAO_TITLE);

				dialog.setIcon(android.R.drawable.ic_delete);
				dialog.setPositiveButton(android.R.string.ok,
						new DialogInterface.OnClickListener()
						{

							@Override
							public void onClick(DialogInterface dialog,
									int which)
							{
								DAOUtilitarios daoUtilitarios = new DAOUtilitarios(
										context);
								if (daoUtilitarios.limparMemoriaPesos())
								{
									FuncoesCompartilhadas
											.exibirToastMsgLong(context,
													"Mem�ria de Pesos limpa com sucesso!");
								}
							}
						});

				dialog.setNegativeButton(android.R.string.cancel, null);

				dialog.create().show();
			}
		});

		final Button btnCalibracao = (Button) findViewById(R.id.btnCalibracao);
		btnCalibracao.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				abrirCalibracaoBalanca();
			}
		});

		final Button btnParametroPesagem = (Button) findViewById(R.id.btnParametros);
		btnParametroPesagem.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				abrirParametrosPesagem();
			}
		});

		final Button btnClassificacoes = (Button) findViewById(R.id.btnClassificacoes);
		btnClassificacoes.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				abrirClassificacoes();
			}
		});

		final Button btnRetornar = (Button) findViewById(R.id.btnRetornar);
		btnRetornar.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				finish();
			}
		});
	}

	private void startDiscovery()
	{
		if (Constantes.btAdapter != null)
		{
			if (Constantes.btAdapter.isDiscovering())
				Constantes.btAdapter.cancelDiscovery();
			Constantes.btAdapter.startDiscovery();
		}
	}

	private void getPairedDevices()
	{
		// TODO Auto-generated method stub
		devicesArray = Constantes.btAdapter.getBondedDevices();
		if (devicesArray.size() > 0)
		{
			for (BluetoothDevice device : devicesArray)
			{
				pairedDevices.add(device.getName());
			}
		}
	}

	private void init()
	{
		listAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, 0);

		pairedDevices = new ArrayList<String>();
		filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
		devices = new ArrayList<BluetoothDevice>();

		getPairedDevices();
		startDiscovery();

		receiver = new BroadcastReceiver()
		{
			@Override
			public void onReceive(Context context, Intent intent)
			{
				String action = intent.getAction();

				if (BluetoothDevice.ACTION_FOUND.equals(action))
				{
					BluetoothDevice device = intent
							.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
					devices.add(device);
					String s = "";

					listAdapter.clear();

					for (int a = 0; a < pairedDevices.size(); a++)
					{
						if (device.getName().equals(pairedDevices.get(a)))
						{
							// append
							s = "(Paired)";
							break;
						}
					}

					listAdapter.add(device.getName() + " " + s + " " + "\n"
							+ device.getAddress());
				} else if (BluetoothAdapter.ACTION_DISCOVERY_STARTED
						.equals(action))
				{
					// run some code
				} else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED
						.equals(action))
				{
					// run some code
				}
			}
		};

		registerReceiver(receiver, filter);
		filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
		registerReceiver(receiver, filter);
		filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
		registerReceiver(receiver, filter);
		filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
		registerReceiver(receiver, filter);
	}

	@Override
	protected void onPause()
	{
		super.onPause();

		try
		{
			if (receiver != null)
			{
				unregisterReceiver(receiver);
			}
		} catch (IllegalArgumentException e)
		{
			receiver = null;
		}

		comunicacao.fechar();
	}

	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		comunicacao.fechar();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_CANCELED)
		{
			FuncoesCompartilhadas.exibirToastMsgShort(context,
					"Bluetooth precisa estar habilitado para continuar");
			finish();
		}
	}

	private void abrirCalibracaoBalanca()
	{
		dialog = new Dialog(context);
		dialog.setContentView(R.layout.activity_view_calibracao);
		dialog.setCanceledOnTouchOutside(false);
		dialog.setTitle("Calibra��o da Balan�a");

		edtCapacidade = (EditText) dialog
				.findViewById(R.id.edtCalibracaoCapacidade);
		edtDivisao = (EditText) dialog.findViewById(R.id.edtCalibracaoDivisao);
		edtPesoConhecido = (EditText) dialog
				.findViewById(R.id.edtCalibracaoPesoConhecido);
		edtTempoPesagem = (EditText) dialog
				.findViewById(R.id.edtCalibracaoTempoPesagem);

		/*
		 * valores default
		 */
		edtCapacidade.setText("10000");
		edtDivisao.setText("10");
		edtPesoConhecido.setText("2000");
		edtTempoPesagem.setText("6");

		Button btnOk = (Button) dialog.findViewById(R.id.btnOkCalibracao);
		btnOk.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{

				if (FuncoesCompartilhadas.EditTextIsEmpty(edtCapacidade))
				{
					FuncoesCompartilhadas.exibirToastMsgLong(context, String
							.format(Constantes.MSG_CAMPO_NAO_INFORMADO,
									"CAPACIDADE"));
					return;
				}

				if (FuncoesCompartilhadas.EditTextIsEmpty(edtDivisao))
				{
					FuncoesCompartilhadas.exibirToastMsgLong(context, String
							.format(Constantes.MSG_CAMPO_NAO_INFORMADO,
									"DIVIS�O"));
					return;
				}

				if (FuncoesCompartilhadas.EditTextIsEmpty(edtPesoConhecido))
				{
					FuncoesCompartilhadas.exibirToastMsgLong(context, String
							.format(Constantes.MSG_CAMPO_NAO_INFORMADO,
									"PESO CONHECIDO"));

					return;
				}

				if (FuncoesCompartilhadas.EditTextIsEmpty(edtTempoPesagem))
				{
					FuncoesCompartilhadas.exibirToastMsgLong(context, String
							.format(Constantes.MSG_CAMPO_NAO_INFORMADO,
									"TEMPO DE PESAGEM"));
					return;
				}

				calibracaoCapacidade = Integer.parseInt(edtCapacidade.getText()
						.toString());
				calibracaoDivisao = Integer.parseInt(edtDivisao.getText()
						.toString());
				calibracaoPesoconhecido = Integer.parseInt(edtPesoConhecido
						.getText().toString());
				calibracaoTempo = Integer.parseInt(edtTempoPesagem.getText()
						.toString());

				if (validaParametrosCalibracao(calibracaoDivisao,
						calibracaoCapacidade, calibracaoPesoconhecido))
				{
					// fun��o que realiza a calibra��o
					setupCalibracao();
				}
			}
		});

		Button btnCancelar = (Button) dialog
				.findViewById(R.id.btnCalcelarCalibracao);
		btnCancelar.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				dialog.dismiss();
			}
		});

		dialog.setCanceledOnTouchOutside(false);
		dialog.show();
	}

	private boolean validaParametrosCalibracao(int idivisao, int icapacidade,
			int ipesoconhecido)
	{
		if (idivisao < 5)
		{
			if (icapacidade > 2000)
			{
				FuncoesCompartilhadas.exibirToastMsgShort(context,
						"Rela��o de Peso / Divis�o inv�lida!");
				return false;
			}
		} else if (idivisao > 9)
		{
			if (icapacidade > 70000)
			{
				FuncoesCompartilhadas.exibirToastMsgShort(context,
						"Rela��o de Peso / Divis�o inv�lida!");
				return false;
			}
		} else
		{
			if (icapacidade > 10000)
			{
				FuncoesCompartilhadas.exibirToastMsgShort(context,
						"Rela��o de Peso / Divis�o inv�lida!");
				return false;
			}
		}

		if (ipesoconhecido > icapacidade)
		{
			FuncoesCompartilhadas.exibirToastMsgShort(context,
					"Peso conhecido superior a capacidade da balan�a!");
			return false;
		}

		return true;
	}

	private void setupCalibracao()
	{
		if (FuncoesCompartilhadas.bluetoothIsSupported(context) == false)
			return;

		if (macAddress == null)
			macAddress = new DAOTipoPesagem(context).getMacAddress();

		if (macAddress != null)
		{
			CalibracaoAsyncTask mAsyncTask = new CalibracaoAsyncTask();
			mAsyncTask.execute(String.valueOf(calibracaoCapacidade),
					String.valueOf(calibracaoDivisao),
					String.valueOf(calibracaoPesoconhecido),
					String.valueOf(calibracaoTempo), macAddress);
		} else
		{
			FuncoesCompartilhadas.exibirToastMsgLong(context,
					Constantes.MSG_BALANCA_NAO_CONFIGURADA);
			return;
		}
	}

	private void abrirParametrosPesagem()
	{
		dialogParametrosPesagem = new Dialog(context);
		dialogParametrosPesagem
				.setContentView(R.layout.activity_view_parametros_pesagem);
		dialogParametrosPesagem.setCanceledOnTouchOutside(false);
		dialogParametrosPesagem.setTitle("Par�metros para Pesagem");

		/*
		 * Listeners
		 */

		Button btnOk = (Button) dialogParametrosPesagem
				.findViewById(R.id.btnConfiguracoesOk);
		btnOk.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if (gravarParametrosPesagem())
					dialogParametrosPesagem.dismiss();
			}
		});

		Button btnCancelar = (Button) dialogParametrosPesagem
				.findViewById(R.id.btnCancelarParametrosPesagem);
		btnCancelar.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				dialogParametrosPesagem.dismiss();
			}
		});

		/* elementos da view para variaveis */
		edtPesoMinimo = (EditText) dialogParametrosPesagem
				.findViewById(R.id.parametros_edtpesominimo);

		rgTipoPesagem = (RadioGroup) dialogParametrosPesagem
				.findViewById(R.id.rgParametrosTipoPesagem);

		rbAutomatico = (RadioButton) dialogParametrosPesagem
				.findViewById(R.id.parametros_rgautomatica);
		rbManual = (RadioButton) dialogParametrosPesagem
				.findViewById(R.id.parametros_rgmanual);

		edtMacAddress = (EditText) dialogParametrosPesagem
				.findViewById(R.id.parametros_edtmac);

		rgTipoPesagem.setOnCheckedChangeListener(new OnCheckedChangeListener()
		{
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId)
			{
				if (checkedId == rbAutomatico.getId())
				{
					edtPesoMinimo.setEnabled(true);
				} else
				{
					edtPesoMinimo.getText().clear();
					edtPesoMinimo.setEnabled(false);
				}
			}
		});

		// carrega configura��es j� salvas
		carregaParametrosPesagem();

		final Button btnSelecionarBT = (Button) dialogParametrosPesagem
				.findViewById(R.id.btnConfiguracaoSelecionarBT);
		btnSelecionarBT.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				if (FuncoesCompartilhadas.bluetoothIsSupported(context) == false)
					finish();

				if (!Constantes.btAdapter.isEnabled())
				{
					FuncoesCompartilhadas.turnOnBT(ViewUtilitarios.this);
					return;
				}

				final AlertDialog.Builder builder = new AlertDialog.Builder(
						context);
				builder.setTitle("Selecione o dispositivo pareado");
				init();

				builder.setAdapter(listAdapter, new OnClickListener()
				{

					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						if (Constantes.btAdapter.isDiscovering())
						{
							Constantes.btAdapter.cancelDiscovery();
						}

						BluetoothDevice selectedDevice = devices.get(which);
						edtMacAddress.setText(selectedDevice.getAddress());
					}
				});
				builder.create().show();
			}
		});

		final Button btnTestarConexao = (Button) dialogParametrosPesagem
				.findViewById(R.id.btnConfiguracaoTestar);
		btnTestarConexao.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				if (Constantes.btAdapter.isDiscovering())
					Constantes.btAdapter.cancelDiscovery();

				if (gravarParametrosPesagem())
				{
					if (edtMacAddress.getText().toString().equals("") == false
							&& BluetoothAdapter
									.checkBluetoothAddress(edtMacAddress
											.getText().toString()))
					{
						macAddress = edtMacAddress.getText().toString();

						if (macAddress != null)
						{
							TesteComunicacaoAsyncTask testeComunicacaoAsyncTask = new TesteComunicacaoAsyncTask();
							testeComunicacaoAsyncTask.execute(macAddress);
						}
					} else
					{
						FuncoesCompartilhadas.exibirToastMsgLong(context,
								Constantes.MSG_BALANCA_NAO_CONFIGURADA);
					}
				}
			}
		});

		dialogParametrosPesagem.show();
	}

	private boolean gravarParametrosPesagem()
	{
		TipoPesagem tipoPesagem = new TipoPesagem();

		// tipo de pesagem
		if (rgTipoPesagem.getCheckedRadioButtonId() == rbAutomatico.getId())
			tipoPesagem.setTipoPesagem(Constantes.TIPO_PESAGEM_AUTOMATICO);
		else
			tipoPesagem.setTipoPesagem(Constantes.TIPO_PESAGEM_MANUAL);

		// peso minimo
		if (rgTipoPesagem.getCheckedRadioButtonId() == rbAutomatico.getId())
		{
			if (edtPesoMinimo.getText() == null
					|| edtPesoMinimo.getText().toString().trim().equals(""))
			{
				FuncoesCompartilhadas.exibirToastMsgShort(
						dialogParametrosPesagem.getContext(), String.format(
								Constantes.MSG_CAMPO_NAO_INFORMADO,
								"Peso M�nimo"));
				edtPesoMinimo.requestFocus();
				return false;
			} else
			{
				int pesoMinimo = Integer.parseInt(edtPesoMinimo.getText()
						.toString());
				if (pesoMinimo == 0)
				{
					FuncoesCompartilhadas.exibirToastMsgShort(
							dialogParametrosPesagem.getContext(),
							"O Peso M�nimo deve ser maior que 0");
					edtPesoMinimo.requestFocus();
					return false;
				} else
				{
					tipoPesagem.setPesoMinimo(pesoMinimo);
				}
			}
		}

		// mac address
		if (edtMacAddress.getText().toString() != null)
			tipoPesagem.setMacAddress(edtMacAddress.getText().toString());

		DAOTipoPesagem daoTipoPesagem = new DAOTipoPesagem(
				dialogParametrosPesagem.getContext());

		if (daoTipoPesagem.gravar(tipoPesagem) == 0)
		{
			return false;
		}

		return true;
	}

	private void carregaParametrosPesagem()
	{
		TipoPesagem t = new DAOTipoPesagem(context).getTipoPesagem();
		if (t != null)
		{
			// peso minimo
			edtPesoMinimo.setText(String.valueOf(t.getPesoMinimo()));

			// tipo de pesagem
			if (t.getTipoPesagem() == Constantes.TIPO_PESAGEM_AUTOMATICO)
				rbAutomatico.setChecked(true);
			else
				rbManual.setChecked(true);

			// mac address
			edtMacAddress.setText(t.getMacAddress());
		}
	}

	private void abrirClassificacoes()
	{
		Intent intent = new Intent(this, ViewClassificacoes.class);
		startActivity(intent);
	}

	private class CalibracaoAsyncTask extends AsyncTask<String, String, Void>
	{

		/* antes */
		@Override
		protected void onPreExecute()
		{
			mDialog = ProgressDialog.show(context,
					"Aguarde! Calibra��o em andamento",
					"Iniciando comunica��o...", false, false);
		}

		/* processamento */
		@Override
		protected Void doInBackground(String... progress)
		{
			comunicacao.inicializaComunicacaoBalanca(progress[4]); // mac

			int count = 0;
			while (comunicacao.bConectado == false
					&& count < Constantes.TEMPO_TENTATIVA_CONEXAO)
			{
				try
				{
					Thread.sleep(100);
				} catch (InterruptedException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				count += 100;
			}

			if (comunicacao.bConectado == false)
			{
				FuncoesCompartilhadas.log("Conexao falhou");
				publishProgress("Conex�o falhou!",
						Constantes.ASYNCTASK_MSG_FALHA);
				try
				{
					Thread.sleep(2000);
				} catch (InterruptedException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				cancel(true);

			} else
			{
				FuncoesCompartilhadas.log("Conex�o OK!");
			}

			if (isCancelled() == false)
			{

				final int calibracaoCapacidade = Integer.parseInt(progress[0]);
				final int calibracaoDivisao = Integer.parseInt(progress[1]);
				final int calibracaoPesoconhecido = Integer
						.parseInt(progress[2]);
				final int calibracaoTempo = Integer.parseInt(progress[3]);

				Log.i(Constantes.TAG, "Ativando Modo Calibra��o...");
				publishProgress("Ativando Modo Calibra��o...");
				comunicacao.enviaComando(Constantes.CALIBRACAO);

				Log.i(Constantes.TAG, "Velocidade...");
				publishProgress("Velocidade...");
				comunicacao
						.enviaComando(String.format("%d%sI", calibracaoTempo,
								String.format("%06d", calibracaoTempo)));

				Log.i(Constantes.TAG, "Capacidade...");
				publishProgress("Capacidade...");
				comunicacao.enviaComando(String.format("%dI",
						calibracaoCapacidade));

				Log.i(Constantes.TAG, "Divis�o...");
				publishProgress("Divis�o...");
				comunicacao.enviaComando(String
						.format("%dI", calibracaoDivisao));

				Log.i(Constantes.TAG, "Cell001....");
				publishProgress("Cell001....");
				comunicacao.enviaComando("I");

				Log.i(Constantes.TAG, "Ponto....");
				publishProgress("Ponto....");
				comunicacao.enviaComando("3I");

				Log.i(Constantes.TAG, "Cal 0...");
				publishProgress("Cal 0...");
				comunicacao.enviaComando("I");

				Log.i(Constantes.TAG, String.format(
						"Coloque o peso conhecido na plataforma!\n"
								+ "Peso: %d", calibracaoPesoconhecido));

				try
				{
					int tempoTotal = 20;
					int tempo = 0;
					do
					{
						publishProgress(String.format(
								"Coloque o peso conhecido na plataforma!\n"
										+ "Peso: %d", calibracaoPesoconhecido)
								+ "\n\n"
								+ "Tempo restante: "
								+ (tempoTotal - (tempo)));

						Thread.sleep(1000);

					} while (tempo++ < tempoTotal);

				} catch (InterruptedException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				Log.i(Constantes.TAG, "Cal P...");
				publishProgress("Cal P...");
				comunicacao.enviaComando(String.format("%dI",
						calibracaoPesoconhecido));

				publishProgress("Retire o peso da plataforma!");

				try
				{
					Thread.sleep(2500);
				} catch (InterruptedException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return null;
		}

		/* depois que terminou */
		@Override
		protected void onPostExecute(Void result)
		{
			mDialog.dismiss();
			Toast.makeText(context, "Calibra��o conclu�da", Toast.LENGTH_SHORT)
					.show();
			comunicacao.fechar();
		}

		/* atualizar a interface */
		@Override
		protected void onProgressUpdate(String... values)
		{
			super.onProgressUpdate(values);
			mDialog.setMessage(values[0]);
		}

		@Override
		protected void onCancelled()
		{
			comunicacao.fechar();
			mDialog.dismiss();
		}
	}

	private class TesteComunicacaoAsyncTask extends
			AsyncTask<String, String, Void>
	{

		/* antes */
		@Override
		protected void onPreExecute()
		{
			if (comunicacao.isConnected())
			{
				comunicacao.fechar();
			}

			mDialog = ProgressDialog.show(context, "Aguarde",
					"Iniciando comunica��o...", false, false);
		}

		@Override
		protected Void doInBackground(String... params)
		{
			comunicacao.inicializaComunicacaoBalanca(params[0]);

			int count = 0;
			while (comunicacao.bConectado == false
					&& count < Constantes.TEMPO_TENTATIVA_CONEXAO)
			{
				try
				{
					Thread.sleep(100);
				} catch (InterruptedException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				count += 100;
			}

			if (comunicacao.bConectado == false)
			{
				publishProgress("Conex�o falhou!");
				try
				{
					Thread.sleep(2000);
				} catch (InterruptedException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				cancel(true);

			} else
			{
				publishProgress("Conectado com sucesso!");

				try
				{
					Thread.sleep(2000);
				} catch (InterruptedException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return null;
		}

		/* atualizar a interface */
		@Override
		protected void onProgressUpdate(String... values)
		{
			super.onProgressUpdate(values);
			mDialog.setMessage(values[0]);
		}

		/* depois que terminou */
		@Override
		protected void onPostExecute(Void result)
		{
			comunicacao.fechar();
			mDialog.dismiss();
		}

		@Override
		protected void onCancelled()
		{
			comunicacao.fechar();
			mDialog.dismiss();
		}
	}
}
