package net.automatize.aves.view;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.automatize.aves.R;
import net.automatize.aves.dao.DAOParametros;
import net.automatize.aves.dao.DAOSelecao100;
import net.automatize.aves.dao.DAOSelecao100Resultado;
import net.automatize.aves.filechooser.FileChooser;
import net.automatize.aves.model.Parametros;
import net.automatize.aves.model.PreAmostragemSelecao100Resultado;
import net.automatize.aves.model.Selecao100;
import net.automatize.aves.util.Constantes;
import net.automatize.aves.util.FuncoesCompartilhadas;
import net.automatize.aves.util.FuncoesEspecificas;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class ViewSelecao100TransferenciaPeso extends Activity
{
	final Context context = this;
	private Parametros parametros = null;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_pre_amostragem_transferencia_pesos);

		getActionBar().setTitle("Transfer�ncia de Pesos da Sele��o 100%");

		/* Carrega itens transferencia de pesos */
		/* Inicializa campos */
		final Spinner spTransferenciaPesoNumeroLote = (Spinner) findViewById(R.id.preamostragem_transferencia_nrlote);
		final Spinner spTransferenciaPesoTamanho = (Spinner) findViewById(R.id.preamostragem_transferencia_tamanho);

		// Numero do lote
		List<String> listNrLote = new ArrayList<String>();
		listNrLote.addAll(Constantes.TRANSFERENCIA_PESO_NRLOTE);

		ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(context,
				android.R.layout.simple_spinner_item, listNrLote);
		dataAdapter1
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spTransferenciaPesoNumeroLote.setAdapter(dataAdapter1);

		// Tamanho
		List<String> listTamanho = new ArrayList<String>();
		listTamanho.addAll(Constantes.TRANSFERENCIA_PESO_TAMANHO);

		ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(context,
				android.R.layout.simple_spinner_item, listTamanho);
		dataAdapter2
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spTransferenciaPesoTamanho.setAdapter(dataAdapter2);

		final EditText edtTransferenciaPesoBalanca = (EditText) findViewById(R.id.preamostragem_transferencia_balanca);

		/* Recupera parametros */
		DAOParametros daoParametros = new DAOParametros(context);
		Parametros p = daoParametros.getParametros();

		if (p != null)
		{
			spTransferenciaPesoNumeroLote.setSelection(p.getNumeroLote());
			spTransferenciaPesoTamanho.setSelection(p.getTamanhoNumeroLote());
			edtTransferenciaPesoBalanca.setText(String.valueOf(p
					.getCodigoBalanca()));
		}

		// bot�es
		final Button btnOk = (Button) findViewById(R.id.preamostragem_transferencia_btnOk);
		btnOk.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{
				short iNumeroLote = (short) spTransferenciaPesoNumeroLote
						.getSelectedItemPosition();
				short iTamanho = (short) spTransferenciaPesoTamanho
						.getSelectedItemPosition();

				if (FuncoesCompartilhadas
						.EditTextIsEmpty(edtTransferenciaPesoBalanca))
				{
					FuncoesCompartilhadas.exibirToastMsgLong(context, String
							.format(Constantes.MSG_CAMPO_NAO_INFORMADO,
									"Balan�a"));
					return;
				}

				short codigoBalanca = Short
						.parseShort(edtTransferenciaPesoBalanca.getText()
								.toString());

				if (codigoBalanca == 0)
				{
					FuncoesCompartilhadas.exibirToastMsgLong(context,
							Constantes.MSG_CODIGO_BALANCA_INVALIDO);
					edtTransferenciaPesoBalanca.requestFocus();
					return;
				}

				parametros = new Parametros(
						Constantes.PARAMETROS_TRANSFERENCIA_CODIGO,
						iNumeroLote, iTamanho, codigoBalanca);

				if (gravarCamposTransferenciaPeso() == false)
					return;

				selecionaDiretorio();
			}
		});

		final Button btnCancelar = (Button) findViewById(R.id.preamostragem_transferencia_btnCancelar);
		btnCancelar.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{
				finish();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		// getMenuInflater().inflate(R.menu.view_amostragem_transferencia_peso,
		// menu);
		return true;
	}

	private boolean gravarCamposTransferenciaPeso()
	{
		DAOParametros daoParametros = new DAOParametros(context);
		if (daoParametros.gravar(parametros) == 0)
		{
			FuncoesCompartilhadas.exibirToastMsgLong(context,
					Constantes.MSG_ERRO_GRAVACAO);
			return false;
		}

		return true;
	}

	private void selecionaDiretorio()
	{
		Intent intent = new Intent(context, FileChooser.class);
		startActivityForResult(intent, 1);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if (requestCode == 1)
		{
			if (resultCode == RESULT_OK)
			{
				if (data.getExtras().containsKey(FileChooser.FILECHOOSER_KEY))
				{
					String s = data.getExtras().getString(
							FileChooser.FILECHOOSER_KEY);
					if (exporta(s))
					{
						FuncoesCompartilhadas.exibirToastMsgLong(context,
								Constantes.MSG_SALVA_ARQUIVO_SUCESSO);
					}
				}
			}
		} else
		{
			FuncoesCompartilhadas.exibirToastMsgLong(context,
					"Opera��o cancelada");
		}
	}

	private boolean exporta(String path)
	{
		DAOSelecao100 dao = new DAOSelecao100(context);
		Selecao100 selecao100 = dao.getSelecao100();

		if (selecao100 == null)
		{
			FuncoesCompartilhadas.exibirToastMsgLong(context,
					Constantes.MSG_TRANSFERENCIA_NAO_HA_DADOS);
			return false;
		}

		DAOSelecao100Resultado daoResultado = new DAOSelecao100Resultado(
				context);
		List<PreAmostragemSelecao100Resultado> dados = daoResultado
				.getResultado();

		if (dados.isEmpty())
		{
			FuncoesCompartilhadas.exibirToastMsgLong(context,
					"N�o h� resultados");
			return false;
		}

		if (parametros != null)
		{
			Date vdata = selecao100.getData();
			if (vdata != null)
			{
				FileWriter fw = null;
				BufferedWriter bw = null;

				try
				{
					String snomearquivo = FuncoesCompartilhadas
							.formataNomeArquivoTransferenciaPeso("Selecao100",
									parametros.getCodigoBalanca(), vdata);

					File f = new File(path + File.separator + snomearquivo);

					if (f.exists())
						f.delete();

					f.createNewFile();

					fw = new FileWriter(f);
					bw = new BufferedWriter(fw);

					String vLin, vLote, vFaixaIni, vFaixaFim, vQtdPesos, vSomaPesos, vMediaPesos;

					vLote = selecao100.getNrlote();
					vLote = FuncoesEspecificas.VerificaLote(context,
							parametros, vLote);

					if (vLote == null)
					{
						FuncoesCompartilhadas.exibirToastMsgLong(context,
								"Erro ao formatar lote");
						return false;
					}

					// cabecalho
					vLin = vLote
							+ ' '
							+ selecao100.getSexo()
							+ ' '
							+ FuncoesCompartilhadas.formataData("dd/MM/yyyy",
									vdata);

					bw.write(vLin);
					bw.write(Constantes.TXT_NOVA_LINHA);

					/* separador */
					bw.write(FuncoesCompartilhadas.dupeString("-", 78));
					bw.write(Constantes.TXT_NOVA_LINHA);

					for (int i = 0; i < dados.size(); i++)
					{
						PreAmostragemSelecao100Resultado resultado = dados
								.get(i);

						/* faixa inicial */
						if (resultado.getFaixa_inicial() > 0)
						{
							vFaixaIni = FuncoesCompartilhadas
									.formataPeso((double) resultado
											.getFaixa_inicial() / 1000);

							vFaixaIni = vFaixaIni.replace(",", ".");

							while (vFaixaIni.length() < 8)
							{
								vFaixaIni = ' ' + vFaixaIni;
							}
						} else
						{
							vFaixaIni = FuncoesCompartilhadas
									.dupeString(" ", 8);
						}

						/* precisa do c�digo da classificacao */
						if (resultado.getClassificacao_codigo() == (short) -999)
						{
							vFaixaIni = vFaixaIni.substring(0,
									vFaixaIni.length() - 3);
						}

						/* faixa final */
						if (resultado.getFaixa_final() > 0)
						{
							vFaixaFim = FuncoesCompartilhadas
									.formataPeso((double) resultado
											.getFaixa_final() / 1000);

							vFaixaFim = vFaixaFim.replace(",", ".");

							while (vFaixaFim.length() < 8)
							{
								vFaixaFim = ' ' + vFaixaFim;
							}
						} else
						{
							vFaixaFim = FuncoesCompartilhadas
									.dupeString(" ", 8);
						}

						/* qtd de pesos */
						vQtdPesos = String.valueOf(resultado.getQtd());
						while (vQtdPesos.length() < 8)
						{
							vQtdPesos = ' ' + vQtdPesos;
						}

						/* soma dos pesos */
						vSomaPesos = String.valueOf(resultado.getAcumulado());
						while (vSomaPesos.length() < 8)
						{
							vSomaPesos = ' ' + vSomaPesos;
						}

						/* media dos pesos */
						vMediaPesos = String.valueOf(resultado.getMedia());
						while (vMediaPesos.length() < 8)
						{
							vMediaPesos = ' ' + vMediaPesos;
						}

						vLin = resultado.getClassificacao_sigla() + ' '
								+ vFaixaIni + ' ' + vFaixaFim + ' ' + vQtdPesos
								+ ' ' + vSomaPesos + ' ' + vMediaPesos;

						bw.write(vLin);
						bw.write(Constantes.TXT_NOVA_LINHA);
					}

					/* criando a copia do arquivo */
					File fOriginal = new File(Constantes.DIRETORIO_SELECAO100
							+ snomearquivo.replace(".txt", ".ori"));
					if (FuncoesCompartilhadas.copiarArquivo(f, fOriginal) == false)
					{
						FuncoesCompartilhadas.exibirToastMsgLong(
								context,
								"Erro ao criar o arquivo "
										+ fOriginal.getName());
						return false;
					}

					return true;

				} catch (IOException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				} finally
				{
					if (bw != null)
					{
						try
						{
							bw.flush();
							bw.close();
						} catch (IOException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
		}
		return false;
	}
}
