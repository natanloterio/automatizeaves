package net.automatize.aves.dao;

import java.util.ArrayList;

import net.automatize.aves.model.Classificacao;
import net.automatize.aves.model.Selecao100Classificacao;
import net.automatize.aves.model.Selecao100Peso;
import net.automatize.aves.model.Selecao100PesoEstatistica;
import net.automatize.aves.util.Constantes;
import net.automatize.aves.util.FuncoesCompartilhadas;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

public class DAOSelecao100Pesos
{
	private Context context;

	public DAOSelecao100Pesos(Context context)
	{
		this.context = context;
	}

	public static final String TABLE_NAME = "SELECAO100PESOS";

	public static final String TABLE_CREATE = "create table if not exists "
			+ TABLE_NAME + " (" + Selecao100Peso.CODIGO
			+ " integer primary key autoincrement, " + Selecao100Peso.PESO
			+ " integer not null, " + Selecao100Peso.CLASSIFICACAO_CODIGO
			+ " integer references " + DAOClassificacao.TABLE_NAME + " ("
			+ Classificacao.CODIGO + "));";

	public Selecao100PesoEstatistica getSelecao100PesosEstatistica()
	{
		Selecao100PesoEstatistica result = new Selecao100PesoEstatistica();
		String sql = "select count(*) as QTD_PESOS ," + "sum("
				+ Selecao100Peso.PESO + ") AS SUM_PESOS, " + "avg("
				+ Selecao100Peso.PESO + ") AS AVG_PESOS, " + "min("
				+ Selecao100Peso.PESO + ") AS MIN_PESOS, " + "max("
				+ Selecao100Peso.PESO + ") AS MAX_PESOS " + "from "
				+ TABLE_NAME;

		Cursor c = ConexaoManager.getInstance(context).rawQuery(sql, null);

		if (c.moveToNext())
		{
			result.setQtd(c.getInt(0));
			result.setSum(c.getInt(1));
			result.setAvg(c.getInt(2));
			result.setMin(c.getInt(3));
			result.setMax(c.getInt(4));
		}
		return result;
	}

	public ArrayList<Selecao100Peso> getSelecao100Pesos()
	{
		ArrayList<Selecao100Peso> result = new ArrayList<Selecao100Peso>();
		String sql = "select P." + Selecao100Peso.CODIGO + ", P."
				+ Selecao100Peso.PESO + ", C." + Classificacao.SIGLA + " from "
				+ TABLE_NAME + " P " + "left join "
				+ DAOClassificacao.TABLE_NAME + " C on C."
				+ Classificacao.CODIGO + "= P."
				+ Selecao100Peso.CLASSIFICACAO_CODIGO + " order by P."
				+ Selecao100Peso.CODIGO + " desc";

		Cursor c = ConexaoManager.getInstance(context).rawQuery(sql, null);

		while (c.moveToNext())
		{
			Selecao100Peso pa = new Selecao100Peso();
			pa.setCodigo(c.getShort(0));
			pa.setPeso(c.getInt(1));
			pa.setClassificacao_sigla(c.getString(2));
			result.add(pa);
		}
		return result;
	}

	public int inserir(Selecao100Peso selecao100Peso)
	{
		ContentValues values = new ContentValues();
		values.put(Selecao100Peso.PESO, selecao100Peso.getPeso());
		values.put(Selecao100Peso.CLASSIFICACAO_CODIGO,
				selecao100Peso.getClassificacao_codigo());

		SQLiteDatabase database = ConexaoManager.getInstance(context);
		int insertId = (int) database.insert(TABLE_NAME, null, values);

		return insertId;
	}

	public String lancaPeso(int p_peso)
	{
		if (p_peso < 1)
		{
			return "Peso inv�lido!";
		}

		SQLiteDatabase conexao = ConexaoManager.getInstance(context);
		Cursor c = conexao.rawQuery("select "
				+ Selecao100Classificacao.CLASSIFICACAO_CODIGO + " from "
				+ DAOSelecao100Classificacao.TABLE_NAME + " where ? between "
				+ Selecao100Classificacao.FAIXAINICIAL + " and "
				+ Selecao100Classificacao.FAIXAFINAL,
				new String[] { String.valueOf(p_peso) });

		short r_cod_classificacao = -2;
		if (c.moveToNext())
		{
			r_cod_classificacao = c.getShort(0);
		}

		int v_min_faixa = 0;
		int v_max_faixa = 0;

		if (r_cod_classificacao == -2)
		{
			c = conexao.rawQuery("select min("
					+ Selecao100Classificacao.FAIXAINICIAL + "), max("
					+ Selecao100Classificacao.FAIXAFINAL + ") " + "from "
					+ DAOSelecao100Classificacao.TABLE_NAME, null);

			c.moveToNext();

			v_min_faixa = c.getInt(0);
			v_max_faixa = c.getInt(1);

			if (p_peso < v_min_faixa)
				r_cod_classificacao = -1; // FB
			else if (p_peso > v_max_faixa)
				r_cod_classificacao = 0; // FA
			else
				return "Imposs�vel determinar a faixa do peso!";
		}

		Selecao100Peso selecao100Peso = new Selecao100Peso();
		selecao100Peso.setClassificacao_codigo(r_cod_classificacao);
		selecao100Peso.setPeso(p_peso);

		if (inserir(selecao100Peso) == 0)
		{
			return Constantes.MSG_ERRO_GRAVACAO;
		}

		return null;
	}

	public boolean excluir(int iselecao100_codigo)
	{
		SQLiteDatabase database = ConexaoManager.getInstance(context);
		int result = database.delete(TABLE_NAME, Selecao100Peso.CODIGO + "=?",
				new String[] { String.valueOf(iselecao100_codigo) });

		if (result > 0)
			return true;
		else
			return false;
	}

	public int count()
	{
		Cursor c = ConexaoManager.getInstance(context).rawQuery(
				"select count(*) as QTD from " + TABLE_NAME, null);
		c.moveToNext();

		return c.getInt(0);
	}

	public boolean limparPesos()
	{
		try
		{
			SQLiteDatabase conexao = ConexaoManager.getInstance(context);
			conexao.delete(TABLE_NAME, null, null);
			FuncoesCompartilhadas.log("Limpei!");

			return true;
		} catch (SQLiteException ex)
		{
			return false;
		}
	}

	public String limparUltimoPeso()
	{
		String sql = "select coalesce(max(" + Selecao100Peso.CODIGO + "), 0) "
				+ "from " + TABLE_NAME;
		try
		{
			SQLiteDatabase conexao = ConexaoManager.getInstance(context);
			Cursor c = conexao.rawQuery(sql, null);
			c.moveToNext();

			if (c.getInt(0) == 0)
				return "N�o h� peso a excluir!";

			conexao.delete(TABLE_NAME, Selecao100Peso.CODIGO + "= (" + sql
					+ ")", null);
		} catch (SQLiteException ex)
		{
			return "Erro ao excluir �ltimo peso!";
		}
		return null;
	}

	public int contaPesosNaFaixa(int classificacao_codigo)
	{
		Cursor c = ConexaoManager.getInstance(context).rawQuery(
				"select count(*) from " + TABLE_NAME + " where "
						+ Selecao100Peso.CLASSIFICACAO_CODIGO + "=?",
				new String[] { String.valueOf(classificacao_codigo) });

		c.moveToNext();
		return c.getInt(0);
	}
}
