package net.automatize.aves.dao;

import java.util.ArrayList;

import net.automatize.aves.model.PreAmostragemPeso;
import net.automatize.aves.model.PreAmostragemPesoEstatistica;
import net.automatize.aves.util.FuncoesCompartilhadas;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

public class DAOPreAmostragemPesos
{
	private Context context;

	public DAOPreAmostragemPesos(Context context)
	{
		this.context = context;
	}

	public static final String TABLE_NAME = "PREAMOSTRAGEMPESOS";

	public static final String TABLE_CREATE = "create table if not exists "
			+ TABLE_NAME + " (" + PreAmostragemPeso.CODIGO
			+ " integer primary key autoincrement, " + PreAmostragemPeso.PESO
			+ " integer not null);";

	public ArrayList<PreAmostragemPeso> getPreAmostragemPesos()
	{
		ArrayList<PreAmostragemPeso> result = new ArrayList<PreAmostragemPeso>();
		String sql = "select " + PreAmostragemPeso.CODIGO + ","
				+ PreAmostragemPeso.PESO + " from "
				+ TABLE_NAME + " order by "
				+ PreAmostragemPeso.CODIGO + " desc";

		Cursor c = ConexaoManager.getInstance(context).rawQuery(sql, null);

		while (c.moveToNext())
		{
			PreAmostragemPeso pa = new PreAmostragemPeso();
			pa.setCodigo(c.getShort(0));
			pa.setPeso(c.getInt(1));
			result.add(pa);
		}
		return result;
	}

	public PreAmostragemPesoEstatistica getPreAmostragemPesosEstatistica()
	{
		PreAmostragemPesoEstatistica result = new PreAmostragemPesoEstatistica();
		String sql = "select count(*) as QTD_PESOS ," + "sum("
				+ PreAmostragemPeso.PESO + ") AS SUM_PESOS, " + "avg("
				+ PreAmostragemPeso.PESO + ") AS AVG_PESOS, " + "min("
				+ PreAmostragemPeso.PESO + ") AS MIN_PESOS, " + "max("
				+ PreAmostragemPeso.PESO + ") AS MAX_PESOS " + "from "
				+ DAOPreAmostragemPesos.TABLE_NAME;

		Cursor c = ConexaoManager.getInstance(context).rawQuery(sql, null);

		if (c.moveToNext())
		{
			result.setQtd(c.getInt(0));
			result.setSum(c.getInt(1));
			result.setAvg(c.getInt(2));
			result.setMin(c.getInt(3));
			result.setMax(c.getInt(4));
		}
		return result;
	}

	public int inserir(PreAmostragemPeso preAmostragemPeso)
	{
		ContentValues values = new ContentValues();
		values.put(PreAmostragemPeso.PESO, preAmostragemPeso.getPeso());

		SQLiteDatabase database = ConexaoManager.getInstance(context);
		int insertId = (int) database.insert(TABLE_NAME, null, values);

		return insertId;
	}

	public boolean excluir(int ipreamostragempeso_codigo)
	{
		SQLiteDatabase database = ConexaoManager.getInstance(context);
		int result = database.delete(TABLE_NAME, PreAmostragemPeso.CODIGO
				+ "=?",
				new String[] { String.valueOf(ipreamostragempeso_codigo) });

		if (result > 0)
			return true;
		else
			return false;
	}

	public int count()
	{
		Cursor c = ConexaoManager.getInstance(context).rawQuery(
				"select count(*) as QTD from "
						+ DAOPreAmostragemPesos.TABLE_NAME, null);
		c.moveToNext();

		return c.getInt(0);
	}

	public boolean limparPesos()
	{
		try
		{
			SQLiteDatabase conexao = ConexaoManager.getInstance(context);
			conexao.delete(TABLE_NAME, null, null);
			FuncoesCompartilhadas.log("Limpei!");

			return true;
		} catch (SQLiteException ex)
		{
			return false;
		}
	}

	public String limparUltimoPeso()
	{
		String sql = "select coalesce(max(" + PreAmostragemPeso.CODIGO
				+ "), 0) " + "from " + TABLE_NAME;
		try
		{
			SQLiteDatabase conexao = ConexaoManager.getInstance(context);

			Cursor c = conexao.rawQuery(sql, null);
			c.moveToNext();
			
			if (c.getInt(0) == 0)
				return "N�o h� peso a excluir!";

			conexao.delete(TABLE_NAME, PreAmostragemPeso.CODIGO + "= (" + sql
					+ ")", null);
		} catch (SQLiteException ex)
		{
			return "Erro ao excluir �ltimo peso!";
		}
		return null;
	}
}
