package net.automatize.aves.dao;

import java.util.ArrayList;
import java.util.List;

import net.automatize.aves.model.Classificacao;
import net.automatize.aves.model.Selecao100Classificacao;
import net.automatize.aves.model.Selecao100ClassificacaoItem;
import net.automatize.aves.util.Constantes;
import net.automatize.aves.util.FuncoesCompartilhadas;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

public class DAOSelecao100Classificacao
{
	private Context context;

	public DAOSelecao100Classificacao(Context context)
	{
		this.context = context;
	}

	public static final String TABLE_NAME = "SELECAO100CLASS";

	public static final String TABLE_CREATE = "create table if not exists "
			+ TABLE_NAME + " (" + Selecao100Classificacao.CLASSIFICACAO_CODIGO
			+ " integer primary key references " + DAOClassificacao.TABLE_NAME
			+ " (" + Classificacao.CODIGO + "), "
			+ Selecao100Classificacao.FAIXAINICIAL + " integer not null, "
			+ Selecao100Classificacao.FAIXAFINAL + " integer not null);";

	public ArrayList<Selecao100ClassificacaoItem> carregaClassificacoesGrid()
	{
		ArrayList<Selecao100ClassificacaoItem> result = new ArrayList<Selecao100ClassificacaoItem>();
		String sql = "select C." + Classificacao.CODIGO + ", " + "C."
				+ Classificacao.SIGLA + ", " + "P."
				+ Selecao100Classificacao.FAIXAINICIAL + ", P."
				+ Selecao100Classificacao.FAIXAFINAL + ", " + "case when P."
				+ Selecao100Classificacao.CLASSIFICACAO_CODIGO
				+ " is null then 0 else 1 end " + "from "
				+ DAOClassificacao.TABLE_NAME + " C " + "left join "
				+ DAOSelecao100Classificacao.TABLE_NAME + " P on P."
				+ Selecao100Classificacao.CLASSIFICACAO_CODIGO + " = C."
				+ Classificacao.CODIGO + " where C." + Classificacao.CODIGO
				+ " > 0 " + "order by C." + Classificacao.CODIGO;

		Cursor c = ConexaoManager.getInstance(context).rawQuery(sql, null);
		while (c.moveToNext())
		{
			Selecao100ClassificacaoItem p = new Selecao100ClassificacaoItem();
			p.setClassificacao_codigo(c.getShort(0));
			p.setClassificacao_sigla(c.getString(1));
			p.setFaixainicial(c.getInt(2));
			p.setFaixafinal(c.getInt(3));
			p.setSelecionado(c.getInt(4) == 1); // 1 == true
			result.add(p);
		}
		return result;
	}

	public String gravarClassificacao(Selecao100ClassificacaoItem data)
	{
		List<Selecao100ClassificacaoItem> value = new ArrayList<Selecao100ClassificacaoItem>();
		value.add(data);
		return gravarClassificacao(value);
	}

	public String gravarClassificacao(List<Selecao100ClassificacaoItem> data)
	{
		SQLiteDatabase conexao = ConexaoManager.getInstance(context);
		conexao.beginTransaction();

		/*
		 * RESUMINDO - se a classifica��o n�o tiver marcada, tenta apagar, se
		 * tiver e existir no banco, update, se n�o existir insert
		 */

		try
		{
			for (Selecao100ClassificacaoItem item : data)
			{
				if (item.getSelecionado())
				{
					ContentValues values = new ContentValues();
					values.put(Selecao100Classificacao.FAIXAINICIAL,
							item.getFaixainicial());
					values.put(Selecao100Classificacao.FAIXAFINAL,
							item.getFaixafinal());

					// procurar
					if (procuraClassificacao(item.getClassificacao_codigo()))
					{
						// update
						long result = (long) conexao.update(TABLE_NAME, values,
								Selecao100Classificacao.CLASSIFICACAO_CODIGO
										+ "=?",
								new String[] { String.valueOf(item
										.getClassificacao_codigo()) });
						if (result == 0)
						{
							return "Erro";
						}

					} else
					{
						// insert
						values.put(
								Selecao100Classificacao.CLASSIFICACAO_CODIGO,
								item.getClassificacao_codigo());

						long result = (long) conexao.insert(TABLE_NAME, null,
								values);
						if (result == 0)
						{
							return "Erro";
						}
					}
				} else
				{
					conexao.delete(
							TABLE_NAME,
							Selecao100Classificacao.CLASSIFICACAO_CODIGO + "=?",
							new String[] { String.valueOf(item
									.getClassificacao_codigo()) });
				}
			}

			conexao.setTransactionSuccessful();

		} catch (SQLiteException ex)
		{
			return Constantes.MSG_ERRO_GRAVACAO + " - " + ex.getMessage();
		} finally
		{
			conexao.endTransaction();
		}

		return null;
	}

	private boolean procuraClassificacao(int iclassificacao_codigo)
	{
		String sql = "select " + Selecao100Classificacao.CLASSIFICACAO_CODIGO
				+ " from " + TABLE_NAME + " where "
				+ Selecao100Classificacao.CLASSIFICACAO_CODIGO + "=?";

		Cursor c = ConexaoManager.getInstance(context).rawQuery(sql,
				new String[] { String.valueOf(iclassificacao_codigo) });

		if (c.moveToNext())
		{
			return true;
		} else
		{
			return false;
		}
	}

	public int count()
	{
		Cursor c = ConexaoManager.getInstance(context).rawQuery(
				"select count(*) from " + TABLE_NAME, null);
		c.moveToNext();

		return c.getInt(0);
	}

	public String validaFaixa()
	{
		/* PROC_VALIDA_FAIXAS_SELECAO_100 */
		int v_qtd = count();

		if (v_qtd < 2)
			return "No m�nimo 2 faixas s�o necess�rias para sele��o!";

		Cursor c = ConexaoManager.getInstance(context).rawQuery(
				"select " + Selecao100Classificacao.FAIXAINICIAL + ", "
						+ Selecao100Classificacao.FAIXAFINAL + " from "
						+ TABLE_NAME + " order by "
						+ Selecao100Classificacao.CLASSIFICACAO_CODIGO
						+ " desc " + "limit 1", null);

		c.moveToNext();

		// short v_cod_classificacao = c.getShort(0);
		int v_faixa_inicial_ant = c.getInt(0);
		int v_faixa_final_ant = c.getInt(1);

		Cursor c2 = ConexaoManager.getInstance(context)
				.rawQuery(
						"select " + Selecao100Classificacao.FAIXAINICIAL + ", "
								+ Selecao100Classificacao.FAIXAFINAL + " from "
								+ TABLE_NAME + " order by "
								+ Selecao100Classificacao.CLASSIFICACAO_CODIGO
								+ " desc " + "limit " + (v_qtd - 1)
								+ " offset 1", null);

		int v_faixa_inicial, v_faixa_final;
		while (c2.moveToNext())
		{
			v_faixa_inicial = c2.getInt(0);
			v_faixa_final = c2.getInt(1);

			if (v_faixa_inicial <= v_faixa_final_ant)
			{
				return "Intervalo de faixa " + v_faixa_inicial
						+ " fora de sequ�ncia!";
			} else if (v_faixa_inicial != v_faixa_final_ant + 1)
			{
				return "Faixa " + v_faixa_inicial + " deve ser "
						+ (v_faixa_final_ant + 1) + "!";
			}

			v_faixa_inicial_ant = v_faixa_inicial;
			v_faixa_final_ant = v_faixa_final;
		}
		return null;
	}

	public ArrayList<Selecao100Classificacao> getClassificacoes()
	{
		ArrayList<Selecao100Classificacao> result = new ArrayList<Selecao100Classificacao>();
		Cursor c = ConexaoManager.getInstance(context).rawQuery(
				"select " + Selecao100Classificacao.CLASSIFICACAO_CODIGO + ", "
						+ Selecao100Classificacao.FAIXAINICIAL + ", "
						+ Selecao100Classificacao.FAIXAFINAL + " from "
						+ TABLE_NAME + " where "
						+ Selecao100Classificacao.CLASSIFICACAO_CODIGO
						+ " > 0 " + "order by "
						+ Selecao100Classificacao.CLASSIFICACAO_CODIGO
						+ " desc", null);

		while (c.moveToNext())
		{
			Selecao100Classificacao item = new Selecao100Classificacao();
			item.setClassificacaoCodigo(c.getShort(0));
			item.setFaixaInicial(c.getInt(1));
			item.setFaixaFinal(c.getInt(2));
			result.add(item);
		}
		return result;
	}

	public String reconheceClassificacao(double peso)
	{
		int ipeso = FuncoesCompartilhadas.kgToGr(peso);
		Cursor c = ConexaoManager.getInstance(context).rawQuery(
				"select C." + Classificacao.SIGLA + " from " + TABLE_NAME
						+ " T " + "left join " + DAOClassificacao.TABLE_NAME
						+ " C on C." + Classificacao.CODIGO + " = T."
						+ Selecao100Classificacao.CLASSIFICACAO_CODIGO
						+ " where ? between T."
						+ Selecao100Classificacao.FAIXAINICIAL + " and T."
						+ Selecao100Classificacao.FAIXAFINAL,
				new String[] { String.valueOf(ipeso) });
		if (c.moveToNext())
		{
			return c.getString(0);
		} else
		{
			Cursor cMenor = ConexaoManager.getInstance(context).rawQuery(
					"select min(coalesce("
							+ Selecao100Classificacao.FAIXAINICIAL + ", 0)) "
							+ "from " + TABLE_NAME, null);
			
			cMenor.moveToNext();
			if (ipeso < cMenor.getInt(0))
				return "FB";

			Cursor cMaior = ConexaoManager.getInstance(context).rawQuery(
					"select max(coalesce(" + Selecao100Classificacao.FAIXAFINAL
							+ ", 0)) " + "from " + TABLE_NAME, null);

			cMaior.moveToNext();
			if (ipeso > cMaior.getInt(0))
				return "FA";
		}

		return "";
	}
}
