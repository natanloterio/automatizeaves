package net.automatize.aves.dao;

import java.util.ArrayList;
import java.util.List;

import net.automatize.aves.model.AmostragemBox;
import net.automatize.aves.model.AmostragemLote;
import net.automatize.aves.util.FuncoesCompartilhadas;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

public class DAOAmostragemLotes
{
	private Context context;

	public DAOAmostragemLotes(Context context)
	{
		this.context = context;
	}

	public static final String TABLE_NAME = "AMOSTRAGEMLOTES";

	public static final String TABLE_CREATE = "create table if not exists "
			+ TABLE_NAME + " (" + AmostragemLote.CODIGO
			+ " integer primary key autoincrement, " + AmostragemLote.NR_LOTE
			+ " varchar(10)," + AmostragemLote.SEXO + " varchar(1), "
			+ AmostragemLote.QTD_BOXES + " smallint, "
			+ AmostragemLote.QTD_AVES_POR_BOX + " integer);" + "alter table "
			+ TABLE_NAME + " add constraint uk UNIQUE ("
			+ AmostragemLote.NR_LOTE + ");";

	public int inserir(AmostragemLote a)
	{
		ContentValues values = new ContentValues();
		values.put(AmostragemLote.NR_LOTE, a.getNumeroLote());
		values.put(AmostragemLote.SEXO, a.getSexo());
		values.put(AmostragemLote.QTD_BOXES, a.getQtdBoxes());
		values.put(AmostragemLote.QTD_AVES_POR_BOX, a.getQtdAvesPorBox());

		SQLiteDatabase database = ConexaoManager.getInstance(context);
		int insertId = (int) database.insert(TABLE_NAME, null, values);

		return insertId;
	}

	public int alterar(AmostragemLote a)
	{
		ContentValues values = new ContentValues();
		values.put(AmostragemLote.NR_LOTE, a.getNumeroLote());
		values.put(AmostragemLote.SEXO, a.getSexo());
		values.put(AmostragemLote.QTD_BOXES, a.getQtdBoxes());
		values.put(AmostragemLote.QTD_AVES_POR_BOX, a.getQtdAvesPorBox());

		SQLiteDatabase database = ConexaoManager.getInstance(context);
		int insertId = (int) database.update(TABLE_NAME, values,
				AmostragemLote.CODIGO + "=?",
				new String[] { String.valueOf(a.getCodigo()) });

		return insertId;
	}

	public int excluir(int amostragemlote_codigo)
	{
		return excluir(amostragemlote_codigo, null);
	}

	public int excluir(int amostragemlote_codigo, SQLiteDatabase conexao)
	{
		int insertId = -1;
		try
		{
			if (conexao == null)
				conexao = ConexaoManager.getInstance(context);

			insertId = (int) conexao.delete(TABLE_NAME, AmostragemLote.CODIGO
					+ "=?",
					new String[] { String.valueOf(amostragemlote_codigo) });
		} catch (SQLiteException ex)
		{
			FuncoesCompartilhadas.log(ex.getMessage());
			return -1;
		}
		return insertId;
	}

	public List<AmostragemLote> getAmostragemLotes()
	{
		List<AmostragemLote> result = new ArrayList<AmostragemLote>();

		Cursor cursor = ConexaoManager.getInstance(context).rawQuery(
				"select " + AmostragemLote.CODIGO + ", "
						+ AmostragemLote.NR_LOTE + ", " + AmostragemLote.SEXO
						+ ", " + AmostragemLote.QTD_BOXES + ","
						+ AmostragemLote.QTD_AVES_POR_BOX + " from "
						+ TABLE_NAME, null);

		if (cursor != null)
		{

			while (cursor.moveToNext())
			{
				result.add(new AmostragemLote(cursor.getInt(0), cursor
						.getString(1), cursor.getString(2), cursor.getShort(3),
						cursor.getInt(4)));
			}
		}

		return result;
	}

	public AmostragemLote getAmostragemLote(int icodigo)
	{
		Cursor cursor = ConexaoManager.getInstance(context).rawQuery(
				"select " + AmostragemLote.CODIGO + ", "
						+ AmostragemLote.NR_LOTE + ", " + AmostragemLote.SEXO
						+ ", " + AmostragemLote.QTD_BOXES + ","
						+ AmostragemLote.QTD_AVES_POR_BOX + " from "
						+ TABLE_NAME + " where " + AmostragemLote.CODIGO
						+ " = ?", new String[] { String.valueOf(icodigo) });

		if (cursor != null)
		{
			if (cursor.moveToNext())
				return new AmostragemLote(cursor.getInt(0),
						cursor.getString(1), cursor.getString(2),
						cursor.getShort(3), cursor.getInt(4));
		}

		return null;
	}

	public boolean loteJaCadastrado(String lote, int amostragemAtual)
	{
		Cursor cursor = ConexaoManager.getInstance(context)
				.rawQuery(
						"select " + AmostragemLote.CODIGO + ", "
								+ AmostragemLote.NR_LOTE + ", "
								+ AmostragemLote.SEXO + ", "
								+ AmostragemLote.QTD_BOXES + ","
								+ AmostragemLote.QTD_AVES_POR_BOX + " from "
								+ TABLE_NAME + " where upper("
								+ AmostragemLote.NR_LOTE + ") = upper(?) and "
								+ AmostragemLote.CODIGO + " <> ?",
						new String[] { lote, String.valueOf(amostragemAtual) });

		if (cursor != null)
		{
			if (cursor.moveToNext())
				return true;
		}

		return false;
	}

	public boolean limparPesosLote(AmostragemLote amostragemlote)
	{
		return limparPesosLote(amostragemlote, null, false);
	}

	public boolean limparPesosLote(AmostragemLote amostragemlote,
			SQLiteDatabase conexao, boolean bEmTransacao)
	{
		if (bEmTransacao == false)
		{
			conexao = ConexaoManager.getInstance(context);
			conexao.beginTransaction();
		}

		DAOAmostragemBox daoAmostragemBox = new DAOAmostragemBox(context);
		DAOAmostragemPesos daoAmostragemPeso = new DAOAmostragemPesos(context);

		List<AmostragemBox> listaBoxesDoLotedao = daoAmostragemBox
				.amostragemBoxPorLote(amostragemlote.getCodigo());

		for (int i = 0; i < listaBoxesDoLotedao.size(); i++)
		{
			/* limpa os pesos do box */
			if (daoAmostragemPeso.limparPesosBox(listaBoxesDoLotedao.get(i)
					.getCodigo(), conexao) == false)
			{
				if (bEmTransacao == false)
					conexao.endTransaction();
				return false;
			}

			/* exclu� o box */
			if (daoAmostragemBox.excluir(
					listaBoxesDoLotedao.get(i).getCodigo(), conexao) == 0)
			{
				if (bEmTransacao == false)
					conexao.endTransaction();
				return false;
			}
		}

		if (bEmTransacao == false)
		{
			conexao.setTransactionSuccessful();
			conexao.endTransaction();
		}

		return true;
	}
}
