package net.automatize.aves.dao;

import java.util.ArrayList;
import java.util.Date;

import net.automatize.aves.model.PreAmostragem;
import net.automatize.aves.model.PreAmostragemClassificacao;
import net.automatize.aves.model.Selecao100;
import net.automatize.aves.model.Selecao100Classificacao;
import net.automatize.aves.util.Constantes;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

public class DAOSelecao100
{
	private Context context;

	public DAOSelecao100(Context context)
	{
		this.context = context;
	}

	public static final String TABLE_NAME = "SELECAO100";

	public static final String TABLE_CREATE = "create table if not exists "
			+ TABLE_NAME + " (" + Selecao100.CODIGO
			+ " integer primary key autoincrement, " + Selecao100.NRLOTE
			+ " varchar(10) not null, " + Selecao100.SEXO
			+ " varchar(1) not null, " + Selecao100.DATA + " date not null);";

	public Selecao100 getSelecao100()
	{
		Cursor c = ConexaoManager.getInstance(context).rawQuery(
				"select " + Selecao100.CODIGO + ", " + Selecao100.NRLOTE + ", "
						+ Selecao100.SEXO + ", " + Selecao100.DATA + " from "
						+ TABLE_NAME + " where " + Selecao100.CODIGO + "=?",
				new String[] { String.valueOf(Constantes.SELECAO100_CODIGO) });

		if (c.moveToNext())
		{
			Selecao100 selecao100 = new Selecao100();
			selecao100.setCodigo(c.getShort(0));
			selecao100.setNrlote(c.getString(1));
			selecao100.setSexo(c.getString(2));
			selecao100.setData(new Date(c.getLong(3)));
			return selecao100;
		}
		return null;
	}

	public int gravar(Selecao100 selecao100)
	{
		return gravar(selecao100, null);
	}

	public int gravar(Selecao100 selecao100, SQLiteDatabase conexao)
	{
		ContentValues values = new ContentValues();
		values.put(Selecao100.NRLOTE, selecao100.getNrlote());
		values.put(Selecao100.DATA, selecao100.getData().getTime());
		values.put(Selecao100.SEXO, selecao100.getSexo());
		values.put(Selecao100.CODIGO, Constantes.SELECAO100_CODIGO);

		long result = 0;
		SQLiteDatabase database = null;

		if (conexao == null)
			database = ConexaoManager.getInstance(context);
		else
			database = conexao;

		if (selecao100.getCodigo() == 0)
		{
			// insert
			values.put(Selecao100.CODIGO, Constantes.SELECAO100_CODIGO);
			result = database.insert(TABLE_NAME, null, values);
		} else
		{
			// update
			result = database
					.update(TABLE_NAME, values, Selecao100.CODIGO + "=?",
							new String[] { String
									.valueOf(Constantes.SELECAO100_CODIGO) });
		}

		return (int) result;
	}

	public String copiarPreAmostragem(
			ArrayList<PreAmostragemClassificacao> lista_faixas,
			PreAmostragem preamostragem)
	{
		SQLiteDatabase conexao = null;
		try
		{
			conexao = ConexaoManager.getInstance(context);
			conexao.beginTransaction();

			conexao.delete(DAOSelecao100Classificacao.TABLE_NAME, null, null);
			conexao.delete(TABLE_NAME, null, null);

			ContentValues values = new ContentValues();
			for (PreAmostragemClassificacao item : lista_faixas)
			{
				if (values.size() > 0)
					values.clear();

				values.put(Selecao100Classificacao.CLASSIFICACAO_CODIGO,
						item.getClassificacaoCodigo());
				values.put(Selecao100Classificacao.FAIXAINICIAL,
						item.getFaixaInicial());
				values.put(Selecao100Classificacao.FAIXAFINAL,
						item.getFaixaFinal());

				if (conexao.insert(DAOSelecao100Classificacao.TABLE_NAME, null,
						values) <= 0)
				{
					return "Erro ao inserir as classifica��es";
				}
			}

			/* cabecalho */
			Selecao100 selecao100 = new Selecao100();
			selecao100.setData(preamostragem.getData());
			selecao100.setNrlote(preamostragem.getNumeroLote());
			selecao100.setSexo(preamostragem.getSexo());

			if (gravar(selecao100, conexao) > 0)
				conexao.setTransactionSuccessful();
			else
				return "N�o foi poss�vel executar a c�pia!";

		} catch (SQLiteException e)
		{
			return String.format("N�o foi poss�vel executar a c�pia!|%s",
					e.getMessage());
		} finally
		{
			conexao.endTransaction();
		}
		return null;
	}
}
