package net.automatize.aves.dao;

import java.util.ArrayList;
import java.util.List;

import net.automatize.aves.model.Classificacao;
import net.automatize.aves.model.PreAmostragemClassificacao;
import net.automatize.aves.model.PreAmostragemClassificacaoItem;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

public class DAOPreAmostragemClassificacao
{
	private Context context;

	public DAOPreAmostragemClassificacao(Context context)
	{
		this.context = context;
	}

	public static final String TABLE_NAME = "PREAMOSTRAGEMCLASS";

	public static final String TABLE_CREATE = "create table if not exists "
			+ TABLE_NAME + " ("
			+ PreAmostragemClassificacao.CLASSIFICACAO_CODIGO
			+ " integer primary key references " + DAOClassificacao.TABLE_NAME
			+ " (" + Classificacao.CODIGO + "), "
			+ PreAmostragemClassificacao.PERCENTUAL + " smallint not null, "
			+ PreAmostragemClassificacao.QTDPESOS + " integer not null, "
			+ PreAmostragemClassificacao.FAIXAINICIAL + " integer not null, "
			+ PreAmostragemClassificacao.FAIXAFINAL + " integer not null);";

	public ArrayList<PreAmostragemClassificacaoItem> carregaClassificacoesConfiguradas()
	{
		ArrayList<PreAmostragemClassificacaoItem> result = new ArrayList<PreAmostragemClassificacaoItem>();
		String sql = "select " + PreAmostragemClassificacao.PERCENTUAL + ", "
				+ PreAmostragemClassificacao.CLASSIFICACAO_CODIGO + " from "
				+ TABLE_NAME + " order by "
				+ PreAmostragemClassificacao.CLASSIFICACAO_CODIGO + " desc";

		Cursor c = ConexaoManager.getInstance(context).rawQuery(sql, null);
		while (c.moveToNext())
		{
			PreAmostragemClassificacaoItem p = new PreAmostragemClassificacaoItem();
			p.setPercentual(c.getInt(0));
			p.setClassificacao_codigo(c.getInt(1));
			result.add(p);
		}

		return result;
	}

	public ArrayList<PreAmostragemClassificacaoItem> carregaClassificacoesGrid()
	{
		ArrayList<PreAmostragemClassificacaoItem> result = new ArrayList<PreAmostragemClassificacaoItem>();
		String sql = "select C." + Classificacao.CODIGO + ", C."
				+ Classificacao.SIGLA + ", P."
				+ PreAmostragemClassificacao.PERCENTUAL + ", " + "case when P."
				+ PreAmostragemClassificacao.CLASSIFICACAO_CODIGO + " is null "
				+ "then 0 else 1 end as SELECIONADO " + "from "
				+ DAOClassificacao.TABLE_NAME + " C " + "left join "
				+ DAOPreAmostragemClassificacao.TABLE_NAME + " P on P."
				+ PreAmostragemClassificacao.CLASSIFICACAO_CODIGO + " = C."
				+ Classificacao.CODIGO + " where C." + Classificacao.CODIGO
				+ " > 0" + " order by C." + Classificacao.CODIGO + " desc";

		Cursor c = ConexaoManager.getInstance(context).rawQuery(sql, null);
		while (c.moveToNext())
		{
			PreAmostragemClassificacaoItem p = new PreAmostragemClassificacaoItem();
			p.setClassificacao_codigo(c.getInt(0));
			p.setClassificacao_sigla(c.getString(1));
			p.setPercentual(c.getInt(2));
			p.setSelecionado(c.getInt(3) == 1); // 1 == true
			result.add(p);
		}

		return result;
	}

	private boolean procuraClassificacao(int iclassificacao_codigo)
	{
		String sql = "select " + PreAmostragemClassificacao.PERCENTUAL
				+ " from " + TABLE_NAME + " where "
				+ PreAmostragemClassificacao.CLASSIFICACAO_CODIGO + "=?";

		Cursor c = ConexaoManager.getInstance(context).rawQuery(sql,
				new String[] { String.valueOf(iclassificacao_codigo) });

		if (c.moveToNext())
		{
			return true;
		} else
		{
			return false;
		}
	}

	public boolean gravarClassificacao(List<PreAmostragemClassificacaoItem> data)
	{
		SQLiteDatabase conexao = ConexaoManager.getInstance(context);
		conexao.beginTransaction();

		try
		{
			for (PreAmostragemClassificacaoItem item : data)
			{
				if (item.getSelecionado())
				{
					ContentValues values = new ContentValues();
					values.put(PreAmostragemClassificacao.PERCENTUAL,
							item.getPercentual());
					values.put(PreAmostragemClassificacao.FAIXAINICIAL, 0);
					values.put(PreAmostragemClassificacao.FAIXAFINAL, 0);
					values.put(PreAmostragemClassificacao.QTDPESOS, 0);

					// procurar
					if (procuraClassificacao(item.getClassificacao_codigo()))
					{
						// update
						long result = (long) conexao.update(TABLE_NAME, values,
								PreAmostragemClassificacao.CLASSIFICACAO_CODIGO
										+ "=?",
								new String[] { String.valueOf(item
										.getClassificacao_codigo()) });
						if (result == 0)
						{
							return false;
						}

					} else
					{
						// insert
						values.put(
								PreAmostragemClassificacao.CLASSIFICACAO_CODIGO,
								item.getClassificacao_codigo());
						long result = (long) conexao.insert(TABLE_NAME, null,
								values);
						if (result == 0)
						{
							return false;
						}
					}
				} else
				{
					// long result = (long)
					conexao.delete(TABLE_NAME,
							PreAmostragemClassificacao.CLASSIFICACAO_CODIGO
									+ "=?", new String[] { String.valueOf(item
									.getClassificacao_codigo()) });
					// if (result == 0)
					// {
					// return false;
					// }
				}
			}

			conexao.setTransactionSuccessful();

		} catch (SQLiteException ex)
		{
			return false;
		}

		conexao.endTransaction();
		return true;
	}

	public int count()
	{
		Cursor c = ConexaoManager.getInstance(context).rawQuery(
				"select count(*) from " + TABLE_NAME, null);
		c.moveToNext();

		return c.getInt(0);
	}

	public boolean percentualClasssificacaoZero()
	{
		Cursor c = ConexaoManager.getInstance(context).rawQuery(
				"select " + PreAmostragemClassificacao.CLASSIFICACAO_CODIGO
						+ " from " + TABLE_NAME + " where "
						+ PreAmostragemClassificacao.PERCENTUAL + " < 1", null);
		return c.moveToNext();
	}

	public int percentualClasssificacaoSoma()
	{
		Cursor c = ConexaoManager.getInstance(context).rawQuery(
				"select coalesce(sum(" + PreAmostragemClassificacao.PERCENTUAL
						+ "), 0) " + "from " + TABLE_NAME, null);
		c.moveToNext();
		return c.getInt(0);
	}

	public boolean atualizaQtdPesosPercentual(SQLiteDatabase conexao,
			int classificacao_codigo, int v_pesos)
	{
		ContentValues values = new ContentValues();
		values.put(PreAmostragemClassificacao.QTDPESOS, v_pesos);
		values.put(PreAmostragemClassificacao.FAIXAINICIAL, 0);
		values.put(PreAmostragemClassificacao.FAIXAFINAL, 0);

		long result = conexao.update(TABLE_NAME, values,
				PreAmostragemClassificacao.CLASSIFICACAO_CODIGO + "=?",
				new String[] { String.valueOf(classificacao_codigo) });

		return (result > 0);
	}

	public ArrayList<PreAmostragemClassificacao> getClassificacaoParaCopiaSelecao100()
	{
		ArrayList<PreAmostragemClassificacao> result = new ArrayList<PreAmostragemClassificacao>();
		Cursor c = ConexaoManager.getInstance(context).rawQuery(
				"select " + PreAmostragemClassificacao.CLASSIFICACAO_CODIGO
						+ "," + PreAmostragemClassificacao.FAIXAINICIAL + ", "
						+ PreAmostragemClassificacao.FAIXAFINAL + " from "
						+ TABLE_NAME + " order by "
						+ PreAmostragemClassificacao.CLASSIFICACAO_CODIGO,

				null);
		while (c.moveToNext())
		{
			PreAmostragemClassificacao item = new PreAmostragemClassificacao();
			item.setClassificacaoCodigo(c.getShort(0));
			item.setFaixaInicial(c.getInt(1));
			item.setFaixaFinal(c.getInt(2));
			result.add(item);
		}

		return result;
	}
}
