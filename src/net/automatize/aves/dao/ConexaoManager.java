package net.automatize.aves.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class ConexaoManager {

	private static ConexaoSQLite conexao = null;
	private static SQLiteDatabase database = null;

	public static SQLiteDatabase getDatabase() {
		return database;
	}

	public static void setDatabase(SQLiteDatabase database) {
		ConexaoManager.database = database;
	}

	public static SQLiteDatabase getInstance(Context context) {

		if (conexao == null) {
			conexao = new ConexaoSQLite(context);
		}

		if (database == null) {
			setDatabase(conexao.getWritableDatabase()); 
		}

		return getDatabase();
	}

}
