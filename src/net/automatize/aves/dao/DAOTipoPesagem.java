package net.automatize.aves.dao;

import net.automatize.aves.model.TipoPesagem;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class DAOTipoPesagem
{
	private Context contexto;

	public DAOTipoPesagem(Context contexto)
	{
		this.contexto = contexto;
	}

	private static final String TABLE_NAME = "TIPOPESAGEM";

	public static final String TABLE_CREATE = "create table if not exists "
			+ TABLE_NAME + " (" + TipoPesagem.CODIGO
			+ " smallint primary key not null, " + TipoPesagem.TIPOPESAGEM
			+ " smallint," + TipoPesagem.PESOMINIMO + " integer, "
			+ TipoPesagem.MACADDRESS + " varchar(17));";

	public int gravar(TipoPesagem tipoPesagem)
	{
		ContentValues values = new ContentValues();
		values.put(TipoPesagem.CODIGO, 1);
		values.put(TipoPesagem.TIPOPESAGEM, tipoPesagem.getTipoPesagem());
		values.put(TipoPesagem.PESOMINIMO, tipoPesagem.getPesoMinimo());
		values.put(TipoPesagem.MACADDRESS, tipoPesagem.getMacAddress());

		SQLiteDatabase database = ConexaoManager.getInstance(contexto);

		TipoPesagem tipoPesagemAtual = getTipoPesagem();

		int idTipoPesagem = 0;
		if (tipoPesagemAtual == null)
		{
			// insert
			idTipoPesagem = (int) database.insert(TABLE_NAME, null, values);
			Log.i("insert", String.valueOf(idTipoPesagem));
		} else
		{
			// update
			idTipoPesagem = database.update(TABLE_NAME, values, null, null);

			Log.i("update", String.valueOf(idTipoPesagem));
		}

		return idTipoPesagem;
	}

	public TipoPesagem getTipoPesagem()
	{
		Cursor cursor = ConexaoManager.getInstance(contexto).rawQuery(
				"select * from " + TABLE_NAME, new String[] {});

		if (cursor != null && cursor.moveToNext())
		{
			return new TipoPesagem(cursor.getShort(0), cursor.getShort(1),
					cursor.getInt(2), cursor.getString(3));
		}
		return null;
	}

	public String getMacAddress()
	{
		Cursor cursor = ConexaoManager.getInstance(contexto).rawQuery(
				"select " + TipoPesagem.MACADDRESS + " from " + TABLE_NAME,
				new String[] {});

		if (cursor != null && cursor.moveToNext())
		{
			return cursor.getString(0);
		}
		return null;
	}
}
