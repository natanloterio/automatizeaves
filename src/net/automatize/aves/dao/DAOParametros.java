package net.automatize.aves.dao;

import net.automatize.aves.model.Parametros;
import net.automatize.aves.util.Constantes;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class DAOParametros
{
	private Context contexto;

	public DAOParametros(Context contexto)
	{
		this.contexto = contexto;
	}

	public static final String TABLE_NAME = "PARAMETROS";

	public static final String TABLE_CREATE = "create table if not exists "
			+ TABLE_NAME + " (" + Parametros.CODIGO + " integer primary key, "
			+ Parametros.NUMEROLOTE + " smallint not null, "
			+ Parametros.TAMANHONRLOTE + " smallint not null, "
			+ Parametros.BALANCACOD + " smallint not null);";

	public long gravar(Parametros p)
	{
		return gravar(p, false);
	}

	public long gravar(Parametros p, boolean bIncluir)
	{
		ContentValues values = new ContentValues();
		values.put(Parametros.NUMEROLOTE, p.getNumeroLote());
		values.put(Parametros.TAMANHONRLOTE, p.getTamanhoNumeroLote());
		values.put(Parametros.BALANCACOD, p.getCodigoBalanca());

		SQLiteDatabase database = ConexaoManager.getInstance(contexto);

		long id = 0;
		if (bIncluir)
		{
			id = database.insert(TABLE_NAME, null, values);
		} else
		{
			id = database
					.update(TABLE_NAME,
							values,
							Parametros.CODIGO + "=?",
							new String[] { String
									.valueOf(Constantes.PARAMETROS_TRANSFERENCIA_CODIGO) });
		}

		return id;
	}

	public Parametros getParametros()
	{
		Cursor cursor = ConexaoManager.getInstance(contexto).rawQuery(
				"select * from " + TABLE_NAME + " where CODIGO=?",
				new String[] { String
						.valueOf(Constantes.PARAMETROS_TRANSFERENCIA_CODIGO) });

		if (cursor != null && cursor.moveToNext())
		{
			return new Parametros(Constantes.PARAMETROS_TRANSFERENCIA_CODIGO,
					cursor.getShort(1), cursor.getShort(2), cursor.getShort(3));
		} else
		{
			Parametros p = new Parametros(
					Constantes.PARAMETROS_TRANSFERENCIA_CODIGO, (short) 0,
					(short) 5, (short) 0);
			gravar(p, true);
			return p;
		}
	}
}
