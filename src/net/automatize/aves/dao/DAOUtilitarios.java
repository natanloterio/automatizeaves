package net.automatize.aves.dao;

import java.util.Calendar;
import java.util.Date;

import net.automatize.aves.model.AmostragemBox;
import net.automatize.aves.model.AmostragemData;
import net.automatize.aves.model.AmostragemLote;
import net.automatize.aves.model.AmostragemPeso;
import net.automatize.aves.model.PreAmostragemClassificacao;
import net.automatize.aves.model.Selecao100Classificacao;
import net.automatize.aves.util.Constantes;
import net.automatize.aves.util.FuncoesCompartilhadas;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

public class DAOUtilitarios
{
	private Context context;

	public DAOUtilitarios(Context context)
	{
		this.context = context;
	}

	public boolean limparMemoriaPesos()
	{
		SQLiteDatabase database = ConexaoManager.getInstance(context);
		try
		{
			database.beginTransaction();
			/* confirmar */
			database.delete(DAOAmostragemPesos.TABLE_NAME, null, null);
			database.delete(DAOAmostragemBox.TABLE_NAME, null, null);
			/* confirmar */
			database.delete(DAOAmostragemLotes.TABLE_NAME, null, null);
			database.delete(DAOAmostragemData.TABLE_NAME, null, null);
			database.delete(DAOSelecao100Pesos.TABLE_NAME, null, null);
			database.delete(DAOSelecao100Classificacao.TABLE_NAME, null, null);
			database.delete(DAOSelecao100.TABLE_NAME, null, null);
			database.delete(DAOPreAmostragemPesos.TABLE_NAME, null, null);
			database.delete(DAOPreAmostragemClassificacao.TABLE_NAME, null,
					null);
			database.delete(DAOPreAmostragem.TABLE_NAME, null, null);

			database.setTransactionSuccessful();

		} catch (SQLiteException ex)
		{
			Log.e("ERRO", ex.getMessage());
			FuncoesCompartilhadas.exibirToastMsgLong(context, ex.getMessage());

		} finally
		{
			database.endTransaction();
		}
		return true;
	}

	public boolean inicializarAmostragemSemanal()
	{
		// false = N�O tem dados de outra data
		// true = tem dados de outra data

		// precisa pegar a data da coleta em amostragemdata.dataamostragem
		Cursor cursor = ConexaoManager.getInstance(context).rawQuery(
				"select " + AmostragemData.DATAAMOSTRAGEM + " from "
						+ DAOAmostragemData.TABLE_NAME + " where "
						+ AmostragemData.CODIGO + "=?",
				new String[] { String.valueOf(1) });

		if (cursor.moveToNext() == false)
		{
			ContentValues values = new ContentValues();
			values.put(AmostragemData.CODIGO, Constantes.AMOSTRAGEMDATA_CODIGO);

			Calendar c = Calendar.getInstance();
			values.put(AmostragemData.DATAAMOSTRAGEM, c.getTimeInMillis());

			SQLiteDatabase database = ConexaoManager.getInstance(context);
			database.insert(DAOAmostragemData.TABLE_NAME, null, values);

			return false;
		} else
		{
			Date dataColetada = FuncoesCompartilhadas.dateTimeToDate(cursor
					.getLong(0));
			if (dataColetada != null)
			{
				if (dataColetada.getTime() != FuncoesCompartilhadas
						.dateTimeToDate(new Date().getTime()).getTime())
				{
					Cursor cursor2 = ConexaoManager.getInstance(context)
							.rawQuery(
									"select " + AmostragemLote.CODIGO
											+ " from "
											+ DAOAmostragemLotes.TABLE_NAME
											+ " union " + "select "
											+ AmostragemBox.CODIGO + " from "
											+ DAOAmostragemBox.TABLE_NAME
											+ " union " + "select "
											+ AmostragemPeso.CODIGO + " from "
											+ DAOAmostragemPesos.TABLE_NAME,
									new String[] {});
					if (cursor2.getCount() > 0)
						return true;
					else
						return false;
				}
			}
		}

		return false;
	}

	public boolean classificacaoEmUso(short codigo)
	{
		Cursor cursor = ConexaoManager.getInstance(context).rawQuery(
				"select " + AmostragemBox.CLASSIFICACAO_CODIGO + " from "
						+ DAOAmostragemBox.TABLE_NAME + " where "
						+ AmostragemBox.CLASSIFICACAO_CODIGO + " = ? "
						+ "union " + "select "
						+ PreAmostragemClassificacao.CLASSIFICACAO_CODIGO
						+ " from " + DAOPreAmostragemClassificacao.TABLE_NAME
						+ " where "
						+ PreAmostragemClassificacao.CLASSIFICACAO_CODIGO
						+ " = ? " + "union " + "select "
						+ Selecao100Classificacao.CLASSIFICACAO_CODIGO
						+ " from " + DAOSelecao100Classificacao.TABLE_NAME
						+ " where "
						+ Selecao100Classificacao.CLASSIFICACAO_CODIGO
						+ " = ? ",
				new String[] { String.valueOf(codigo), String.valueOf(codigo),
						String.valueOf(codigo) });

		if (cursor.moveToNext())
		{
			return true;
		}

		return false;
	}
}
