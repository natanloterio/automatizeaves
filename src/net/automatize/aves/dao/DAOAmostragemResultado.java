package net.automatize.aves.dao;

import java.util.ArrayList;
import java.util.List;

import net.automatize.aves.model.AmostragemBox;
import net.automatize.aves.model.AmostragemPeso;
import net.automatize.aves.model.AmostragemResultado;
import net.automatize.aves.model.Classificacao;
import net.automatize.aves.util.FuncoesCompartilhadas;
import android.content.Context;
import android.database.Cursor;

public class DAOAmostragemResultado
{
	private Context context;

	public DAOAmostragemResultado(Context context)
	{
		this.context = context;
	}

	public List<AmostragemResultado> getResultados(int amostragemlote_codigo)
	{
		List<AmostragemResultado> result = new ArrayList<AmostragemResultado>();
		String sql = "select " + AmostragemPeso.AMOSTRAGEMBOX_CODIGO + ","
				+ " C." + Classificacao.SIGLA + "," + AmostragemBox.NRBOX + ","
				+ "(select count(*) from " + DAOAmostragemPesos.TABLE_NAME
				+ " where " + AmostragemPeso.AMOSTRAGEMBOX_CODIGO + "= T."
				+ AmostragemPeso.AMOSTRAGEMBOX_CODIGO + ") as QTD_PESOS, "
				+ "(select sum(" + AmostragemPeso.PESO + ") from "
				+ DAOAmostragemPesos.TABLE_NAME + " where "
				+ AmostragemPeso.AMOSTRAGEMBOX_CODIGO + "=T."
				+ AmostragemPeso.AMOSTRAGEMBOX_CODIGO + ") as PESO_TOTAL, "
				+ "(select avg(" + AmostragemPeso.PESO + ") from "
				+ DAOAmostragemPesos.TABLE_NAME + " where "
				+ AmostragemPeso.AMOSTRAGEMBOX_CODIGO + "=T."
				+ AmostragemPeso.AMOSTRAGEMBOX_CODIGO + ") as PESO_MEDIO, "
				+ "(select min(" + AmostragemPeso.PESO + ") from "
				+ DAOAmostragemPesos.TABLE_NAME + " where "
				+ AmostragemPeso.AMOSTRAGEMBOX_CODIGO + "=T."
				+ AmostragemPeso.AMOSTRAGEMBOX_CODIGO + ") as PESO_MINIMO, "
				+ "(select max(" + AmostragemPeso.PESO + ") from "
				+ DAOAmostragemPesos.TABLE_NAME + " where "
				+ AmostragemPeso.AMOSTRAGEMBOX_CODIGO + "=T."
				+ AmostragemPeso.AMOSTRAGEMBOX_CODIGO + ") as PESO_MAXIMO "
				+ "from " + DAOAmostragemPesos.TABLE_NAME + " T "
				+ "left join " + DAOAmostragemBox.TABLE_NAME + " AB on AB."
				+ AmostragemBox.CODIGO + "= T."
				+ AmostragemPeso.AMOSTRAGEMBOX_CODIGO + " left join "
				+ DAOClassificacao.TABLE_NAME + " C on C."
				+ Classificacao.CODIGO + "= AB."
				+ AmostragemBox.CLASSIFICACAO_CODIGO + " where T."
				+ AmostragemPeso.AMOSTRAGEMLOTE_CODIGO + "=? " + "group by "
				+ AmostragemPeso.AMOSTRAGEMBOX_CODIGO + ","
				+ AmostragemBox.NRBOX + " order by " + AmostragemBox.NRBOX;

		Cursor c = ConexaoManager.getInstance(context).rawQuery(sql,
				new String[] { String.valueOf(amostragemlote_codigo) });

		while (c.moveToNext())
		{
			AmostragemResultado ar = new AmostragemResultado();
			AmostragemBox ab = new AmostragemBox();
			ab.setCodigo(c.getInt(0));
			ab.setClassificacao_descricao(c.getString(1));
			ab.setNrbox(c.getShort(2));
			ar.setAmostragemBox(ab);

			ar.setQtdPesos(c.getInt(3));
			ar.setPesoTotal(c.getDouble(4));
			ar.setPesoMedio(c.getDouble(5));
			ar.setMenorPeso(c.getDouble(6));
			ar.setMaiorPeso(c.getDouble(7));
			result.add(ar);
		}

		return result;
	}
}
