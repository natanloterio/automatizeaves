package net.automatize.aves.dao;

import java.sql.Date;
import java.util.ArrayList;

import net.automatize.aves.model.PreAmostragem;
import net.automatize.aves.model.PreAmostragemClassificacao;
import net.automatize.aves.model.PreAmostragemClassificacaoItem;
import net.automatize.aves.model.PreAmostragemPeso;
import net.automatize.aves.util.Constantes;
import net.automatize.aves.util.FuncoesCompartilhadas;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

public class DAOPreAmostragem
{
	private Context context;

	public DAOPreAmostragem(Context context)
	{
		this.context = context;
	}

	public static final String TABLE_NAME = "PREAMOSTRAGEM";

	public static final String TABLE_CREATE = "create table if not exists "
			+ TABLE_NAME
			+ " ("
			+ PreAmostragem.CODIGO
			+ " smallint primary key, " // sempre 1
			+ PreAmostragem.NRLOTE + " varchar(10) not null, "
			+ PreAmostragem.SEXO + " varchar(1) not null, "
			+ PreAmostragem.TIPO + " varchar(1) not null, "
			+ PreAmostragem.NRAVES + " integer not null, "
			+ PreAmostragem.NRCORTES + " smallint not null, "
			+ PreAmostragem.DATA + " date not null);";

	public PreAmostragem getPreAmostragem()
	{

		String sql = "select " + PreAmostragem.CODIGO + ","
				+ PreAmostragem.NRLOTE + "," + PreAmostragem.SEXO + ","
				+ PreAmostragem.TIPO + "," + PreAmostragem.NRAVES + ","
				+ PreAmostragem.NRCORTES + "," + PreAmostragem.DATA + " from "
				+ DAOPreAmostragem.TABLE_NAME + " where "
				+ PreAmostragem.CODIGO + " = "
				+ Constantes.PREAMOSTRAGEM_CODIGO;

		Cursor c = ConexaoManager.getInstance(context).rawQuery(sql, null);

		if (c.moveToNext())
		{
			PreAmostragem result = new PreAmostragem();
			result.setCodigo(c.getShort(0)); // sempre 1
			result.setNumeroLote(c.getString(1));
			result.setSexo(c.getString(2));
			result.setTipo(c.getString(3));
			result.setNumeroAves(c.getInt(4));
			result.setNumeroCortes(c.getShort(5));
			result.setData(new Date(c.getLong(6)));
			return result;
		}
		return null;
	}

	public int gravar(PreAmostragem preAmostragem)
	{
		ContentValues values = new ContentValues();
		values.put(PreAmostragem.NRLOTE, preAmostragem.getNumeroLote());
		values.put(PreAmostragem.DATA, preAmostragem.getData().getTime());
		values.put(PreAmostragem.SEXO, preAmostragem.getSexo());
		values.put(PreAmostragem.TIPO, preAmostragem.getTipo());
		values.put(PreAmostragem.NRCORTES, preAmostragem.getNumeroCortes());

		// if (preAmostragem.getTipo()
		// .equals(Constantes.TIPOAMOSTRAGEM_INDEX_FIXO))
		// values.put(PreAmostragem.NRAVES, preAmostragem.getNumeroAves());
		// else
		// values.put(PreAmostragem.NRAVES, 0);

		values.put(PreAmostragem.NRAVES, preAmostragem.getNumeroAves());

		long result = 0;
		SQLiteDatabase database = ConexaoManager.getInstance(context);
		if (preAmostragem.getCodigo() == 0)
		{
			// insert
			values.put(PreAmostragem.CODIGO, Constantes.PREAMOSTRAGEM_CODIGO);
			result = database.insert(TABLE_NAME, null, values);
		} else
		{
			// update
			result = database.update(TABLE_NAME, values, PreAmostragem.CODIGO
					+ "=?", new String[] { String
					.valueOf(Constantes.PREAMOSTRAGEM_CODIGO) });
		}

		return (int) result;
	}

	public boolean atualizarNumeroAvesTipoPercentual(int ivalue)
	{
		ContentValues values = new ContentValues();
		values.put(PreAmostragem.NRAVES, ivalue);

		SQLiteDatabase database = ConexaoManager.getInstance(context);
		long result = database
				.update(TABLE_NAME, values, PreAmostragem.CODIGO + "=?",
						new String[] { String
								.valueOf(Constantes.PREAMOSTRAGEM_CODIGO) });
		return (result > 0);
	}

	/* Devido ao numero de sqls metodo criado neste DAO */
	public String calc_faixas_pre_amostragem(PreAmostragem preAmostragem)
	{
		SQLiteDatabase conexao = ConexaoManager.getInstance(context);
		DAOPreAmostragemClassificacao daoClass = new DAOPreAmostragemClassificacao(
				context);
		DAOPreAmostragemPesos daoPesos = new DAOPreAmostragemPesos(context);

		/* valida��es */
		if (preAmostragem.getNumeroCortes() < 2)
			return "No m�nimo duas faixas de classifica��es s�o necess�rias!";

		if (preAmostragem.getTipo() == null
				|| preAmostragem.getTipo().equals(""))
			return "N�o foi poss�vel determinar o tipo da pr�-amostragem!";

		int v_qtd_class = daoClass.count();
		if (preAmostragem.getNumeroCortes() != v_qtd_class)
		{
			return "N�mero de cortes previsto difere da quantidade selecionada!";
		}

		int v_qtd_pesos = daoPesos.count();		
		if (v_qtd_pesos < v_qtd_class)
			return "Quantidade de cortes inferior a quantidade de classifica��es selecionada!";

		if (preAmostragem.getTipo().equals(
				Constantes.TIPOAMOSTRAGEM_VALOR_PERCENTUAL))
		{
			if (daoClass.percentualClasssificacaoZero())
				return "Existem classifica��es sem percentual definido!";

			if (daoClass.percentualClasssificacaoSoma() != 100)
				return "A soma dos percentuais difere de 100%!";
		} else
		{
			// FIXO
			if (v_qtd_pesos != preAmostragem.getNumeroAves())
				return "N�mero de aves pesadas difere da quantidade especificada!";
		}

		/* altera��o dos dados */

		/* CUIDAR COM TRANSA��O */
		conexao.beginTransaction();

		try
		{
			if (preAmostragem.getTipo().equals(
					Constantes.TIPOAMOSTRAGEM_VALOR_FIXO))
			{
				int v_pesos = (preAmostragem.getNumeroAves() / preAmostragem
						.getNumeroCortes());

				ContentValues values = new ContentValues();
				values.put(PreAmostragemClassificacao.QTDPESOS, v_pesos);
				values.put(PreAmostragemClassificacao.FAIXAINICIAL, 0);
				values.put(PreAmostragemClassificacao.FAIXAFINAL, 0);

				long result = conexao.update(
						DAOPreAmostragemClassificacao.TABLE_NAME, values, null,
						null);
				if (result <= 0)
				{
					return Constantes.MSG_ERRO_GRAVACAO;
				}
			} else
			{
				ArrayList<PreAmostragemClassificacaoItem> result = daoClass
						.carregaClassificacoesConfiguradas();

				for (PreAmostragemClassificacaoItem item : result)
				{
					int v_pesos = (v_qtd_pesos * item.getPercentual()) / 100;
					if (v_pesos < 1)
						v_pesos = 1;

					if (daoClass.atualizaQtdPesosPercentual(conexao,
							item.getClassificacao_codigo(), v_pesos) == false)
						return Constantes.MSG_ERRO_GRAVACAO;
				}
			}

			int v_nr_faixa = 1;

			// select first 1 peso
			// from pre_amostragem_pesos
			// order by peso
			// into :v_faixa_inicial;

			Cursor cursor = conexao.rawQuery("select " + PreAmostragemPeso.PESO
					+ " from " + DAOPreAmostragemPesos.TABLE_NAME
					+ " order by " + PreAmostragemPeso.PESO + " limit 1", null);

			cursor.moveToNext();

			int v_faixa_inicial = cursor.getInt(0);

			// select first 1
			// cod_classificacao
			// , qtd_pesos
			// from pre_amostragem_class
			// order by cod_classificacao desc
			// into :v_cod_classificacao
			// , :v_qtd_pesos_class;

			cursor = conexao.rawQuery("select "
					+ PreAmostragemClassificacao.CLASSIFICACAO_CODIGO + ", "
					+ PreAmostragemClassificacao.QTDPESOS + " from "
					+ DAOPreAmostragemClassificacao.TABLE_NAME + " order by "
					+ PreAmostragemClassificacao.CLASSIFICACAO_CODIGO
					+ " desc " + "limit 1", null);

			cursor.moveToNext();

			int v_cod_classificacao = cursor.getInt(0);
			int v_qtd_pesos_class = cursor.getInt(1);

			int v_faixa_final = 0;

			// if (:v_qtd_pesos_class > 1) then begin
			// select first 1 skip (:v_qtd_pesos_class - 1) peso
			// from pre_amostragem_pesos
			// order by peso
			// into :v_faixa_final;
			// end
			// else
			// v_faixa_final = :v_faixa_inicial;

			if (v_qtd_pesos_class > 1)
			{
				cursor = conexao.rawQuery("select " + PreAmostragemPeso.PESO
						+ " from " + DAOPreAmostragemPesos.TABLE_NAME
						+ " order by " + PreAmostragemPeso.PESO + " limit 1 "
						+ "offset " + (v_qtd_pesos_class - 1), null);

				cursor.moveToNext();

				v_faixa_final = cursor.getInt(0);
			} else
			{
				v_faixa_final = v_faixa_inicial;
			}

			// update pre_amostragem_class
			// set faixa_inicial = :v_faixa_inicial
			// , faixa_final = :v_faixa_final
			// where (cod_classificacao = :v_cod_classificacao);
			//
			// v_faixa_inicial = :v_faixa_final + 1;
			// v_nr_faixa = :v_nr_faixa + 1;
			// v_qtd_pesos = :v_qtd_pesos_class;

			ContentValues values = new ContentValues();
			values.put(PreAmostragemClassificacao.FAIXAINICIAL, v_faixa_inicial);
			values.put(PreAmostragemClassificacao.FAIXAFINAL, v_faixa_final);

			long result = conexao.update(
					DAOPreAmostragemClassificacao.TABLE_NAME, values,
					PreAmostragemClassificacao.CLASSIFICACAO_CODIGO + "=?",
					new String[] { String.valueOf(v_cod_classificacao) });

			if (result < 1)
				return Constantes.MSG_ERRO_GRAVACAO;

			v_faixa_inicial = v_faixa_final + 1;
			v_nr_faixa = v_nr_faixa + 1;
			v_qtd_pesos = v_qtd_pesos_class;

			// for
			// select skip 1
			// cod_classificacao
			// , qtd_pesos
			// from pre_amostragem_class
			// order by cod_classificacao desc
			// into :v_cod_classificacao
			// , :v_qtd_pesos_class
			// do

			String sql = "select "
					+ PreAmostragemClassificacao.CLASSIFICACAO_CODIGO + ", "
					+ PreAmostragemClassificacao.QTDPESOS + " from "
					+ DAOPreAmostragemClassificacao.TABLE_NAME + " order by "
					+ PreAmostragemClassificacao.CLASSIFICACAO_CODIGO
					+ " desc " + "limit (select count(*) from "
					+ DAOPreAmostragemClassificacao.TABLE_NAME + ") "
					+ " offset 1";
			cursor = conexao.rawQuery(sql, null);

			while (cursor.moveToNext())
			{
				v_cod_classificacao = cursor.getInt(0);
				v_qtd_pesos_class = cursor.getInt(1);

				// if (:v_nr_faixa = :v_qtd_class) then
				// break;
				//
				// v_qtd_pesos = :v_qtd_pesos + :v_qtd_pesos_class;
				//
				// select first 1 skip (:v_qtd_pesos - 1) peso
				// from pre_amostragem_pesos
				// order by peso
				// into :v_faixa_final;

				if (v_nr_faixa == v_qtd_class)
					break;

				v_qtd_pesos = v_qtd_pesos + v_qtd_pesos_class;

				Cursor cursorLocal = conexao.rawQuery("select "
						+ PreAmostragemPeso.PESO + " from "
						+ DAOPreAmostragemPesos.TABLE_NAME + " order by "
						+ PreAmostragemPeso.PESO + " limit 1 " + "offset "
						+ (v_qtd_pesos - 1), null);

				cursorLocal.moveToNext();
				v_faixa_final = cursorLocal.getInt(0);

				// update pre_amostragem_class
				// set faixa_inicial = :v_faixa_inicial
				// , faixa_final = :v_faixa_final
				// where (cod_classificacao = :v_cod_classificacao);
				//
				// v_faixa_inicial = :v_faixa_final + 1;
				// v_nr_faixa = :v_nr_faixa + 1;

				values = new ContentValues();
				values.put(PreAmostragemClassificacao.FAIXAINICIAL,
						v_faixa_inicial);
				values.put(PreAmostragemClassificacao.FAIXAFINAL, v_faixa_final);

				result = conexao.update(
						DAOPreAmostragemClassificacao.TABLE_NAME, values,
						PreAmostragemClassificacao.CLASSIFICACAO_CODIGO + "=?",
						new String[] { String.valueOf(v_cod_classificacao) });

				if (result < 1)
					return Constantes.MSG_ERRO_GRAVACAO;

				v_faixa_inicial = v_faixa_final + 1;
				v_nr_faixa = v_nr_faixa + 1;
			}

			// v_faixa_inicial = :v_faixa_final + 1;
			//
			// select max(peso)
			// from pre_amostragem_pesos
			// into :v_faixa_final;
			//
			// update pre_amostragem_class
			// set faixa_inicial = :v_faixa_inicial
			// , faixa_final = :v_faixa_final
			// where (cod_classificacao = :v_cod_classificacao);

			v_faixa_inicial = v_faixa_final + 1;

			cursor = conexao.rawQuery("select max(" + PreAmostragemPeso.PESO
					+ ") " + "from " + DAOPreAmostragemPesos.TABLE_NAME, null);

			cursor.moveToNext();

			v_faixa_final = cursor.getInt(0);

			values = new ContentValues();
			values.put(PreAmostragemClassificacao.FAIXAINICIAL, v_faixa_inicial);
			values.put(PreAmostragemClassificacao.FAIXAFINAL, v_faixa_final);

			result = conexao.update(DAOPreAmostragemClassificacao.TABLE_NAME,
					values, PreAmostragemClassificacao.CLASSIFICACAO_CODIGO
							+ "=?",
					new String[] { String.valueOf(v_cod_classificacao) });

			if (result < 1)
				return Constantes.MSG_ERRO_GRAVACAO;

			conexao.setTransactionSuccessful();

		} catch (SQLiteException ex)
		{
			FuncoesCompartilhadas.log(ex.getLocalizedMessage());
			return Constantes.MSG_ERRO_GRAVACAO + " - "
					+ ex.getLocalizedMessage();

		} finally
		{
			conexao.endTransaction();
		}

		return null;
	}
}
