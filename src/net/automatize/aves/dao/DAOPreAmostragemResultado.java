package net.automatize.aves.dao;

import java.util.ArrayList;

import net.automatize.aves.model.Classificacao;
import net.automatize.aves.model.PreAmostragemClassificacao;
import net.automatize.aves.model.PreAmostragemPeso;
import net.automatize.aves.model.PreAmostragemSelecao100Resultado;
import net.automatize.aves.util.FuncoesCompartilhadas;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class DAOPreAmostragemResultado
{

	private Context context;

	public DAOPreAmostragemResultado(Context context)
	{
		this.context = context;
	}

	public ArrayList<PreAmostragemSelecao100Resultado> getResultado()
	{
		ArrayList<PreAmostragemSelecao100Resultado> result = new ArrayList<PreAmostragemSelecao100Resultado>();
		String sql = "select "
				+ PreAmostragemClassificacao.CLASSIFICACAO_CODIGO + ", "
				+ Classificacao.SIGLA + ", "
				+ PreAmostragemClassificacao.FAIXAINICIAL + ", "
				+ PreAmostragemClassificacao.FAIXAFINAL + " from "
				+ DAOPreAmostragemClassificacao.TABLE_NAME + " T "
				+ "left join " + DAOClassificacao.TABLE_NAME + " C on C."
				+ Classificacao.CODIGO + " = "
				+ PreAmostragemClassificacao.CLASSIFICACAO_CODIGO
				+ " order by "
				+ PreAmostragemClassificacao.CLASSIFICACAO_CODIGO;
		
		FuncoesCompartilhadas.log(sql);

		SQLiteDatabase conexao = ConexaoManager.getInstance(context);
		Cursor cursorDadosAtualizados = conexao.rawQuery(sql, null);

		while (cursorDadosAtualizados.moveToNext())
		{
			Cursor cursorLocal = conexao.rawQuery(
					"select count(*), " + "sum(" + PreAmostragemPeso.PESO
							+ "), " + "avg(" + PreAmostragemPeso.PESO + ") "
							+ "from " + DAOPreAmostragemPesos.TABLE_NAME
							+ " where " + PreAmostragemPeso.PESO
							+ " between ? and ?",
					new String[] {
							String.valueOf(cursorDadosAtualizados.getInt(2)),
							String.valueOf(cursorDadosAtualizados.getInt(3)) });

			cursorLocal.moveToNext();

			PreAmostragemSelecao100Resultado r = new PreAmostragemSelecao100Resultado();
			r.setClassificacao_sigla(cursorDadosAtualizados.getString(1));
			r.setFaixa_inicial(cursorDadosAtualizados.getInt(2));
			r.setFaixa_final(cursorDadosAtualizados.getInt(3));
			r.setQtd(cursorLocal.getInt(0));
			r.setAcumulado(cursorLocal.getInt(1));
			r.setMedia(cursorLocal.getInt(2));
			result.add(r);
		}

		/* Totais */
		Cursor cursorLocal = conexao.rawQuery("select count(*), " + "sum("
				+ PreAmostragemPeso.PESO + "), " + "avg("
				+ PreAmostragemPeso.PESO + ") " + "from "
				+ DAOPreAmostragemPesos.TABLE_NAME, null);

		cursorLocal.moveToNext();

		PreAmostragemSelecao100Resultado r = new PreAmostragemSelecao100Resultado();
		r.setClassificacao_codigo((short) -999);
		r.setClassificacao_sigla("TOTAL");
		r.setQtd(cursorLocal.getInt(0));
		r.setAcumulado(cursorLocal.getInt(1));
		r.setMedia(cursorLocal.getInt(2));
		result.add(r);

		return result;
	}
}
