package net.automatize.aves.dao;

import java.util.ArrayList;
import java.util.List;

import net.automatize.aves.model.AmostragemBox;
import net.automatize.aves.model.AmostragemLote;
import net.automatize.aves.model.AmostragemPeso;
import net.automatize.aves.model.AmostragemTransferenciaPeso;
import net.automatize.aves.util.FuncoesCompartilhadas;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

public class DAOAmostragemPesos
{
	private Context context;

	public DAOAmostragemPesos(Context context)
	{
		this.context = context;
	}

	public static final String TABLE_NAME = "AMOSTRAGEMPESOS";

	public static final String TABLE_CREATE = "create table if not exists "
			+ TABLE_NAME
			+ " ("
			+ AmostragemPeso.CODIGO
			+ " integer primary key autoincrement, "
			+ AmostragemPeso.AMOSTRAGEMBOX_CODIGO
			+ " smallint not null, "
			+ AmostragemPeso.AMOSTRAGEMLOTE_CODIGO
			+ " integer not null, "
			+ AmostragemPeso.PESO
			+ " integer not null);"
			// fk
			+ "alter table " + TABLE_NAME
			+ " add constraint fk_cod_box foreign key ("
			+ AmostragemPeso.AMOSTRAGEMBOX_CODIGO + ") references "
			+ DAOAmostragemBox.TABLE_NAME
			+ " ("
			+ AmostragemBox.CODIGO
			+ ");"
			// fk
			+ "alter table " + TABLE_NAME
			+ " add constraint fk_cod_amostragemlotes foreign key ("
			+ AmostragemPeso.AMOSTRAGEMLOTE_CODIGO + ") references "
			+ DAOAmostragemLotes.TABLE_NAME + " (" + AmostragemLote.CODIGO
			+ ");";

	public int inserir(AmostragemPeso amostragemPeso)
	{
		ContentValues values = new ContentValues();
		values.put(AmostragemPeso.AMOSTRAGEMLOTE_CODIGO,
				amostragemPeso.getAmostragemlote_codigo());
		values.put(AmostragemPeso.AMOSTRAGEMBOX_CODIGO,
				amostragemPeso.getAmostragembox_codigo());
		values.put(AmostragemPeso.PESO, amostragemPeso.getPeso());

		SQLiteDatabase database = ConexaoManager.getInstance(context);
		int insertId = (int) database.insert(TABLE_NAME, null, values);

		return insertId;
	}

	public List<AmostragemPeso> getPesosPorLoteBox(int amostragemlote_codigo,
			int amostragembox_codigo)
	{
		List<AmostragemPeso> result = new ArrayList<AmostragemPeso>();
		Cursor cursor = ConexaoManager.getInstance(context).rawQuery(
				"select " + AmostragemPeso.CODIGO + "," + AmostragemPeso.PESO
						+ " from " + TABLE_NAME + " where "
						+ AmostragemPeso.AMOSTRAGEMLOTE_CODIGO + " =? "
						+ "and " + AmostragemPeso.AMOSTRAGEMBOX_CODIGO + "=? "
						+ "order by " + AmostragemPeso.CODIGO + " desc",
				new String[] { String.valueOf(amostragemlote_codigo),
						String.valueOf(amostragembox_codigo) });

		if (cursor != null)
		{
			while (cursor.moveToNext())
			{
				result.add(new AmostragemPeso(cursor.getInt(0), cursor
						.getInt(1)));
			}
		}
		return result;
	}

	public boolean excluir(int amostragemlote_codigo)
	{
		try
		{
			SQLiteDatabase database = ConexaoManager.getInstance(context);
			// int result = database.delete(TABLE_NAME, AmostragemLote.CODIGO
			// + "=?",
			// new String[] { String.valueOf(amostragemlote_codigo) });

			database.delete(TABLE_NAME, AmostragemLote.CODIGO + "=?",
					new String[] { String.valueOf(amostragemlote_codigo) });

			return true;

			// if (result > 0)
			// return true;
			// else
			// return false;
		} catch (SQLiteException ex)
		{
			FuncoesCompartilhadas.log(ex.getMessage());
			return false;
		}
	}

	public boolean limparPesosBox(int amostragembox_codigo)
	{
		return limparPesosBox(amostragembox_codigo, null);
	}

	public boolean limparPesosBox(int amostragembox_codigo,
			SQLiteDatabase conexao)
	{
		try
		{
			SQLiteDatabase database;

			if (conexao != null)
				database = conexao;
			else
				database = ConexaoManager.getInstance(context);

			database.delete(TABLE_NAME, AmostragemPeso.AMOSTRAGEMBOX_CODIGO
					+ "=?",
					new String[] { String.valueOf(amostragembox_codigo) });

			return true;
			// if (result > 0)
			// return true;
			// else
			// return false;
		} catch (SQLiteException ex)
		{
			FuncoesCompartilhadas.log(ex.getMessage());
			return false;
		}
	}

	public List<AmostragemTransferenciaPeso> lerPesosParaTransferencia()
	{
		List<AmostragemTransferenciaPeso> result = new ArrayList<AmostragemTransferenciaPeso>();
		String sql = "select " + AmostragemLote.NR_LOTE + ","
				+ AmostragemLote.SEXO + "," + AmostragemBox.NRBOX + ","
				+ AmostragemBox.CLASSIFICACAO_CODIGO + ","
				+ AmostragemPeso.PESO + " from " + TABLE_NAME + " P " + "join "
				+ DAOAmostragemBox.TABLE_NAME + " B on B."
				+ AmostragemBox.CODIGO + " = P."
				+ AmostragemPeso.AMOSTRAGEMBOX_CODIGO + " join "
				+ DAOAmostragemLotes.TABLE_NAME + " L on L."
				+ AmostragemLote.CODIGO + " = B."
				+ AmostragemBox.AMOSTRAGEMLOTE_CODIGO + " order by P."
				+ AmostragemPeso.AMOSTRAGEMLOTE_CODIGO + ", P."
				+ AmostragemPeso.AMOSTRAGEMBOX_CODIGO + ", P."
				+ AmostragemPeso.CODIGO;

		Cursor c = ConexaoManager.getInstance(context).rawQuery(sql, null);

		while (c.moveToNext())
		{
			AmostragemTransferenciaPeso a = new AmostragemTransferenciaPeso();
			a.setNr_lote(c.getString(0));
			a.setSexo(c.getString(1));
			a.setNr_box(c.getShort(2));
			a.setClassificacao_codigo(c.getString(3));
			a.setPeso(c.getDouble(4));
			result.add(a);
		}
		return result;
	}

	public int boxTemPesos(int amostragembox_codigo)
	{
		Cursor c = ConexaoManager.getInstance(context).rawQuery(
				"select count(*) from " + TABLE_NAME + " where "
						+ AmostragemPeso.AMOSTRAGEMBOX_CODIGO + "=?",
				new String[] { String.valueOf(amostragembox_codigo) });

		c.moveToNext();

		return c.getInt(0);
	}
}