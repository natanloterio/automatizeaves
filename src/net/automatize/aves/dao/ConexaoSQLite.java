package net.automatize.aves.dao;

import net.automatize.aves.util.Constantes;
import net.automatize.aves.util.FuncoesCompartilhadas;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ConexaoSQLite extends SQLiteOpenHelper
{
	private SQLiteDatabase database;

	public ConexaoSQLite(Context context)
	{
		super(context, Constantes.DIRETORIO_ARMAZENAMENTO
				+ Constantes.DATABASE_NAME, null, Constantes.DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db)
	{
		/* cuidar com ordena��o */
		db.execSQL(DAOAmostragemBox.TABLE_CREATE);
		db.execSQL(DAOClassificacao.TABLE_CREATE);
		db.execSQL(DAOTipoPesagem.TABLE_CREATE);
		db.execSQL(DAOAmostragemLotes.TABLE_CREATE);
		db.execSQL(DAOAmostragemData.TABLE_CREATE);
		db.execSQL(DAOAmostragemPesos.TABLE_CREATE);
		db.execSQL(DAOSelecao100Pesos.TABLE_CREATE);
		db.execSQL(DAOSelecao100Classificacao.TABLE_CREATE);
		db.execSQL(DAOSelecao100.TABLE_CREATE);
		db.execSQL(DAOPreAmostragemPesos.TABLE_CREATE);
		db.execSQL(DAOPreAmostragemClassificacao.TABLE_CREATE);
		db.execSQL(DAOPreAmostragem.TABLE_CREATE);
		db.execSQL(DAOParametros.TABLE_CREATE);
		setDatabase(db);
	}

	@Override
	public void onOpen(SQLiteDatabase db)
	{
		super.onOpen(db);
		if (!db.isReadOnly())
		{
			// Enable foreign key constraints
			db.execSQL("PRAGMA foreign_keys=ON;");
			FuncoesCompartilhadas.log("PRAGMA foreign_keys=ON;");
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
	{
		setDatabase(db);
	}

	public SQLiteDatabase getDatabase()
	{
		return database;
	}

	public void setDatabase(SQLiteDatabase database)
	{
		this.database = database;
	}
}
