package net.automatize.aves.dao;

import java.util.Date;

import net.automatize.aves.model.AmostragemData;
import net.automatize.aves.util.Constantes;
import android.content.Context;
import android.database.Cursor;

public class DAOAmostragemData
{
	private Context context;

	public DAOAmostragemData(Context context)
	{
		this.context = context;
	}

	public static final String TABLE_NAME = "AMOSTRAGEMDATA";

	public static final String TABLE_CREATE = "create table if not exists "
			+ TABLE_NAME + " (" + AmostragemData.CODIGO
			+ " integer primary key, " // ser� sempre 1, tem constante
										// AMOSTRAGEMDATA_CODIGO
			+ AmostragemData.DATAAMOSTRAGEM + " date not null);";

	public Date getAmostragemData()
	{
		Cursor c = ConexaoManager.getInstance(context)
				.rawQuery(
						"select " + AmostragemData.DATAAMOSTRAGEM + " from "
								+ TABLE_NAME + " where "
								+ AmostragemData.CODIGO + "=?",
						new String[] { String
								.valueOf(Constantes.AMOSTRAGEMDATA_CODIGO) });
		if (c.moveToNext())
		{
			return new Date(c.getLong(0));
		}
		return null;
	}
}
