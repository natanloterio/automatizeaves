package net.automatize.aves.dao;

import java.util.ArrayList;

import net.automatize.aves.model.Classificacao;
import net.automatize.aves.model.PreAmostragemSelecao100Resultado;
import net.automatize.aves.model.Selecao100Classificacao;
import net.automatize.aves.model.Selecao100Peso;
import android.content.Context;
import android.database.Cursor;

public class DAOSelecao100Resultado
{

	private Context context;

	public DAOSelecao100Resultado(Context context)
	{
		this.context = context;
	}

	public ArrayList<PreAmostragemSelecao100Resultado> getResultado()
	{
		ArrayList<PreAmostragemSelecao100Resultado> result = new ArrayList<PreAmostragemSelecao100Resultado>();

		DAOSelecao100Classificacao daoSelecao100Classificacao = new DAOSelecao100Classificacao(
				context);
		DAOClassificacao daoClassificacao = new DAOClassificacao(context);
		ArrayList<Selecao100Classificacao> listaClassificacoes = daoSelecao100Classificacao
				.getClassificacoes();

		for (Selecao100Classificacao item : listaClassificacoes)
		{
			Classificacao classificacao = daoClassificacao
					.getClassificacao(item.getClassificacaoCodigo());
			String r_sigla = String.format("%02d",
					item.getClassificacaoCodigo())
					+ "-" + classificacao.getSigla();

			String sql = "select count(*), " + "coalesce(sum("
					+ Selecao100Peso.PESO + "), 0), " + "coalesce(avg("
					+ Selecao100Peso.PESO + "), 0) " + "from "
					+ DAOSelecao100Pesos.TABLE_NAME + " where "
					+ Selecao100Peso.CLASSIFICACAO_CODIGO + "=?";

			Cursor c2 = ConexaoManager.getInstance(context)
					.rawQuery(
							sql,
							new String[] { String.valueOf(item
									.getClassificacaoCodigo()) });
			c2.moveToNext();

			PreAmostragemSelecao100Resultado resultado = new PreAmostragemSelecao100Resultado();
			resultado.setClassificacao_codigo(item.getClassificacaoCodigo());
			resultado.setClassificacao_sigla(r_sigla);
			resultado.setFaixa_inicial(item.getFaixaInicial());
			resultado.setFaixa_final(item.getFaixaFinal());
			resultado.setQtd(c2.getInt(0));
			resultado.setAcumulado(c2.getInt(1));
			resultado.setMedia(c2.getInt(2));

			result.add(resultado);
		}

		/* -1 = FB */

		String sql = "select count(*), " + "coalesce(sum("
				+ Selecao100Peso.PESO + "), 0), " + "coalesce(avg("
				+ Selecao100Peso.PESO + "), 0) " + "from "
				+ DAOSelecao100Pesos.TABLE_NAME + " where "
				+ Selecao100Peso.CLASSIFICACAO_CODIGO + "=-1";

		Cursor c2 = ConexaoManager.getInstance(context).rawQuery(sql, null);
		c2.moveToNext();

		if (c2.getInt(0) > 0)
		{
			Cursor c21 = ConexaoManager.getInstance(context).rawQuery(
					"select min(" + Selecao100Classificacao.FAIXAINICIAL
							+ ") - 1 " + "from "
							+ DAOSelecao100Classificacao.TABLE_NAME, null);
			
			c21.moveToNext();

			PreAmostragemSelecao100Resultado resultado = new PreAmostragemSelecao100Resultado();
			resultado.setClassificacao_sigla("97-FB");
			resultado.setFaixa_inicial(1);
			resultado.setFaixa_final(c21.getInt(0));
			resultado.setQtd(c2.getInt(0));
			resultado.setAcumulado(c2.getInt(1));
			resultado.setMedia(c2.getInt(2));
			result.add(resultado);
		}

		/* 0 = FA */

		String sql2 = "select count(*), " + "coalesce(sum("
				+ Selecao100Peso.PESO + "), 0), " + "coalesce(avg("
				+ Selecao100Peso.PESO + "), 0) " + "from "
				+ DAOSelecao100Pesos.TABLE_NAME + " where "
				+ Selecao100Peso.CLASSIFICACAO_CODIGO + "=0";

		Cursor c3 = ConexaoManager.getInstance(context).rawQuery(sql2, null);
		c3.moveToNext();

		if (c3.getInt(0) > 0)
		{
			Cursor c31 = ConexaoManager.getInstance(context).rawQuery(
					"select max(" + Selecao100Classificacao.FAIXAFINAL
							+ ") + 1 " + "from "
							+ DAOSelecao100Classificacao.TABLE_NAME, null);
			
			c31.moveToNext();

			PreAmostragemSelecao100Resultado resultado = new PreAmostragemSelecao100Resultado();
			resultado.setClassificacao_sigla("98-FA");
			resultado.setFaixa_inicial(c31.getInt(0));
			resultado.setFaixa_final(99999);
			resultado.setQtd(c3.getInt(0));
			resultado.setAcumulado(c3.getInt(1));
			resultado.setMedia(c3.getInt(2));
			result.add(resultado);
		}

		/* total */

		String sql3 = "select count(*), " + "coalesce(sum("
				+ Selecao100Peso.PESO + "), 0), " + "coalesce(avg("
				+ Selecao100Peso.PESO + "), 0) " + "from "
				+ DAOSelecao100Pesos.TABLE_NAME;

		Cursor c4 = ConexaoManager.getInstance(context).rawQuery(sql3, null);
		c4.moveToNext();

		PreAmostragemSelecao100Resultado resultado = new PreAmostragemSelecao100Resultado();
		resultado.setClassificacao_sigla("99-==");
		resultado.setFaixa_inicial(1);
		resultado.setFaixa_final(99999);
		resultado.setQtd(c4.getInt(0));
		resultado.setAcumulado(c4.getInt(1));
		resultado.setMedia(c4.getInt(2));
		result.add(resultado);

		return result;
	}
}
