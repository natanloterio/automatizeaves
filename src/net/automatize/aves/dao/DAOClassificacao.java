package net.automatize.aves.dao;

import java.util.ArrayList;
import java.util.List;

import net.automatize.aves.model.Classificacao;
import net.automatize.aves.util.Constantes;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class DAOClassificacao
{
	private Context contexto = null;

	public DAOClassificacao(Context contexto)
	{
		this.contexto = contexto;
	}

	public static final String TABLE_NAME = "CLASSIFICACAO";

	public static final String TABLE_CREATE = "create table if not exists "
			+ TABLE_NAME + " (" + Classificacao.CODIGO
			+ " integer primary key, " + Classificacao.SIGLA + " varchar(2));";

	public int inserir(Classificacao classificacoes)
	{
		ContentValues values = new ContentValues();
		values.put(Classificacao.CODIGO, classificacoes.getCodigo());
		values.put(Classificacao.SIGLA, classificacoes.getSigla());

		SQLiteDatabase database = ConexaoManager.getInstance(contexto);
		int insertId = (int) database.insert(TABLE_NAME, null, values);

		return insertId;
	}

	public int alterar(Classificacao classificacoes)
	{
		ContentValues values = new ContentValues();
		values.put(Classificacao.SIGLA, classificacoes.getSigla());

		SQLiteDatabase database = ConexaoManager.getInstance(contexto);

		int updateId = database.update(TABLE_NAME, values, Classificacao.CODIGO
				+ "=?",
				new String[] { String.valueOf(classificacoes.getCodigo()) });

		Log.i("update", String.valueOf(updateId));

		return updateId;
	}

	public int excluir(short codigo)
	{
		ContentValues values = new ContentValues();
		values.put(Classificacao.CODIGO, codigo);

		SQLiteDatabase database = ConexaoManager.getInstance(contexto);
		int removedId = database.delete(TABLE_NAME,
				Classificacao.CODIGO + "=?",
				new String[] { String.valueOf(codigo) });

		Log.i(Constantes.MSG_REGISTRO_EXCLUIDO_SUCESSO,
				String.valueOf(removedId));

		return removedId;
	}

	public Classificacao getClassificacao(short codigo)
	{
		Cursor cursor = ConexaoManager.getInstance(contexto).rawQuery(
				"select " + Classificacao.CODIGO + ", " + Classificacao.SIGLA
						+ " from " + TABLE_NAME + " where "
						+ Classificacao.CODIGO + " = ?",
				new String[] { String.valueOf(codigo) });

		if (cursor != null && cursor.moveToNext())
		{
			return new Classificacao(cursor.getShort(0), cursor.getString(1));
		}
		return null;
	}

	public boolean siglaCadastrada(short codigo, String sigla)
	{
		Cursor cursor = ConexaoManager.getInstance(contexto).rawQuery(
				"select " + Classificacao.CODIGO + " from " + TABLE_NAME
						+ " where " + Classificacao.CODIGO + " <> ? "
						+ "and upper(" + Classificacao.SIGLA + ") = upper(?)",
				new String[] { String.valueOf(codigo), sigla });

		if (cursor != null && cursor.moveToNext())
			return true;

		return false;
	}

	public List<Classificacao> getClassificacoes(boolean bVazio)
	{

		List<Classificacao> lista = new ArrayList<Classificacao>();
		Cursor cursor = ConexaoManager.getInstance(contexto).rawQuery(
				"select " + Classificacao.CODIGO + ", " + Classificacao.SIGLA
						+ " from " + TABLE_NAME + " where "
						+ Classificacao.CODIGO + " > 0", null);

		if (cursor != null)
		{
			if (bVazio)
				lista.add(new Classificacao((short) 0, ""));

			while (cursor.moveToNext())
			{
				lista.add(new Classificacao(cursor.getShort(0), cursor
						.getString(1)));
			}
		}
		return lista;
	}

	public short proximoCodigo()
	{
		Cursor cursor = ConexaoManager.getInstance(contexto).rawQuery(
				"select coalesce(max(" + Classificacao.CODIGO + "), 0) + 1 as "
						+ Classificacao.CODIGO + "  from " + TABLE_NAME, null);

		if (cursor != null && cursor.moveToNext())
		{
			return cursor.getShort(0);
		}
		return 0;
	}

	public boolean inserirPadrao()
	{
		for (String s : Constantes.CLASSIFICACOES_PADRAO)
		{
			Classificacao c = new Classificacao(proximoCodigo(), s);
			if (inserir(c) == 0)
				return false;
		}
		return true;
	}

	public int count()
	{
		Cursor cursor = ConexaoManager.getInstance(contexto).rawQuery(
				"select count(*) as QTD from " + TABLE_NAME, null);

		cursor.moveToNext();
		return cursor.getInt(0);
	}
}
