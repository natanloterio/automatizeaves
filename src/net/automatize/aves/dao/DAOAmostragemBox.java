package net.automatize.aves.dao;

import java.util.ArrayList;
import java.util.List;

import net.automatize.aves.model.AmostragemBox;
import net.automatize.aves.model.AmostragemLote;
import net.automatize.aves.model.Classificacao;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class DAOAmostragemBox
{

	private Context context = null;

	public DAOAmostragemBox(Context context)
	{
		this.context = context;
	}

	public static final String TABLE_NAME = "AMOSTRAGEMBOX";

	public static final String TABLE_CREATE = "create table if not exists "
			+ TABLE_NAME + " (" + AmostragemBox.CODIGO
			+ " integer primary key autoincrement, " + AmostragemBox.NRBOX
			+ " integer, " + AmostragemBox.AMOSTRAGEMLOTE_CODIGO
			+ " integer references " + DAOAmostragemLotes.TABLE_NAME + " ("
			+ AmostragemLote.CODIGO + "), " + AmostragemBox.QTDAVESPESAR
			+ " integer, " + AmostragemBox.CLASSIFICACAO_CODIGO
			+ " integer references " + DAOClassificacao.TABLE_NAME + " ("
			+ Classificacao.CODIGO + "));" + "alter table " + TABLE_NAME
			+ " add constraint uk UNIQUE ("
			+ AmostragemBox.AMOSTRAGEMLOTE_CODIGO + ", " + AmostragemBox.NRBOX
			+ ");";

	public int gravar(AmostragemBox amostragemBox)
	{
		ContentValues values = new ContentValues();
		values.put(AmostragemBox.NRBOX, amostragemBox.getNrbox());
		values.put(AmostragemBox.AMOSTRAGEMLOTE_CODIGO,
				amostragemBox.getAmostragemlote_codigo());
		values.put(AmostragemBox.QTDAVESPESAR, amostragemBox.getQtdAvesPesar());
		values.put(AmostragemBox.CLASSIFICACAO_CODIGO,
				amostragemBox.getClassificacao_codigo());

		SQLiteDatabase database = ConexaoManager.getInstance(context);

		int id = 0;
		if (amostragemBox.getCodigo() > 0)
		{
			id = (int) database.update(TABLE_NAME, values, AmostragemBox.CODIGO
					+ "=?",
					new String[] { String.valueOf(amostragemBox.getCodigo()) });
		} else
		{
			id = (int) database.insert(TABLE_NAME, null, values);
		}

		return id;
	}

	public AmostragemBox getAmostragemBox(int codigo)
	{
		Cursor cursor = ConexaoManager.getInstance(context).rawQuery(
				"select " + AmostragemBox.CODIGO + ", " + AmostragemBox.NRBOX
						+ ", " + AmostragemBox.AMOSTRAGEMLOTE_CODIGO + ", "
						+ AmostragemBox.QTDAVESPESAR + ", "
						+ AmostragemBox.CLASSIFICACAO_CODIGO + " from "
						+ TABLE_NAME + " where " + AmostragemBox.CODIGO + " = "
						+ codigo, null);
		if (cursor != null)
		{
			cursor.moveToFirst();

			return new AmostragemBox(cursor.getShort(0), cursor.getShort(1),
					cursor.getInt(2), cursor.getInt(3), cursor.getShort(4));
		} else
		{
			return null;
		}
	}

	public int excluir(int amostragembox_codigo)
	{
		return excluir(amostragembox_codigo, null);
	}

	public int excluir(int amotragembox_codigo, SQLiteDatabase conexao)
	{
		// SQLiteDatabase database;
		if (conexao == null)
		{
			conexao = ConexaoManager.getInstance(context);
		}
		// else
		// {
		// database = conexao;
		// }

		return conexao.delete(TABLE_NAME, AmostragemBox.CODIGO + "=?",
				new String[] { String.valueOf(amotragembox_codigo) });
	}

	public List<AmostragemBox> amostragemBoxPorLote(int amostragemlote_codigo)
	{
		List<AmostragemBox> result = new ArrayList<AmostragemBox>();
		Cursor cursor = ConexaoManager.getInstance(context).rawQuery(
				"select T." + AmostragemBox.CODIGO + ", T."
						+ AmostragemBox.NRBOX + ", T."
						+ AmostragemBox.AMOSTRAGEMLOTE_CODIGO + ", T."
						+ AmostragemBox.QTDAVESPESAR + ", T."
						+ AmostragemBox.CLASSIFICACAO_CODIGO + ", C."
						+ Classificacao.SIGLA + " as "
						+ AmostragemBox.CLASSIFICACAO_DESCRICAO + " from "
						+ TABLE_NAME + " T " + "left join "
						+ DAOClassificacao.TABLE_NAME + " C on C."
						+ Classificacao.CODIGO + " = T."
						+ AmostragemBox.CLASSIFICACAO_CODIGO + " where T."
						+ AmostragemBox.AMOSTRAGEMLOTE_CODIGO + " = ? "
						+ "order by T." + AmostragemBox.NRBOX,
				new String[] { String.valueOf(amostragemlote_codigo) });
		if (cursor != null)
		{
			while (cursor.moveToNext())
			{
				AmostragemBox ab = new AmostragemBox(cursor.getShort(0),
						cursor.getShort(1), cursor.getInt(2), cursor.getInt(3),
						cursor.getShort(4), cursor.getString(5));

				int qtdPesadas = new DAOAmostragemPesos(context)
						.boxTemPesos(cursor.getShort(0));
				ab.setQtdPesadas(qtdPesadas);

				result.add(ab);
			}
		}
		return result;
	}

	public int amostragemBoxPorLoteCount(int amostragemlote_codigo)
	{
		Cursor cursor = ConexaoManager.getInstance(context).rawQuery(
				"select count(*) from " + TABLE_NAME + " where "
						+ AmostragemBox.AMOSTRAGEMLOTE_CODIGO + " = ?",
				new String[] { String.valueOf(amostragemlote_codigo) });

		cursor.moveToNext();

		return cursor.getInt(0);
	}

	public int amostragemBoxPorLoteMaxAves(int amostragemlote_codigo)
	{
		Cursor cursor = ConexaoManager.getInstance(context).rawQuery(
				"select coalesce(max(" + AmostragemBox.QTDAVESPESAR
						+ "), 0) from " + TABLE_NAME + " where "
						+ AmostragemBox.AMOSTRAGEMLOTE_CODIGO + " = ?",
				new String[] { String.valueOf(amostragemlote_codigo) });

		cursor.moveToNext();

		return cursor.getInt(0);
	}
}
