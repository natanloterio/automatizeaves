package net.automatize.aves.adapter;

import java.util.ArrayList;

import net.automatize.aves.R;
import net.automatize.aves.model.AmostragemLote;
import net.automatize.aves.util.Constantes;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class AmostragemLotesAdapter extends ArrayAdapter<AmostragemLote> {
	private ArrayList<AmostragemLote> entries;
	private Context context;

	public AmostragemLotesAdapter(Context context, int textViewResourceId,
			ArrayList<AmostragemLote> entries) {
		super(context, textViewResourceId, entries);
		this.entries = entries;
		this.context = context;
	}

	public static class ViewHolder {
		public TextView item1;
		public TextView item2;
		public TextView item3;
		public TextView item4;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		ViewHolder holder;
		if (v == null) {
			LayoutInflater vi = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			v = vi.inflate(
					R.layout.activity_view_amostragem_semanal_selecaolote_grid_item,
					null);

			holder = new ViewHolder();
			holder.item1 = (TextView) v
					.findViewById(R.id.amostragemlote_nrlote);
			holder.item2 = (TextView) v.findViewById(R.id.amostragemlote_sexo);

			holder.item3 = (TextView) v
					.findViewById(R.id.amostragemlote_qtd_boxes);

			holder.item4 = (TextView) v
					.findViewById(R.id.amostragemlote_qtd_aves_box);

			v.setTag(holder);
		} else
			holder = (ViewHolder) v.getTag();

		final AmostragemLote custom = entries.get(position);
		if (custom != null) {
			holder.item1.setText(custom.getNumeroLote());

			if (custom.getSexo().equals(Constantes.SEXO_VALOR_FEMEA)) {
				holder.item2.setText("F�mea");
			} else if (custom.getSexo().equals(Constantes.SEXO_VALOR_MACHO)) {
				holder.item2.setText("Macho");
			} else if (custom.getSexo().equals(Constantes.SEXO_VALOR_AMBOS)) {
				holder.item2.setText("Ambos");
			}

			holder.item3.setText(String.valueOf(custom.getQtdBoxes()));
			holder.item4.setText(String.valueOf(custom.getQtdAvesPorBox()));
		}
		return v;
	}
}
