package net.automatize.aves.adapter;

import java.util.ArrayList;

import net.automatize.aves.R;
import net.automatize.aves.model.AmostragemBox;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class AmostragemBoxAdapter extends ArrayAdapter<AmostragemBox>
{
	private ArrayList<AmostragemBox> entries;
	private Context context;

	public AmostragemBoxAdapter(Context context, int textViewResourceId,
			ArrayList<AmostragemBox> entries)
	{
		super(context, textViewResourceId, entries);
		this.entries = entries;
		this.context = context;
	}

	public static class ViewHolder
	{
		public TextView item1;
		public TextView item2;
		public TextView item3;
		public TextView item4;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		View v = convertView;
		ViewHolder holder;
		if (v == null)
		{
			LayoutInflater vi = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			v = vi.inflate(
					R.layout.activity_view_amostragem_semanal_configuracao_box_grid_item,
					null);

			holder = new ViewHolder();
			holder.item1 = (TextView) v
					.findViewById(R.id.amostragembox_item_nrbox);
			holder.item2 = (TextView) v
					.findViewById(R.id.amostragembox_item_qtd_aves_pesar);

			holder.item3 = (TextView) v
					.findViewById(R.id.amostragembox_item_classificacao);
			
			holder.item4 = (TextView) v
					.findViewById(R.id.amostragembox_item_qtdpesadas);

			v.setTag(holder);
		} else
			holder = (ViewHolder) v.getTag();

		final AmostragemBox custom = entries.get(position);
		if (custom != null)
		{
			holder.item1.setText(String.valueOf(custom.getNrbox()));
			holder.item2.setText(String.valueOf(custom.getQtdAvesPesar()));
			holder.item3.setText(custom.getClassificacao_descricao());
			holder.item4.setText(String.valueOf(custom.getQtdPesadas()));
		}
		return v;
	}
}
