package net.automatize.aves.adapter;

import java.util.ArrayList;

import net.automatize.aves.R;
import net.automatize.aves.model.Classificacao;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ClassificacoesAdapter extends ArrayAdapter<Classificacao>
{
	private ArrayList<Classificacao> entries;
	private Context context;

	public ClassificacoesAdapter(Context context, int textViewResourceId,
			ArrayList<Classificacao> entries)
	{
		super(context, textViewResourceId, entries);
		this.entries = entries;
		this.context = context;
	}

	public static class ViewHolder
	{
		public TextView item1;
		public TextView item2;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		View v = convertView;
		ViewHolder holder;
		if (v == null)
		{
			LayoutInflater vi = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			v = vi.inflate(R.layout.activity_view_classificacoes_grid_item,
					null);

			holder = new ViewHolder();
			holder.item1 = (TextView) v
					.findViewById(R.id.classificacoes_codigo);
			holder.item2 = (TextView) v.findViewById(R.id.classificacoes_sigla);
			v.setTag(holder);
		} else
			holder = (ViewHolder) v.getTag();

		final Classificacao custom = entries.get(position);
		if (custom != null)
		{
			holder.item1.setText(String.valueOf(custom.getCodigo()));
			holder.item2.setText(custom.getSigla());
		}
		return v;
	}
}
