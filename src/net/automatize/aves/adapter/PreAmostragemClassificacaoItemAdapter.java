package net.automatize.aves.adapter;

import java.util.ArrayList;

import net.automatize.aves.R;
import net.automatize.aves.model.PreAmostragemClassificacaoItem;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class PreAmostragemClassificacaoItemAdapter extends
		ArrayAdapter<PreAmostragemClassificacaoItem>
{
	private ArrayList<PreAmostragemClassificacaoItem> entries;
	private Context context;

	public PreAmostragemClassificacaoItemAdapter(Context context,
			int textViewResourceId,
			ArrayList<PreAmostragemClassificacaoItem> entries)
	{
		super(context, textViewResourceId, entries);
		this.entries = entries;
		this.context = context;
	}

	public static class ViewHolder
	{
		public TextView item1;
		public CheckBox item2;
		public TextView item3;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		View v = convertView;
		ViewHolder holder;
		if (v == null)
		{
			LayoutInflater vi = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			v = vi.inflate(
					R.layout.activity_view_pre_amostragem_selecao100_classificacao_grid_item,
					null);

			holder = new ViewHolder();
			holder.item1 = (TextView) v
					.findViewById(R.id.preamostragem_classificacao_sigla);
			holder.item2 = (CheckBox) v
					.findViewById(R.id.preamostragem_classificacao_selecionado);
			holder.item2.setTag(position);

			holder.item2.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					CheckBox chk = (CheckBox) v;
					Integer estado = (Integer) chk.getTag();

					if (chk.isChecked())
					{
						entries.get(estado).setSelecionado(true);
					} else
					{
						entries.get(estado).setSelecionado(false);
					}
				}
			});

			holder.item3 = (EditText) v
					.findViewById(R.id.preamostragem_classificacao_percentual);

			v.setTag(holder);
		} else
			holder = (ViewHolder) v.getTag();

		final PreAmostragemClassificacaoItem custom = entries.get(position);
		if (custom != null)
		{
			holder.item1.setText(custom.getClassificacao_sigla());
			holder.item2.setSelected(custom.getSelecionado());
//			if (holder.item2.isSelected())
//			{
				holder.item3.setText(String.valueOf(custom.getPercentual()));
//				holder.item3.setEnabled(true);
//			} else
//			{
//				holder.item3.setText("0");
//				holder.item3.setEnabled(false);
//			}
		}
		return v;
	}
}
