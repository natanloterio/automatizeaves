package net.automatize.aves.adapter;

import java.util.ArrayList;

import net.automatize.aves.R;
import net.automatize.aves.model.AmostragemResultado;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class AmostragemResultadoAdapter extends
		ArrayAdapter<AmostragemResultado>
{
	private ArrayList<AmostragemResultado> entries;
	private Context context;

	public AmostragemResultadoAdapter(Context context, int textViewResourceId,
			ArrayList<AmostragemResultado> entries)
	{
		super(context, textViewResourceId, entries);
		this.entries = entries;
		this.context = context;
	}

	public static class ViewHolder
	{
		public TextView item1;
		public TextView item2;
		public TextView item3;
		public TextView item4;
		public TextView item5;
		public TextView item6;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		View v = convertView;
		ViewHolder holder;
		if (v == null)
		{
			LayoutInflater vi = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			v = vi.inflate(
					R.layout.activity_view_amostragem_semanal_resultado_grid_item,
					null);

			holder = new ViewHolder();
			holder.item1 = (TextView) v
					.findViewById(R.id.amostragemresultado_lote);
			holder.item2 = (TextView) v
					.findViewById(R.id.amostragemresultado_qtdpesos);

			holder.item3 = (TextView) v
					.findViewById(R.id.amostragemresultado_pesototal);

			holder.item4 = (TextView) v
					.findViewById(R.id.amostragemresultado_pesomedio);

			holder.item5 = (TextView) v
					.findViewById(R.id.amostragemresultado_menorpeso);

			holder.item6 = (TextView) v
					.findViewById(R.id.amostragemresultado_maiorpeso);

			v.setTag(holder);
		} else
			holder = (ViewHolder) v.getTag();

		final AmostragemResultado custom = entries.get(position);
		if (custom != null)
		{
			holder.item1.setText(custom.getAmostragemBox().getNrbox() + "-"
					+ custom.getAmostragemBox().getClassificacao_descricao());
			holder.item2.setText(String.valueOf(custom.getQtdPesos()));
			holder.item3.setText(String.valueOf(custom.getPesoTotal()));
			holder.item4.setText(String.valueOf(custom.getPesoMedio()));
			holder.item5.setText(String.valueOf(custom.getMenorPeso()));
			holder.item6.setText(String.valueOf(custom.getMaiorPeso()));
		}
		return v;
	}
}
