package net.automatize.aves.adapter;

import java.util.ArrayList;

import net.automatize.aves.R;
import net.automatize.aves.model.PreAmostragemSelecao100Resultado;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class PreAmostragemSelecao100ResultadoAdapter extends
		ArrayAdapter<PreAmostragemSelecao100Resultado>
{
	private ArrayList<PreAmostragemSelecao100Resultado> entries;
	private Context context;

	public PreAmostragemSelecao100ResultadoAdapter(Context context,
			int textViewResourceId, ArrayList<PreAmostragemSelecao100Resultado> entries)
	{
		super(context, textViewResourceId, entries);
		this.entries = entries;
		this.context = context;
	}

	public static class ViewHolder
	{
		public TextView item1;
		public TextView item2;
		public TextView item3;
		public TextView item4;
		public TextView item5;
		public TextView item6;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		View v = convertView;
		final ViewHolder holder;
		if (v == null)
		{
			LayoutInflater vi = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			v = vi.inflate(
					R.layout.activity_view_pre_amostragem_selecao100_resultado_grid_item,
					null);

			holder = new ViewHolder();
			holder.item1 = (TextView) v
					.findViewById(R.id.preamostragem_resultado_classificacao);

			holder.item2 = (TextView) v
					.findViewById(R.id.preamostragem_resultado_faixainicial);
			holder.item3 = (TextView) v
					.findViewById(R.id.preamostragem_resultado_faixafinal);

			holder.item4 = (TextView) v
					.findViewById(R.id.preamostragem_resultado_quantidade);

			holder.item5 = (TextView) v
					.findViewById(R.id.preamostragem_resultado_acumulado);

			holder.item6 = (TextView) v
					.findViewById(R.id.preamostragem_resultado_media);

			v.setTag(holder);
		} else
			holder = (ViewHolder) v.getTag();

		final PreAmostragemSelecao100Resultado custom = entries.get(position);
		if (custom != null)
		{
			holder.item1.setText(custom.getClassificacao_sigla());

			if (custom.getClassificacao_sigla().equals("TOTAL") == false)
				holder.item2.setText(String.valueOf(custom.getFaixa_inicial()));

			if (custom.getFaixa_final() > 0)
				holder.item3.setText(String.valueOf(custom.getFaixa_final()));

			holder.item4.setText(String.valueOf(custom.getQtd()));
			holder.item5.setText(String.valueOf(custom.getAcumulado()));
			holder.item6.setText(String.valueOf(custom.getMedia()));
		}
		return v;
	}
}