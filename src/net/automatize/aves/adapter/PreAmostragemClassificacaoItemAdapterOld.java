package net.automatize.aves.adapter;

import java.util.ArrayList;

import net.automatize.aves.R;
import net.automatize.aves.model.PreAmostragemClassificacaoItem;
import net.automatize.aves.util.FuncoesCompartilhadas;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class PreAmostragemClassificacaoItemAdapterOld extends
		ArrayAdapter<PreAmostragemClassificacaoItem>
{
	private ArrayList<PreAmostragemClassificacaoItem> entries;
	private Context context;	 

	public PreAmostragemClassificacaoItemAdapterOld(Context context,
			int textViewResourceId,
			ArrayList<PreAmostragemClassificacaoItem> entries)
	{
		super(context, textViewResourceId, entries);
		this.entries = entries;
		this.context = context;
	}

	public static class ViewHolder2
	{
		public CheckBox item1;
		public TextView item2;
		public TextView item3;
		public ImageButton item4;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		View v = convertView;
		final ViewHolder2 holder;
		if (v == null)
		{
			LayoutInflater vi = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			v = vi.inflate(
					R.layout.activity_view_pre_amostragem_selecao100_classificacao_grid_item,
					null);

			holder = new ViewHolder2();
			holder.item1 = (CheckBox) v
					.findViewById(R.id.preamostragem_classificacao_selecionado);

			holder.item2 = (TextView) v
					.findViewById(R.id.preamostragem_classificacao_sigla);

			holder.item3 = (TextView) v
					.findViewById(R.id.preamostragem_classificacao_percentual);

			holder.item4 = (ImageButton) v
					.findViewById(R.id.preamostragem_classificacao_btnedit);

			v.setTag(holder);
		} else
			holder = (ViewHolder2) v.getTag();

		holder.item1.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				CheckBox chk = (CheckBox) v;
				if (chk.isChecked())
				{
					entries.get(position).setSelecionado(true);
				} else
				{
					entries.get(position).setSelecionado(false);
				}
				FuncoesCompartilhadas.log(position);
			}
		});

		holder.item4.setTag(position);

		holder.item4.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				final PreAmostragemClassificacaoItem item = entries
						.get(position);

				if (item != null)
				{
					AlertDialog.Builder builder = new AlertDialog.Builder(
							context);
					builder.setTitle("Title");

					// Set up the input
					final EditText input = new EditText(context);
					input.setText(String.valueOf(input.getText().toString()));

					// Specify the type of input expected; this, for example,
					// sets
					// the input as a password, and will mask the text
					input.setInputType(InputType.TYPE_CLASS_NUMBER);
					builder.setView(input);

					// Set up the buttons
					builder.setPositiveButton("OK",
							new DialogInterface.OnClickListener()
							{
								@Override
								public void onClick(DialogInterface dialog,
										int which)
								{

									if (input.getText().toString().equals("") == false)
									{
										short s = Short.parseShort(input
												.getText().toString());
										entries.get(position).setPercentual(s);
									}
								}
							});
					builder.setNegativeButton("Cancel",
							new DialogInterface.OnClickListener()
							{
								@Override
								public void onClick(DialogInterface dialog,
										int which)
								{
									dialog.cancel();
								}
							});

					builder.show();
				}
			}
		});

		final PreAmostragemClassificacaoItem custom = entries.get(position);
		if (custom != null)
		{
			holder.item1.setSelected(custom.getSelecionado());
			holder.item2.setText(custom.getClassificacao_sigla());
			holder.item3.setText(String.valueOf(custom.getPercentual()));
		}
		return v;
	}
}