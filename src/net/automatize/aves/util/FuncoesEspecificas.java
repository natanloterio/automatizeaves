package net.automatize.aves.util;

import android.content.Context;
import net.automatize.aves.model.Parametros;

public class FuncoesEspecificas
{
	public static boolean ValidaNumeroLote(String NrLote)
	{
		int vTam;
		// String sexo = null;
		// boolean bOk = false;

		NrLote = NrLote.replace(" ", "");
		vTam = NrLote.length();

		// try
		// {
		if ((vTam > 0) && (vTam < 11))
		{
			for (int i = 0; i < NrLote.length(); i++)
			{
				if (Character.isDigit(NrLote.charAt(i)) == false
						&& (NrLote.charAt(i) != '-'))
				{
					return false;
				}
			}

			vTam = StrCountChar(NrLote, '-');
			if (vTam == 0)
			{
				// return sexo;
				return true;
			} else if (vTam == 1)
			{
				if (NrLote.indexOf("-") == NrLote.length() - 2)
				{
					if (NrLote.endsWith("1") || NrLote.endsWith("2")
							|| NrLote.endsWith("3"))
					{

						return true;
						// if (NrLote.charAt(NrLote.length() - 1) == '1')
						// {
						// sexo = Constantes.SEXO_VALOR_MACHO;
						// } else if (NrLote.charAt(NrLote.length() - 1) == '2')
						// {
						// sexo = Constantes.SEXO_VALOR_FEMEA;
						// } else
						// {
						// sexo = Constantes.SEXO_VALOR_AMBOS;
						// }
						// return sexo;
					}
				}
			}
		}

		// } finally
		// {
		// if (bOk == false)
		// {
		// System.out.println("N�mero do Lote inv�lido!");
		// }
		// }
		// return sexo;
		return false;
	}

	public static int StrCountChar(String value, char caracter)
	{
		int count = 0;
		for (int i = 0; i < value.length(); i++)
		{
			if (value.charAt(i) == caracter)
			{
				count++;
			}
		}
		return count;
	}

	public static String VerificaLote(Context context, Parametros parametros,
			String lote)
	{
		lote = lote.replace("-", "");
		int vTam = lote.length();
		int vMax = parametros.getTamanhoNumeroLote() + 1;

		if (vTam > vMax)
		{
			FuncoesCompartilhadas.exibirToastMsgLong(context,
					"Tamanho do Lote superior ao especificado!");
		} else
		{
			if (vTam != vMax)
			{
				if (parametros.getNumeroLote() == Constantes.TRANSFERENCIA_NUMEROLOTE_INDEX_ZERO_ESQUERDA)
				{
					while (vMax > vTam)
					{
						lote = "0" + lote;
						vTam = lote.length();
					}
				} else
				{
					while (vMax > vTam)
					{
						lote = lote + " ";
						vTam = lote.length();
					}
				}

				return lote;
			}
		}
		return null;
	}
}
