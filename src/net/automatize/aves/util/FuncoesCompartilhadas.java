package net.automatize.aves.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import net.automatize.aves.dao.DAOTipoPesagem;
import net.automatize.aves.model.TipoPesagem;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class FuncoesCompartilhadas
{
	public static void exibirToastMsgLong(Context context, String msg)
	{
		Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
	}

	public static void log(String msg)
	{
		Log.i(Constantes.TAG, msg);
	}

	public static void log(int value)
	{
		Log.i(Constantes.TAG, String.valueOf(value));
	}

	public static void exibirToastMsgShort(Context contexto, String msg)
	{
		Toast.makeText(contexto, msg, Toast.LENGTH_SHORT).show();
	}

	public static boolean bluetoothIsSupported(Context context)
	{
		if (Constantes.btAdapter == null)
		{
			exibirToastMsgLong(context, Constantes.MSG_BLUETOOTH_NAO_SUPORTADO);
			return false;
		}
		return true;
	}

	public static void turnOnBT(Activity activity)
	{
		Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
		activity.startActivityForResult(intent, 1);
	}

	public static void MensagemAlerta(Context context, String message)
	{
		final AlertDialog.Builder dialog = criaAlertDialogCustom(context,
				message);
		dialog.create().show();
	}

	/* valida��o para parametros de pesagem e configura��o da balan�a */

	public static TipoPesagem parametrosPesagemConfigurados(Context context)
	{
		if (bluetoothIsSupported(context) == false)
		{
			return null;
		}

		DAOTipoPesagem daoTipoPesagem = new DAOTipoPesagem(context);
		TipoPesagem tp = daoTipoPesagem.getTipoPesagem();

		if (tp == null)
		{
			exibirToastMsgLong(context, "Configure os par�metros de pesagem");
			return null;
		}

		if (tp.getMacAddress() == null || tp.getMacAddress().equals(""))
		{
			exibirToastMsgLong(context, Constantes.MSG_BALANCA_NAO_CONFIGURADA);
			return null;
		}

		if (tp.getTipoPesagem() == 0)
		{
			exibirToastMsgLong(context,
					"Configure o Tipo de Pesagem (Manual ou Autom�tica)");
			return null;
		}

		if (tp.getTipoPesagem() == Constantes.TIPO_PESAGEM_AUTOMATICO
				&& tp.getPesoMinimo() == 0)
		{
			exibirToastMsgLong(context,
					"Configure o peso m�nimo para pesagem autom�tica");
			return null;
		}

		return tp;
	}

	public static boolean validaPeso(String speso)
	{

		if (speso == null || speso.equals(""))
			return false;

		char[] data = speso.toCharArray();
		for (int i = 0; i < data.length; i++)
		{
			if ((Character.isDigit(data[i])
					|| data[i] == Constantes.ASCII_PONTO || data[i] == Constantes.ASCII_LETRA_P) == false)
			{
				return false;
			}
		}

		int count = 0;
		for (int i = 0; i < data.length; i++)
		{
			if (data[i] == Constantes.ASCII_PONTO)
			{
				count++;
			}
		}

		if (count != 1)
		{
			return false;
		}

		return true;
	}

	public static double grToKg(double value)
	{
		return (value / 1000);
	}

	public static String grToKgFormatado(double value)
	{
		return String.format(Locale.getDefault(), "%.3f", grToKg(value));
	}

	public static int kgToGr(double value)
	{
		return (int) (value * 1000);
	}

	public static String formataPeso(double value)
	{
		return String.format(Locale.getDefault(), "%.3f", value);
	}

	public static Date dateTimeToDate(long date)
	{
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(date);

		calendar.set(Calendar.HOUR, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);

		return calendar.getTime();
	}

	public static String formataDataHora(Date date)
	{
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		return df.format(date);
	}

	public static String formataData(Date date)
	{
		return formataData("dd/MM/yyyy", date);
	}

	public static String formataData(String format, Date date)
	{
		SimpleDateFormat df = new SimpleDateFormat(format);
		return df.format(date);
	}

	public static String formataNomeArquivoTransferenciaPeso(String nome,
			short balancaCodigo, Date data)
	{
		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		String filename = nome + "%s%s.txt";
		String f = String.format(filename,
				String.format("%04d", balancaCodigo), df.format(new Date()));

		return f;
	}

	public static String formataPesoTransferencia(double value)
	{
		DecimalFormat formatoDois = new DecimalFormat("#,##0.000",
				new DecimalFormatSymbols(new Locale("pt", "BR")));

		String s = formatoDois.format(value);
		while (s.length() != 8)
			s = " " + s;

		return s;
	}

	public static String quotedStr(String value)
	{
		return "'" + value + "'";
	}

	public static java.util.Date strToDate(String value)
	{
		try
		{
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			return df.parse(value);
		} catch (ParseException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public static String dupeString(String caracter, int count)
	{
		StringBuilder buider = new StringBuilder();
		for (int i = 0; i < count; i++)
		{
			buider.append(caracter);
		}
		return buider.toString();
	}

	public static boolean EditTextIsEmpty(EditText field)
	{
		if (field.getText() == null)
			return true;

		String value = field.getText().toString();
		if (value.trim().equals(""))
			return true;

		return false;
	}

	public static void inicializaDiretorios()
	{
		File pastaAutomatize = new File(Constantes.DIRETORIO_ARMAZENAMENTO);
		if (pastaAutomatize.exists() == false)
		{
			FuncoesCompartilhadas.log("N�o existe - "
					+ Constantes.DIRETORIO_ARMAZENAMENTO);
			pastaAutomatize.mkdir();
		}

		File pastaOriginal = new File(Constantes.DIRETORIO_AMOSTRAGEMSEMANAL);
		if (pastaOriginal.exists() == false)
		{
			FuncoesCompartilhadas.log("N�o existe - "
					+ Constantes.DIRETORIO_AMOSTRAGEMSEMANAL);
			pastaOriginal.mkdirs();
		}

		File pastaPre = new File(Constantes.DIRETORIO_PREAMOSTRAGEM);
		if (pastaPre.exists() == false)
		{
			FuncoesCompartilhadas.log("N�o existe - "
					+ Constantes.DIRETORIO_PREAMOSTRAGEM);
			pastaPre.mkdirs();
		}

		File pastaSelecao100 = new File(Constantes.DIRETORIO_SELECAO100);
		if (pastaSelecao100.exists() == false)
		{
			FuncoesCompartilhadas.log("N�o existe - "
					+ Constantes.DIRETORIO_SELECAO100);
			pastaSelecao100.mkdirs();
		}
	}

	public static boolean copiarArquivo(File foriginal, File fnovo)
	{
		InputStream inStream = null;
		OutputStream outStream = null;

		try
		{
			inStream = new FileInputStream(foriginal);
			outStream = new FileOutputStream(fnovo);

			byte[] buffer = new byte[1024];
			int length;

			while ((length = inStream.read(buffer)) > 0)
			{
				outStream.write(buffer, 0, length);
			}

			inStream.close();
			outStream.close();

		} catch (IOException e)
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public static AlertDialog.Builder criaAlertDialogCustom(Context context,
			String msg)
	{
		return criaAlertDialogCustom(context, msg, "Aten��o");
	}

	public static AlertDialog.Builder criaAlertDialogCustom(Context context,
			String msg, String title)
	{
		final AlertDialog.Builder dialog = new AlertDialog.Builder(context);

		/* title */
		final TextView tvTitle = new TextView(context);
		tvTitle.setText(title);
		tvTitle.setTextColor(context.getResources().getColor(
				android.R.color.holo_blue_light));
		tvTitle.setPadding(8, 10, 8, 10);
		tvTitle.setTextSize(26);
		dialog.setCustomTitle(tvTitle);

		/* Message */
		final TextView tvMessage = new TextView(context);
		tvMessage.setText(msg);
		tvMessage.setPadding(8, 10, 8, 10);
		tvMessage.setTextSize(26);
		dialog.setView(tvMessage);

		return dialog;
	}

}
