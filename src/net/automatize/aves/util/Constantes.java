package net.automatize.aves.util;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import android.bluetooth.BluetoothAdapter;
import android.os.Environment;

public class Constantes
{
	public static final String TITULO_APLICACAO = "Automatize - Aves - Android - Vers�o: 2014.1";
	public static final String DATABASE_NAME = "automatize.db";
	public static final int DATABASE_VERSION = 1;

	public static final String TXT_NOVA_LINHA = "\r\n";

	/* diret�rios de armazenamento */
	public static final String DIRETORIO_ARMAZENAMENTO = Environment
			.getExternalStorageDirectory().getPath()
			+ File.separator
			+ "AutomatizeAves" + File.separator;

	public static final String DIRETORIO_AMOSTRAGEMSEMANAL = DIRETORIO_ARMAZENAMENTO
			+ "Original" + File.separator;

	public static final String DIRETORIO_PREAMOSTRAGEM = DIRETORIO_ARMAZENAMENTO
			+ "OriginalPre" + File.separator;

	public static final String DIRETORIO_SELECAO100 = DIRETORIO_ARMAZENAMENTO
			+ "Original100" + File.separator;

	/*
	 * Captura peso e config BT
	 */

	public static final BluetoothAdapter btAdapter = BluetoothAdapter
			.getDefaultAdapter();

	public static final char PESO5 = 5; // congela
	public static final char PESO6 = 6; // n�o congela
	public static final String TAG = "DEBUG";
	public static final char CALIBRACAO = 9;
	public static final String TARAR = "z";
	public static final int SUCESSO_CONEXAO = 0;
	public static final int MENSAGEM_LIDA = 1;
	public static final int DESCONECTADO = 2;
	// public static final int PESO_NA_BALANCA = 3;
	public static final UUID MY_UUID = UUID
			.fromString("00001101-0000-1000-8000-00805F9B34FB");

	public static final int CALIBRACAO_TEMPO_ENVIO_ANTES = 1000;
	public static final int CALIBRACAO_TEMPO_ENVIO_DEPOIS = 1500;
	public static final String ASYNCTASK_MSG_PESO = "P";
	public static final String ASYNCTASK_MSG_FALHA = "F";
	public static final int TEMPO_TENTATIVA_CONEXAO = 8000; // 5s j� retorna
															// erro se o BT
															// estiver desligado

	/* valores ascii */
	public static final int ASCII_PONTO = 46;
	public static final int ASCII_LETRA_P = 80;

	/*
	 * Parametros de pesagem
	 */

	public static final short TIPO_PESAGEM_MANUAL = 1;
	public static final short TIPO_PESAGEM_AUTOMATICO = 2;
	public static final String TIPO_PESAGEM_MANUAL_DESC = "Manual";
	public static final String TIPO_PESAGEM_AUTOMATICO_DESC = "Autom�tico";

	/*
	 * Mensagens
	 */
	public static final String MSG_OPERACAO_REALIZADA_SUCESSO = "Opera��o realizada com sucesso!";
	public static final String MSG_CONFIRMACAO_EXCLUSAO_TITLE = "Confirma��o de exclus�o...";
	public static final String MSG_CONFIRMACAO_EXCLUSAO_MSG = "Voc� deseja excluir este registro?";
	public static final String MSG_REGISTRO_EXCLUIDO_SUCESSO = "Registro(s) exclu�do(s) com sucesso!";
	public static final String MSG_REGISTRO_INCLUIDO_SUCESSO = "Registro inserido com sucesso!";
	public static final String MSG_REGISTRO_ALTERADO_SUCESSO = "Registro alterado com sucesso!";
	public static final String MSG_ERRO_GRAVACAO = "Houve um erro ao realizar esta opera��o. Tente novamente.";
	public static final String MSG_BLUETOOTH_NAO_SUPORTADO = "Bluetooth n�o suportado. O aplicativo ser� encerrado.";
	public static final String MSG_NAO_HA_CLASSIFICACOES = "N�o h� classifica��es cadastradas";
	public static final String MSG_BALANCA_NAO_CONFIGURADA = "Balan�a n�o configurada!";
	public static final String MSG_CAMPO_NAO_INFORMADO = "O campo %s deve ser informado";
	public static final String MSG_TITLE_MENU_CONTEXT = "Selecione a a��o desejada";
	public static final String MSG_TOTAL_BOXES_EXCEDIDO = "N�mero de boxes superior a quantidade informada no lote. N� de boxes configurado: %d";
	public static final String MSG_FINAL_PESAGEM = "Final de Pesagem!";
	public static final String MSG_CONFIRMACAO_LIMPEZA_BOX = "Confirma a limpeza de todos os pesos deste box?";
	public static final String MSG_AMOSTRAGEM_PESOS_OUTRA_DATA = "Existem pesos lan�ados de outra data!";
	public static final String MSG_CONFIRMACAO_LIMPEZA_LOTE = "Confirma a limpeza de todos os pesos deste lote?";
	public static final String MSG_CONFIRMACAO_LIMPEZA_PESOS = "Confirma limpeza de todos os pesos?";
	public static final String MSG_CONFIRMACAO_LIMPEZA_ULTIMO_PESO = "Confirma a exclus�o do �ltimo peso?";
	public static final String MSG_CODIGO_BALANCA_INVALIDO = "O c�digo da balan�a informado n�o � v�lido";
	public static final String MSG_ERRO_PERMISSAO_PASTA = "Verifique as permiss�es de leitura e grava��o deste diret�rio";
	public static final String MSG_TRANSFERENCIA_NAO_HA_DADOS = "N�o h� dados a transmitir!";
	public static final String MSG_SALVA_ARQUIVO_SUCESSO = "Arquivo gerado com sucesso!";
	public static final String MSG_IMPOSSIVEL_EXCLUIR = "Imposs�vel excluir";
	public static final String MSG_IMPOSSIVEL_ALTERAR = "Imposs�vel alterar";
	public static final String MSG_PESO_MENOR_MINIMO = "Menor que o m�nimo";

	public static final String[] CLASSIFICACOES_PADRAO = { "PP", "PE", "PL",
			"ME", "ML", "LE", "SL" };

	// transferencia de peso - numero lote
	public static final String TRANSFERENCIA_PESO_NRLOTE_ZERO_ESQUERDA = "Zeros � esquerda";
	public static final String TRANSFERENCIA_PESO_NRLOTE_ESPACO_DIREITA = "Espa�os � direita";
	public static final List<String> TRANSFERENCIA_PESO_NRLOTE = new ArrayList<String>(
			Arrays.asList(TRANSFERENCIA_PESO_NRLOTE_ZERO_ESQUERDA,
					TRANSFERENCIA_PESO_NRLOTE_ESPACO_DIREITA));

	// transferencia de peso - tamanho
	public static final String TRANSFERENCIA_PESO_TAMANHO_1 = "Byte 1";
	public static final String TRANSFERENCIA_PESO_TAMANHO_2 = "Byte 2";
	public static final String TRANSFERENCIA_PESO_TAMANHO_3 = "Byte 3";
	public static final String TRANSFERENCIA_PESO_TAMANHO_4 = "Byte 4";
	public static final String TRANSFERENCIA_PESO_TAMANHO_5 = "Byte 5";
	public static final String TRANSFERENCIA_PESO_TAMANHO_6 = "Byte 6";
	public static final String TRANSFERENCIA_PESO_TAMANHO_7 = "Byte 7";
	public static final String TRANSFERENCIA_PESO_TAMANHO_8 = "Byte 8";
	public static final List<String> TRANSFERENCIA_PESO_TAMANHO = new ArrayList<String>(
			Arrays.asList(TRANSFERENCIA_PESO_TAMANHO_1,
					TRANSFERENCIA_PESO_TAMANHO_2, TRANSFERENCIA_PESO_TAMANHO_3,
					TRANSFERENCIA_PESO_TAMANHO_4, TRANSFERENCIA_PESO_TAMANHO_5,
					TRANSFERENCIA_PESO_TAMANHO_6, TRANSFERENCIA_PESO_TAMANHO_7,
					TRANSFERENCIA_PESO_TAMANHO_8));

	public static final int TRANSFERENCIA_NUMEROLOTE_INDEX_ZERO_ESQUERDA = 0;
	public static final int TRANSFERENCIA_NUMEROLOTE_INDEX_ESPACO_DIREITA = 1;

	public static final int PARAMETROS_TRANSFERENCIA_CODIGO = 1;
	public static final int AMOSTRAGEMDATA_CODIGO = 1;
	public static final short PREAMOSTRAGEM_CODIGO = 1;
	public static final short SELECAO100_CODIGO = 1;

	/* Amostragem semanal */

	public static final String MSG_AMOSTRAGEMSEMANAL_CAPTURA_PESO_STATUS = "Amostragem Semanal - Lote: %s - Box: %d - Pesadas: %d/%d";
	public static final String MSG_AMOSTRAGEMSEMANAL_RESULTADO_TITLE = "Amostragem Semanal - Resultado do Lote: %s";

	/* Pre amostragem */
	public static final String MSG_PREAMOSTRAGEM_CAPTURAPESO_TITLE = "Pr�-Amostragem para Sele��o 100% - Pesadas: ";

	/* Sele��o 100% */
	public static final String MSG_SELECAO_CAPTURAPESO_TITLE = "Sele��o 100% - Pesadas: ";

	/* geral */
	public static final String MSG_EXISTEM_PESADOS_ACAO_TITLE = "Existem %d pesos registrados: A��o: ";

	/* Constantes para o tipo de dado Sexo */

	public static final int SEXO_INDEX_FEMEA = 0;
	public static final int SEXO_INDEX_MACHO = 1;
	public static final int SEXO_INDEX_AMBOS = 2;
	public static final String SEXO_VALOR_FEMEA = "F";
	public static final String SEXO_VALOR_MACHO = "M";
	public static final String SEXO_VALOR_AMBOS = "A";

	/* constantes para tipo de amostragem (percentual/fixo) */
	public static final int TIPOAMOSTRAGEM_INDEX_FIXO = 0;
	public static final int TIPOAMOSTRAGEM_INDEX_PERCENTUAL = 1;
	public static final String TIPOAMOSTRAGEM_VALOR_FIXO = "F";
	public static final String TIPOAMOSTRAGEM_VALOR_PERCENTUAL = "P";
}
